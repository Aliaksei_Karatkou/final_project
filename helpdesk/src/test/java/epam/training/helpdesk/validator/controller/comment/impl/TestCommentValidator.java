package epam.training.helpdesk.validator.controller.comment.impl;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.validation.MalformedDataException;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.comment.CommentValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCommentValidator {

    private static final Integer MAX_COMMENT_LENGTH = 500;
    private static final String STRING_WITH_INVALID_CHARACTERS = "avФФФddd";
    private TicketService ticketService;
    private CommentValidator validator;
    private CommentDto dto;

    @Before
    public void setUp() {
        ticketService = mock(TicketService.class);
        validator = new CommentValidatorImpl(ticketService);
        dto = new CommentDto();
    }

    @Test(expected = MalformedDataException.class)
    public void testValidateDtoDtoIsNull() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        validator.validateDto(null, new User());
    }

    @Test(expected = MalformedDataException.class)
    public void testValidateDtoDtoTextIsNull() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        dto.setTicketId(1);
        validator.validateDto(dto, null);
    }

    @Test(expected = MalformedDataException.class)
    public void testValidateDtoDtoTicketIdIsNull() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        dto.setText("ee");
        validator.validateDto(dto, null);
    }

    @Test(expected = ValidationException.class)
    public void testValidateDtoTextIsEmpty() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        dto.setText("");
        dto.setTicketId(1);
        validator.validateDto(dto, new User());
    }

    @Test(expected = ValidationException.class)
    public void testValidateDtoTextIsTooLong() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        StringBuilder sb = new StringBuilder(MAX_COMMENT_LENGTH + 1);
        for (int i = 0; i < MAX_COMMENT_LENGTH + 1; i++) {
            sb.append('a');
        }
        dto.setText(sb.toString());
        dto.setTicketId(1);
        validator.validateDto(dto, new User());
    }

    @Test(expected = ValidationException.class)
    public void testValidateDtoTicketNotVisible() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(false);
        dto.setText("abc");
        dto.setTicketId(1);
        validator.validateDto(dto, new User());
    }

    @Test
    public void testValidateDtoNormal() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        dto.setText("~.\"(),:;<>@[]!#$%&'*+-/=?^_`{|}azAS4");
        dto.setTicketId(1);
        validator.validateDto(dto, new User());
    }

    @Test(expected = ValidationException.class)
    public void testValidateDtoWrongChars() {
        when(ticketService.isTicketVisible(any(), any())).thenReturn(true);
        dto.setText(STRING_WITH_INVALID_CHARACTERS);
        dto.setTicketId(1);
        validator.validateDto(dto, new User());
    }

}

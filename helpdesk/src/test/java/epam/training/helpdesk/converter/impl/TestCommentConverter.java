package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.CommentConverter;
import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.service.impl.TicketServiceImpl;
import epam.training.helpdesk.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCommentConverter {

    @Mock
    private TicketServiceImpl ticketService;

    @Mock
    private UserServiceImpl userService;

    @InjectMocks
    private CommentConverter converter = new CommentConverterImpl(userService, ticketService);

    private CommentDto getTestCommentDto(Timestamp timestamp) {
        CommentDto dto = new CommentDto();
        dto.setText("comment");
        dto.setUserId(1);
        dto.setDate(timestamp);
        dto.setTicketId(1);
        return dto;
    }

    private Comment getTestComment(Timestamp timestamp) {
        Comment comment = new Comment();
        Ticket ticket = new Ticket();
        ticket.setId(2);
        User user = new User();
        user.setId(3);
        user.setFirstName("1");
        user.setLastName("2");
        comment.setDate(timestamp);
        comment.setCommentedTicket(ticket);
        comment.setCommentator(user);
        comment.setText("text");
        comment.setId(4);
        return comment;
    }

    @Test
    public void testConvertFromDto() {
// given
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        CommentDto dto = getTestCommentDto(timestamp);
        Ticket ticket = new Ticket();
        User user = new User();
        when(ticketService.getTicket(1)).thenReturn(Optional.ofNullable(ticket));
        when(userService.getUserById(1)).thenReturn(user);
// when
        Comment comment = converter.convertFromDto(dto);
// then
        assertEquals(user, comment.getCommentator());
        assertEquals("comment", comment.getText());
        assertEquals(timestamp, comment.getDate());
        assertEquals(ticket, comment.getCommentedTicket());
    }

    @Test
    public void testConvertToDto() {
// given
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Comment comment = getTestComment(timestamp);
// when
        CommentDto dto = converter.convertToDto(comment);
// then
        assertEquals(comment.getId(), dto.getId());
        assertEquals(comment.getCommentator().getFirstName() + " " + comment.getCommentator().getLastName(), dto.getUsername());
        assertEquals(comment.getText(), dto.getText());
    }
}

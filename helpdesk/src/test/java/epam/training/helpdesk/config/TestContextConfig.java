package epam.training.helpdesk.config;

import epam.training.helpdesk.service.CategoryService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestContextConfig {

    @Bean
    public CategoryService categoryService() {
        return Mockito.mock(CategoryService.class);
    }
}

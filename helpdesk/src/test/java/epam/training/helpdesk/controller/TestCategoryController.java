package epam.training.helpdesk.controller;

import epam.training.helpdesk.config.AppConfig;
import epam.training.helpdesk.config.TestContextConfig;
import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.service.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@WebAppConfiguration
@ContextConfiguration(classes = {AppConfig.class, TestContextConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class TestCategoryController {

    private static final int ATTACHMENTS_COUNT = 2;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    CategoryService categoryService;

    private MockMvc mockMvc;

    private static final String CONTENT_TYPE = "application/json";

    private List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        for (int i = 1; i <= ATTACHMENTS_COUNT; i++) {
            Category category = new Category();
            category.setId(i);
            category.setName("category" + i);
            categories.add(category);
        }
        return categories;
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testGetCategories() throws Exception {
// given
        List<Category> categories = getCategories();
        Mockito.when(categoryService.getAllCategories()).thenReturn(categories);
// when
        mockMvc.perform(MockMvcRequestBuilders
                .get("/helpdesk/api/1.0.0/categories/"))
// then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(ATTACHMENTS_COUNT)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("category1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("category2")));
    }
}

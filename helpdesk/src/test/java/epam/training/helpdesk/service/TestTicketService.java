package epam.training.helpdesk.service;

import epam.training.helpdesk.converter.PermissionConverter;
import epam.training.helpdesk.converter.TicketConverter;
import epam.training.helpdesk.converter.impl.TicketConverterImpl;
import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.domain.Role;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.repository.TicketRepository;
import epam.training.helpdesk.service.impl.TicketServiceImpl;
import epam.training.helpdesk.validator.controller.filter.FilterValidator;
import epam.training.helpdesk.validator.controller.sort.SortOrderValidator;
import epam.training.helpdesk.validator.controller.ticket.TicketValidator;
import epam.training.helpdesk.validator.controller.ticket.impl.TicketValidatorImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestTicketService {

    @Mock
    private AttachmentService attachmentService;

    @Mock
    private CategoryService categoryService;

    @Mock
    private FilterValidator filterValidator;

    @Mock
    private PermissionConverter permissionConverter;

    @Mock
    private HistoryService historyService;

    @Mock
    private NotificationService notificationService;

    @Mock
    private SortOrderValidator sortOrderValidator;

    @Mock
    private TicketRepository ticketRepository;

    @InjectMocks
    private TicketConverter ticketConverter = new TicketConverterImpl(categoryService, attachmentService);

    @InjectMocks
    private TicketValidator ticketValidator = new TicketValidatorImpl(attachmentService, categoryService);

    @InjectMocks
    private TicketService ticketService = new TicketServiceImpl(ticketRepository,
            ticketConverter,
            ticketValidator,
            filterValidator,
            attachmentService,
            permissionConverter,
            historyService,
            notificationService,
            sortOrderValidator);

    private Date getCurrentDatePlusTwoDays() {
        Date date = new Date();
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusDays(2);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private Attachment getAttachment() {
        Attachment attachment = new Attachment();
        attachment.setName("name1");
        return attachment;
    }

    private Category getCategory() {
        Category category = new Category();
        category.setId(1);
        category.setName("some category name");
        return category;
    }

    private TicketDto getTicketDto(Date desiredResolutionDate, Set<Integer> attachmetsSet) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setState(State.DRAFT);
        ticketDto.setName("name");
        ticketDto.setCategoryId(1);
        ticketDto.setDesiredResolutionDate(desiredResolutionDate);
        ticketDto.setAttachmentsId(attachmetsSet);
        return ticketDto;
    }

    private Ticket getTicketFromRepository() {
        Ticket ticketFromRepository = new Ticket();
        ticketFromRepository.setId(1);
        return ticketFromRepository;
    }

    private User getUser() {
        User user = new User();
        user.setId(1);
        user.setRole(Role.EMPLOYEE);
        return user;
    }

    @Test
    public void testAddTicket() {
//given
        Date desiredResolutionDate = getCurrentDatePlusTwoDays();
        Set<Integer> attachmetsSet = new HashSet<>(Arrays.asList(1, 2));
        TicketDto ticketDto = getTicketDto(desiredResolutionDate, attachmetsSet);
        Category category = getCategory();
        Attachment attachment = getAttachment();
        Ticket ticketFromRepository = getTicketFromRepository();
        User user = getUser();

        when(categoryService.getCategory(1)).thenReturn(Optional.of(category));
        when(ticketRepository.addTicket(any())).thenReturn(Optional.of(ticketFromRepository));
        when(ticketRepository.getTicket(1)).thenReturn(Optional.of(ticketFromRepository));
        when(attachmentService.getAttachmentName(1)).thenReturn(Optional.of("file1"));
        when(attachmentService.getAttachmentName(2)).thenReturn(Optional.of("file2"));
        when(attachmentService.getAttachment(Matchers.anyInt())).thenReturn(Optional.of(attachment));
//  when:
        Integer ticketId = ticketService.addTicket(user, ticketDto);
//  then:
        assertTrue(1 == ticketId);
        Mockito.verify(ticketRepository, times(1)).addTicket(any(Ticket.class));
        Mockito.verify(ticketRepository, times(1)).linkAttachments(attachmetsSet, 1);
        Mockito.verify(historyService, times(1)).addHistory(user, ticketFromRepository, Action.ACTION_TICKET_CREATED, "");
        Mockito.verify(historyService, times(1)).addHistory(user, ticketFromRepository, Action.ACTION_FILE_ATTACHED, ": \"file1\"");
        Mockito.verify(historyService, times(1)).addHistory(user, ticketFromRepository, Action.ACTION_FILE_ATTACHED, ": \"file2\"");
        Mockito.verify(categoryService, times(2)).getCategory(1);
    }
}

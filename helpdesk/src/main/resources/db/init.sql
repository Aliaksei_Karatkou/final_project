DROP TABLE IF EXISTS CATEGORY;
CREATE TABLE CATEGORY (
    id integer IDENTITY NOT NULL PRIMARY KEY,
    name character varying(255) NOT NULL,
);

DROP TABLE IF EXISTS USER;
CREATE TABLE USER (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    role_id character varying(8) NOT NULL,
    email character varying(100) NOT NULL,
    password character varying(60),
);

DROP TABLE IF EXISTS TICKET;
CREATE TABLE TICKET (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    name character varying(100),
    description character varying(500),
    created_on SMALLDATETIME NOT NULL,
/*    desired_resolution_date SMALLDATETIME default NULL,*/
    desired_resolution_date DATE default NULL,
    assignee_id int,
    owner_id int,
    state_id character varying(32) NOT NULL,
    category_id int,
    urgency_id  character varying(8),
    approver_id int,
    foreign key (assignee_id) references USER(id),
    foreign key (owner_id) references USER(id),
    foreign key (approver_id) references USER(id),
    foreign key (category_id) references CATEGORY(id)
);


DROP TABLE IF EXISTS FEEDBACK;
CREATE TABLE FEEDBACK (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    user_id integer NOT NULL,
    rate integer NOT NULL,
    date SMALLDATETIME NOT NULL,
    text character varying(500),
    ticket_id integer NOT NULL,
    foreign key (user_id ) references USER(id)
);

DROP TABLE IF EXISTS ATTACHMENT;
CREATE TABLE ATTACHMENT (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    blob BLOB NOT NULL,
    ticket_id int,
    name character varying(255) NOT NULL,
    foreign key (ticket_id ) references TICKET(id),
);


DROP TABLE IF EXISTS HISTORY;
CREATE TABLE HISTORY (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    ticket_id int NOT NULL,
    date SMALLDATETIME NOT NULL,
    action character varying(255) NOT NULL,
    user_id int NOT NULL,
    description character varying(255) NOT NULL,
    foreign key (ticket_id ) references TICKET(id),
    foreign key (user_id ) references USER(id)
);



DROP TABLE IF EXISTS COMMENT;
CREATE TABLE COMMENT (
    id integer IDENTITY NOT NULL  PRIMARY KEY,
    user_id int NOT NULL,
    date SMALLDATETIME NOT NULL,
    text character varying(500) NOT NULL,
    ticket_id int NOT NULL,
    foreign key (ticket_id ) references TICKET(id),
    foreign key (user_id ) references USER(id)

);



INSERT INTO USER (first_name, last_name, role_id, email, password) values
('emp1FirstName', 'emp1LastName', 'EMPLOYEE', 'user1_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('emp2FirstName', 'emp1LastName', 'EMPLOYEE', 'user2_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('mgr1FirstName', 'mgr1LastName', 'MANAGER', 'manager1_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('mgr2FirstName', 'mgr2LastName', 'MANAGER', 'manager2_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('eng1FirstName', 'eng1LastName', 'ENGINEER', 'engineer1_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq'),
('eng2FirstName', 'eng2LastName', 'ENGINEER', 'engineer2_mogilev@yopmail.com','$2y$10$Te2n4tbhVDDwttLiZIefY.K.R3joW.wv6U6O43lgpqALS3NX9idlq');

insert into CATEGORY(NAME)  values 
('Application & Services'),
('Benefits & Paper Work'),
('Hardware & Software'),
('People Management'),
('Security & Access'),
('Workplaces & Facilities');

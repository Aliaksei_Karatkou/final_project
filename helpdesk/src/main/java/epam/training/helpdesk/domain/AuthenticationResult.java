package epam.training.helpdesk.domain;

/**
 * Authentication result class.
 */
public class AuthenticationResult {

    /**
     * User's role.
     */
    private Role role;

    public AuthenticationResult(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}

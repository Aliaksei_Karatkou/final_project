package epam.training.helpdesk.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * Attachment entity.
 */
@Entity
@Table(name = "Attachment")
public class Attachment implements Serializable {

    /**
     * Entity id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Attachment body.
     */
    @Lob
    @Column(name = "blob", columnDefinition = "BLOB")
    @NotNull
    private Byte[] blob;

    /**
     * Ticket with current attachment.
     */
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket attachmentTicket;


    /**
     * Attachment name.
     */
    @Column(name = "name")
    @NotNull
    private String name;

    /**
     * Constructor.
     * @param blob File body.
     * @param name File name.
     */
    public Attachment(Byte[] blob, String name) {
        this.blob = blob;
        this.name = name;
    }

    /**
     * Constructor.
     */
    public Attachment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer idToSet) {
        id = idToSet;
    }

    public Byte[] getBlob() {
        return blob;
    }

    public void setBlob(Byte[] blobToSet) {
        blob = blobToSet;
    }

    public Ticket getAttachmentTicket() {
        return attachmentTicket;
    }

    public void setAttachmentTicket(Ticket attachmentTicketToSet) {
        attachmentTicket = attachmentTicketToSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameToSet) {
        name = nameToSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Attachment that = (Attachment) o;
        return id.equals(that.id)
                && Arrays.equals(blob, that.blob)
                && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name);
        result = 31 * result + Arrays.hashCode(blob);
        return result;
    }
}

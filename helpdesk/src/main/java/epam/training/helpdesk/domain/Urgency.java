package epam.training.helpdesk.domain;

/**
 * Available urgency levels.
 */
public enum Urgency {

    /**
     * Low urgency.
     */
    LOW,

    /**
     * Average urgency.
     */
    AVERAGE,

    /**
     * High urgency.
     */
    HIGH,

    /**
     * Critical urgency.
     */
    CRITICAL
}

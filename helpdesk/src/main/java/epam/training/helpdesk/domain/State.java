package epam.training.helpdesk.domain;

/**
 * Ticket states.
 */
public enum State {

    /**
     * "Draft" ticket state.
     */
    DRAFT,

    /**
     * "New" ticket state.
     */
    NEW,

    /**
     * "Approved" ticket state.
     */
    APPROVED,

    /**
     * "Declined" ticket state.
     */
    DECLINED,

    /**
     * "In progress" ticket state.
     */
    IN_PROGRESS,

    /**
     * "Done" ticket state.
     */
    DONE,

    /**
     * "Cancelled" ticket state.
     */
    CANCELLED
}

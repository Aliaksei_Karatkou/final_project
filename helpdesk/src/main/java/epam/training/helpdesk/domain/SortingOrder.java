package epam.training.helpdesk.domain;

import java.util.Objects;

/**
 * Class to store sorting order.
 */
public class SortingOrder {

    /**
     * Sorting field.
     */
    private SortingField field;

    /**
     * Sorting trend.
     */
    private SortingTrend trend;

    /**
     * Constructor.
     * @param field
     * @param trend
     */
    public SortingOrder(SortingField field, SortingTrend trend) {
        this.field = field;
        this.trend = trend;
    }

    public SortingField getField() {
        return field;
    }

    public void setField(SortingField field) {
        this.field = field;
    }

    public SortingTrend getTrend() {
        return trend;
    }

    public void setTrend(SortingTrend trend) {
        this.trend = trend;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortingOrder that = (SortingOrder) o;
        return field == that.field &&
                trend == that.trend;
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, trend);
    }
}

package epam.training.helpdesk.domain;

/**
 * Actions enumeration.
 */
public enum Action {

    /**
     * Ticket created.
     */
    ACTION_TICKET_CREATED("Ticket is created"),

    /**
     * Ticket is edited.
     */
    ACTION_TICKET_EDITED("Ticket is edited"),

    /**
     * Ticket status changed.
     */
    ACTION_TICKET_CHANGED("Ticket status is changed"),

    /**
     * File is attachet to the ticket.
     */
    ACTION_FILE_ATTACHED("File is attached"),

    /**
     * File was removed from the ticket.
     */
    ACTION_FILE_REMOVED("File is removed");

    /**
     * Action text that is corresponding to the action enum item.
     */
    private final String actionText;

    /**
     * Constructor.
     *
     * @param actionText Action text that corresponds to the enum item.
     */
    private Action(String actionText) {
        this.actionText = actionText;
    }

    /**
     * Get action text.
     *
     * @return action text.
     */
    public String getActionText() {
        return actionText;
    }
}

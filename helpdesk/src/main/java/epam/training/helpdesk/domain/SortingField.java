package epam.training.helpdesk.domain;

/**
 * Class to store tickets sorting field.
 */
public enum SortingField {

    /**
     * Default sorting order.
     */
    SORT_DEFAULT(""),

    /**
     * Sorting field for sorting by id.
     */
    SORT_BY_ID("id"),

    /**
     * Sorting field for sorting by name.
     */
    SORT_BY_NAME("name"),

    /**
     * Sorting order by desired resolution date.
     */
    SORT_BY_DESIRED_DATE("desiredResolutionDate"),

    /**
     * Sorting field for sorting by urgency.
     */
    SORT_BY_URGENCY("urgency"),

    /**
     * Sorting field for sorting by ticket status.
     */
    SORT_BY_STATUS("state");

    /**
     * Field name that corresponds to the chosen sorting field.
     */
    private final String fieldName;

    /**
     * Constructor.
     * @param fieldName Field name to set.
     */
    private SortingField(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Get sorting order field name.
     * @return Field name.
     */
    public String getFieldName() {
        return this.fieldName;
    }
}

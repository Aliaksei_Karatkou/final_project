package epam.training.helpdesk.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class to store allowed actions.
 * Each permission applies to one ticket.
 */
public class Permission implements Serializable {

    /**
     * Permission to submit the ticket.
     */
    private boolean submitAllowed;

    /**
     * Permission to cancel the ticket.
     */
    private boolean cancelAllowed;

    /**
     * Permission to approve the ticket.
     */
    private boolean approveAllowed;

    /**
     * Permission to decline the ticket.
     */
    private boolean declineAllowed;

    /**
     * Permission to do "assign to me" action.
     */
    private boolean assignToMeAllowed;

    /**
     * Permission to set ticket state "done" action.
     */
    private boolean doneAllowed;

    /**
     * Permission to set the ticket to 'done' state.
     */
    private boolean feedbackViewingAllowed;

    /**
     * Permission to view ticket feedback.
     */
    private boolean feedbackAdditionAllowed;

    public boolean isSubmitAllowed() {
        return submitAllowed;
    }

    public void setSubmitAllowed(boolean submitAllowed) {
        this.submitAllowed = submitAllowed;
    }

    public boolean isCancelAllowed() {
        return cancelAllowed;
    }

    public void setCancelAllowed(boolean cancelAllowed) {
        this.cancelAllowed = cancelAllowed;
    }

    public boolean isApproveAllowed() {
        return approveAllowed;
    }

    public void setApproveAllowed(boolean approveAllowed) {
        this.approveAllowed = approveAllowed;
    }

    public boolean isDeclineAllowed() {
        return declineAllowed;
    }

    public void setDeclineAllowed(boolean declineAllowed) {
        this.declineAllowed = declineAllowed;
    }

    public boolean isAssignToMeAllowed() {
        return assignToMeAllowed;
    }

    public void setAssignToMeAllowed(boolean assignToMeAllowed) {
        this.assignToMeAllowed = assignToMeAllowed;
    }

    public boolean isDoneAllowed() {
        return doneAllowed;
    }

    public void setDoneAllowed(boolean doneAllowed) {
        this.doneAllowed = doneAllowed;
    }

    public boolean isFeedbackViewingAllowed() {
        return feedbackViewingAllowed;
    }

    public void setFeedbackViewingAllowed(boolean feedbackViewingAllowed) {
        this.feedbackViewingAllowed = feedbackViewingAllowed;
    }

    public boolean isFeedbackAdditionAllowed() {
        return feedbackAdditionAllowed;
    }

    public void setFeedbackAdditionAllowed(boolean feedbackAdditionAllowed) {
        this.feedbackAdditionAllowed = feedbackAdditionAllowed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return submitAllowed == that.submitAllowed &&
                cancelAllowed == that.cancelAllowed &&
                approveAllowed == that.approveAllowed &&
                declineAllowed == that.declineAllowed &&
                assignToMeAllowed == that.assignToMeAllowed &&
                doneAllowed == that.doneAllowed &&
                feedbackViewingAllowed == that.feedbackViewingAllowed &&
                feedbackAdditionAllowed == that.feedbackAdditionAllowed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(submitAllowed, cancelAllowed, approveAllowed, declineAllowed, assignToMeAllowed, doneAllowed, feedbackViewingAllowed, feedbackAdditionAllowed);
    }
}

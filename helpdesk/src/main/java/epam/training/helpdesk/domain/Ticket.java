package epam.training.helpdesk.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Ticket entity.
 */
@Entity
@Table(name = "ticket")
public class Ticket implements Serializable {

    /**
     * Ticket Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Field "name".
     */
    @Column(name = "name")
    private String name;

    /**
     * Field "description".
     */
    @Column(name = "description")
    private String description;

    /**
     * Field "created on".
     * Stores ticket data and time creation.
     */
    @Column(name = "created_on")
    @NotNull
    private Timestamp createdOn;

    /**
     * Desired ticket resolution date.
     */
    @Column(name = "desired_resolution_date")
    @Temporal(TemporalType.DATE)
    private Date desiredResolutionDate;


    /**
     * Person who was assigned to the ticket execution.
     */
    @ManyToOne
    @JoinColumn(name = "assignee_id")
    private User assignee;


    /**
     * Ticket owner.
     */
    @ManyToOne
    @JoinColumn(name = "owner_id")
    @NotNull
    private User owner;

    /**
     * Ticket state.
     */
    @Column(name = "state_id")
    @NotNull
    @Enumerated(EnumType.STRING)
    private State state;

    /**
     * Ticket attachments.
     */
    @OneToMany(mappedBy = "attachmentTicket")
    private Set<Attachment> attachments;


    /**
     * Ticket category.
     */
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;


    /**
     * Ticket urgency.
     */
    @Column(name = "urgency_id")
    @Enumerated(EnumType.STRING)
    private Urgency urgency;

    /**
     * Ticket approver.
     */
    @ManyToOne
    @JoinColumn(name = "approver_id")
    private User approver;

    /**
     * History items of the ticket.
     */
    @OneToMany(mappedBy = "historyTicket")
    List<History> historyRecords;

    /**
     * Comments to the thicket.
     */
    @OneToMany(mappedBy = "commentedTicket")
    List<Comment> comments;


    /**
     * Ticket feedback.
     */
    @OneToOne(mappedBy = "feedbackedTicket")
    private Feedback feedback;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public User getApprover() {
        return approver;
    }

    public void setApprover(User approver) {
        this.approver = approver;
    }

    public List<History> getHistoryRecords() {
        return historyRecords;
    }

    public void setHistoryRecords(List<History> historyRecords) {
        this.historyRecords = historyRecords;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id.equals(ticket.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

package epam.training.helpdesk.domain;

/**
 * Roles enumeration.
 */
public enum Role {

    /**
     * Employee's role.
     */
    EMPLOYEE,

    /**
     * Manager's role.
     */
    MANAGER,

    /**
     * Engineer's role.
     */
    ENGINEER;
}

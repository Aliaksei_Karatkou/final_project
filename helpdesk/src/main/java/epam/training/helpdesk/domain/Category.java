package epam.training.helpdesk.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * Category entity.
 */
@Entity
@Table(name = "Category")
public class Category implements Serializable {

    /**
     * Category id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private int id;

    /**
     * Category name.
     */
    @Column(name = "name")
    @NotNull
    private String name;

    /**
     * Tickets with this category.
     */
    @JsonIgnore
    @OneToMany(mappedBy = "category")
    private Set<Ticket> tickets;

    public int getId() {
        return id;
    }

    public void setId(int idToSet) {
        id = idToSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameToSet) {
        name = nameToSet;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> ticketsToSet) {
        tickets = ticketsToSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        return id == category.id
                && Objects.equals(name, category.name)
                && Objects.equals(tickets, category.tickets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, tickets);
    }
}

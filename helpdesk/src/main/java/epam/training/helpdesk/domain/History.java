package epam.training.helpdesk.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * History item entity.
 */
@Entity
@Table(name = "History")
public class History implements Serializable {

    /**
     * History item id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Ticket that this comment applied to.
     */
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    @NotNull
    private Ticket historyTicket;

    /**
     * Action date.
     */
    @Column(name = "date")
    @NotNull
    private Timestamp date;

    /**
     * Action type.
     */
    @Column(name = "action")
    @NotNull
    private String action;

    /**
     * Acting user.
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User actingUser;

    /**
     * History item description.
     */
    @Column(name = "description")
    @NotNull
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Ticket getHistoryTicket() {
        return historyTicket;
    }

    public void setHistoryTicket(Ticket historyTicket) {
        this.historyTicket = historyTicket;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public User getActingUser() {
        return actingUser;
    }

    public void setActingUser(User actingUser) {
        this.actingUser = actingUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return Objects.equals(id, history.id) &&
                Objects.equals(historyTicket, history.historyTicket) &&
                Objects.equals(date, history.date) &&
                Objects.equals(action, history.action) &&
                Objects.equals(actingUser, history.actingUser) &&
                Objects.equals(description, history.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, historyTicket, date, action, actingUser, description);
    }
}

package epam.training.helpdesk.domain;

/**
 * Sorting trend.
 */
public enum SortingTrend {

    /**
     * Undefined sorting trend.
     */
    UNDEFINED,

    /**
     * Sort by ascending order.
     */
    ASCENDING,

    /**
     * Sort by descending order.
     */
    DESCENDING
}

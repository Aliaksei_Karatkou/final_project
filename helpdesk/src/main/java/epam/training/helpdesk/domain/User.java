package epam.training.helpdesk.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * User entity.
 */
@Entity
@Table(name = "User")
public class User implements UserDetails, Serializable {

    /**
     * User's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private Integer id;

    /**
     * First name.
     */
    @Column(name = "first_name")
    @NotNull
    private String firstName;

    /**
     * Last name.
     */
    @Column(name = "last_name")
    @NotNull
    private String lastName;

    /**
     * Role.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "role_id")
    @NotNull
    private Role role;

    /**
     * User's email.
     */
    @Column(name = "email")
    @NotNull
    private String email;

    /**
     * User's password.
     */
    @Column(name = "password")
    @NotNull
    private String password;

    /**
     * Assigned tickets.
     */
    @OneToMany(mappedBy = "assignee")
    private Set<Ticket> assignedTickets;

    /**
     * Set of Own tickets.
     */
    @OneToMany(mappedBy = "owner")
    private Set<Ticket> ownTickets;

    /**
     * Tickets were this user is approver.
     */
    @OneToMany(mappedBy = "approver")
    private Set<Ticket> approveTickets;

    /**
     * Tickets that were commented by this user.
     */
    @OneToMany(mappedBy = "commentator")
    private Set<Comment> comments;

    /**
     * Tickets in whose history presents this user.
     */
    @OneToMany(mappedBy = "actingUser")
    private List<History> activityHistory;

    /**
     * Tickets that have feedback by this user.
     */
    @OneToMany(mappedBy = "feedbackAuthor")
    private Set<Feedback> feedbacks;

    /**
     * Return user's authority.
     *
     * @return User's authority.
     */
    @Override
    public final Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + getRole().name()));
        return authorities;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public final boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public final boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public final boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public final boolean isEnabled() {
        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Ticket> getAssignedTickets() {
        return assignedTickets;
    }

    public void setAssignedTickets(Set<Ticket> assignedTickets) {
        this.assignedTickets = assignedTickets;
    }

    public Set<Ticket> getOwnTickets() {
        return ownTickets;
    }

    public void setOwnTickets(Set<Ticket> ownTickets) {
        this.ownTickets = ownTickets;
    }

    public Set<Ticket> getApproveTickets() {
        return approveTickets;
    }

    public void setApproveTickets(Set<Ticket> approveTickets) {
        this.approveTickets = approveTickets;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public List<History> getActivityHistory() {
        return activityHistory;
    }

    public void setActivityHistory(List<History> activityHistory) {
        this.activityHistory = activityHistory;
    }

    public Set<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(Set<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, role, email, password);
    }
}

package epam.training.helpdesk.config;

import epam.training.helpdesk.filter.CorsFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Security config class.
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Encryption strength.
     */
    private static final Integer STRENGTH = 10;

    /**
     * User details service.
     */
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Authentication provider.
     *
     * @return new authentication provider.
     */
    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider =
                new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    /**
     * BCrypt password encoder.
     *
     * @return new BCrypt password encoder.
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(STRENGTH);
    }

    /**
     * Configure to encrypt passwords.
     *
     * @param auth AuthenticationManagerBuilder object
     * @throws Exception if error occurs.
     */
    @Override
    protected final void configure(final AuthenticationManagerBuilder auth)
            throws Exception {
        auth.authenticationProvider(authProvider());
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    /**
     * Confirure web-access security.
     *
     * @param http HttpSecurity object.
     * @throws Exception if error occurs.
     */
    @Override
    protected final void configure(final HttpSecurity http) throws Exception {
        http
                .cors().configurationSource(
                request -> new CorsConfiguration()
                        .applyPermitDefaultValues())
                .and()
                .csrf().disable()
                .addFilterBefore(
                        new CorsFilter(), ChannelProcessingFilter.class)
                .authorizeRequests()
                .antMatchers("/", "/*", "/index.html", "/static/**")
                .permitAll()
                .antMatchers("/api/**").hasAnyAuthority("ROLE_EMPLOYEE", "ROLE_MANAGER", "ROLE_ENGINEER")
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
}
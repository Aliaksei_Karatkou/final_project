package epam.training.helpdesk.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Security init.
 */
public class SecurityInit
        extends AbstractSecurityWebApplicationInitializer {
}

package epam.training.helpdesk.config;

/**
 * Config for REST controllers.
 */
public class RestConfig {

    /**
     * Api prefix.
     */
    private static final String API_PREFIX_URL = "/helpdesk/api";

    /**
     * Api version.
     */
    private static final String API_VERSION = "/1.0.0";

    /**
     * Result prefix.
     */
    public static final String REST_API_URL_PREFIX = API_PREFIX_URL + API_VERSION;

}

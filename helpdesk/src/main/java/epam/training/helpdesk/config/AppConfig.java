package epam.training.helpdesk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Application config class.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "epam.training.helpdesk")
public class AppConfig {
    private static final Integer MAX_UPLOAD_FILE_SIZE = 5 * 1024 * 1024;

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(MAX_UPLOAD_FILE_SIZE);
        return multipartResolver;
    }
}

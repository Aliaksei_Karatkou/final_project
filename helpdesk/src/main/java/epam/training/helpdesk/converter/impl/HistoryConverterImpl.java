package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.HistoryConverter;
import epam.training.helpdesk.domain.History;
import epam.training.helpdesk.dto.history.HistoryDto;

/**
 * History converter implementation.
 */

public class HistoryConverterImpl implements HistoryConverter {

    /**
     * Convert history record entity to DTO.
     *
     * @param history History record entity to convert.
     * @return History DTO.
     */
    @Override
    public HistoryDto convertToDto(History history) {
        String username = history.getActingUser().getFirstName() + " " + history.getActingUser().getLastName();
        return new HistoryDto(history.getId(), history.getDate(), history.getAction(), username, history.getDescription());
    }
}

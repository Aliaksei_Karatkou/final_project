package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.user.UserDto;

/**
 * User converter interface.
 */
public interface UserConverter {

    /**
     * Convert user object to DTO.
     *
     * @param user User object to convert.
     * @return user DTO.
     */
    UserDto convertToDto(User user);
}

package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.AttachmentConverter;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.validator.controller.attachment.AttachmentValidator;
import org.springframework.stereotype.Component;

/**
 * Attachment converter implementation.
 */

@Component
public class AttachmentConverterImpl implements AttachmentConverter {

    /**
     * Attachment validator.
     */
    AttachmentValidator attachmentValidator;

    public AttachmentConverterImpl(AttachmentValidator attachmentValidator) {
        this.attachmentValidator = attachmentValidator;
    }

    /**
     * Convert attachment entity to DTO.
     *
     * @param attachment Attachment entity to convert.
     * @return Attachment DTO.
     */
    public AttachmentDto convertToDto(Attachment attachment) {
        return new AttachmentDto(attachment.getId(),
                attachment.getBlob(),
                attachment.getName());
    }

    /**
     * Convert attachment DTO to entity.
     *
     * @param attachmentDto Attachment DTO to convert.
     * @return Attachment entity.
     */
    public Attachment convertFromDto(AttachmentDto attachmentDto) {
        attachmentValidator.validate((attachmentDto));
        return new Attachment(attachmentDto.getFileBody(), attachmentDto.getName());

    }
}

package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.dto.permission.PermissionDto;


/**
 * Permission converter interface.
 */
public interface PermissionConverter {

    /**
     * Convert permission entity to DTO.
     *
     * @param permission Permission entity to convert.
     * @param ticketId   Ticket id.
     * @return permission DTO (including ticket id).
     */
    PermissionDto convertToDto(Permission permission, Integer ticketId);
}

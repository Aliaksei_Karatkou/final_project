package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.PermissionConverter;
import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.dto.permission.PermissionDto;
import org.springframework.stereotype.Component;

/**
 * Permission converter implementation.
 */

@Component
public class PermissionConverterImpl implements PermissionConverter {

    /**
     * Convert permission entity to DTO.
     *
     * @param permission Permission entity to convert.
     * @param ticketId   Ticket id.
     * @return permission DTO (including ticket id).
     */
    public PermissionDto convertToDto(Permission permission, Integer ticketId) {
        return new PermissionDto(ticketId,
                permission.isSubmitAllowed(),
                permission.isCancelAllowed(),
                permission.isApproveAllowed(),
                permission.isDeclineAllowed(),
                permission.isAssignToMeAllowed(),
                permission.isDoneAllowed(),
                permission.isFeedbackViewingAllowed(),
                permission.isFeedbackAdditionAllowed());
    }
}

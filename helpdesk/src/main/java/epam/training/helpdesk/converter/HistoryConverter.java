package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.History;
import epam.training.helpdesk.dto.history.HistoryDto;

/**
 * History converter interface.
 */
public interface HistoryConverter {

    /**
     * Convert history record entity to DTO.
     *
     * @param history History record entity to convert.
     * @return History DTO.
     */
    HistoryDto convertToDto(History history);
}

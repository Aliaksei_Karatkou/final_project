package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.UserConverter;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.user.UserDto;

/**
 * User converter implementation.
 */
public class UserConverterImpl implements UserConverter {

    /**
     * Convert user object to DTO.
     *
     * @param user User object to convert.
     * @return user DTO.
     */
    public UserDto convertToDto(User user) {
        return new UserDto(user.getId(), user.getFirstName(), user.getLastName());
    }
}

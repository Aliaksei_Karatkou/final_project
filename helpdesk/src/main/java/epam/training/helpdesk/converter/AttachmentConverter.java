package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.dto.attachment.AttachmentDto;

/**
 * Attachment converter interface.
 */

public interface AttachmentConverter {

    /**
     * Convert attachment entity to DTO.
     *
     * @param attachment Attachment entity to convert.
     * @return Attachment DTO.
     */
    AttachmentDto convertToDto(Attachment attachment);

    /**
     * Convert attachment DTO to entity.
     *
     * @param attachmentDto Attachment DTO to convert.
     * @return Attachment entity.
     */
    Attachment convertFromDto(AttachmentDto attachmentDto);
}

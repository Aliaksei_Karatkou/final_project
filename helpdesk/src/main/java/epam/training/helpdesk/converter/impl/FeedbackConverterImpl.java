package epam.training.helpdesk.converter.impl;

import epam.training.helpdesk.converter.FeedbackConverter;
import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.dto.feedback.FeedbackDto;
import org.springframework.stereotype.Component;

/**
 * Feedback converter implementation.
 */
@Component
public class FeedbackConverterImpl implements FeedbackConverter {

    /**
     * Empty string.
     */
    private static final String EMPTY_STRING = "";

    /**
     * Convert feedback from DTO to entity.
     *
     * @param dto Feedback dto to convert.
     * @return Feedback entity.
     */
    public Feedback convertFromDto(FeedbackDto dto) {
        Feedback feedback = new Feedback(dto.getRate(), dto.getText());
        if (feedback.getText() == null) {
            feedback.setText(EMPTY_STRING);
        }
        return feedback;
    }

    /**
     * Convert feedback from entity to DTO.
     *
     * @param feedback Feedback entity to convert.
     * @return Feedback DTO.
     */
    public FeedbackDto convertToDto(Feedback feedback) {
        return new FeedbackDto(feedback.getRate(), feedback.getText());
    }

}

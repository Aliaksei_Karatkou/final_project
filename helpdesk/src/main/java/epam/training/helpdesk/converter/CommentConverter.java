package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.dto.comment.CommentDto;

/**
 * Comment converter interface.
 */
public interface CommentConverter {

    /**
     * Convert Comment DTO to comment entity.
     *
     * @param dto Comment DTO to convert.
     * @return Comment entity.
     */
    Comment convertFromDto(CommentDto dto);

    /**
     * Convert comment to DTO.
     *
     * @param comment Comment entity to convert.
     * @return Comment DTO.
     */
    CommentDto convertToDto(Comment comment);
}

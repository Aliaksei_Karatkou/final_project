package epam.training.helpdesk.converter;

import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.dto.feedback.FeedbackDto;

/**
 * Feedback converter interface.
 */
public interface FeedbackConverter {

    /**
     * Convert feedback from DTO to entity.
     *
     * @param dto Feedback dto to convert.
     * @return Feedback entity.
     */
    Feedback convertFromDto(FeedbackDto dto);

    /**
     * Convert feedback from entity to DTO.
     *
     * @param feedback Feedback entity to convert.
     * @return Feedback DTO.
     */
    FeedbackDto convertToDto(Feedback feedback);
}

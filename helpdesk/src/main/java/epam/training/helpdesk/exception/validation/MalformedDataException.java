package epam.training.helpdesk.exception.validation;

/**
 * Exception to be thrown if data is malformed.
 */
public class MalformedDataException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public MalformedDataException(final String message) {
        super(message);
    }
}

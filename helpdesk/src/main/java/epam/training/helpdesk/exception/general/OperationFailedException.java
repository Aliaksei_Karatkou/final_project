package epam.training.helpdesk.exception.general;

/**
 * Operation failed exception.
 */
public class OperationFailedException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public OperationFailedException(final String message) {
        super(message);
    }
}


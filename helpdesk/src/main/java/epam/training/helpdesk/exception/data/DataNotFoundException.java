package epam.training.helpdesk.exception.data;

/**
 * Data not found exception.
 * Thrown if requested data not found.
 */
public class DataNotFoundException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public DataNotFoundException(final String message) {
        super(message);
    }
}

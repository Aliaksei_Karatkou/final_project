package epam.training.helpdesk.exception.authentication;

/**
 * Class of an exception to be thrown when user not found.
 */
public class UserNotFoundException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public UserNotFoundException(final String message) {
        super(message);
    }


}
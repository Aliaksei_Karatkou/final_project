package epam.training.helpdesk.exception.validation;

/**
 * Exception to be thrown if validation fails.
 */
public class ValidationException extends RuntimeException {
    /**
     * Throw exception method.
     *
     * @param message message to explain exception.
     */
    public ValidationException(final String message) {
        super(message);
    }
}
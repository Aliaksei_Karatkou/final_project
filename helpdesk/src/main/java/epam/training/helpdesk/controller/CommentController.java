package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.service.CommentService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static epam.training.helpdesk.config.RestConfig.REST_API_URL_PREFIX;

/**
 * Comments controller.
 */
@RestController
@RequestMapping(REST_API_URL_PREFIX + "/comments")
public class CommentController {

    /**
     * Comment service to use.
     */
    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * Create new comment.
     *
     * @param dto      Comment DTO.
     * @param authData User's authentication data.
     * @return Id of the added comment.
     */
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping(value = "/")
    public ResponseEntity<Integer> addComment(@RequestBody CommentDto dto, final Authentication authData) {
        return new ResponseEntity<>(commentService.addComment(dto, (User) authData.getPrincipal()), HttpStatus.CREATED);
    }

    /**
     * Get comments for the ticket.
     *
     * @param authData  User's authentication data.
     * @param id        ticket id
     * @param unlimited Mark to control comments count to return.
     *                  Must be not empty to get all comments.
     * @return List of the comments (DTO) to the ticket.
     */
    @GetMapping(value = "/tickets/{id}")
    public ResponseEntity<List<CommentDto>> getTicketComments(Authentication authData,
                                                              @NotBlank(message = "The id cannot be blank")
                                                              @PathVariable final Integer id,
                                                              @RequestParam(required = false, defaultValue = "") String unlimited) {
        return ResponseEntity.ok(commentService.getTicketComments(id, unlimited, (User) authData.getPrincipal()));
    }
}

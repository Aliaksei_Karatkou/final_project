package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.Urgency;
import epam.training.helpdesk.service.UrgencyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static epam.training.helpdesk.config.RestConfig.REST_API_URL_PREFIX;

/**
 * Urgency service.
 */
@RestController
@RequestMapping(REST_API_URL_PREFIX + "/urgency")
public class UrgencyController {

    /**
     * Urgency service.
     */
    private UrgencyService urgencyService;

    public UrgencyController(UrgencyService urgencyService) {
        this.urgencyService = urgencyService;
    }

    /**
     * Get list of the all available urgency values.
     *
     * @return List of the possible urgency values.
     */
    @GetMapping(value = "/")
    public ResponseEntity<Urgency[]> getUrgencyList() {
        return ResponseEntity.ok(urgencyService.getUrgencyList());
    }

}

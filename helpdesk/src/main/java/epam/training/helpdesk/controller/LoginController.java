package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.AuthenticationResult;
import epam.training.helpdesk.domain.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static epam.training.helpdesk.config.RestConfig.REST_API_URL_PREFIX;

/**
 * Controller to test authentication.
 */
@RestController
@RequestMapping(REST_API_URL_PREFIX + "/login")
public class LoginController {
    /**
     * Return response without any data (to confirm successful authentication).
     *
     * @return Empty response with HTTP status NO_CONTENT (204).
     */
    @GetMapping
    public ResponseEntity<AuthenticationResult> login(Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        AuthenticationResult authenticationResult = new AuthenticationResult(user.getRole());
        return new ResponseEntity<>(authenticationResult, HttpStatus.OK);
    }
}

package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentSimpleDto;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.dto.permission.PermissionDto;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.service.TicketService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static epam.training.helpdesk.config.RestConfig.REST_API_URL_PREFIX;

/**
 * Tickets controller.
 */
@RestController
@RequestMapping(REST_API_URL_PREFIX + "/tickets")
public class TicketController {

    /**
     * Ticket service.
     */
    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Get all visible tickets DTOs.
     *
     * @param authData Authentication data of the user who requests the information.
     * @param sort     Sort order.
     * @param filter   Filter.
     * @return List of the tickets DTOs.
     */
    @GetMapping(value = "/")
    public ResponseEntity<List<TicketDto>> getTickets(final Authentication authData,
                                                      @RequestParam(required = false, defaultValue = "default") String sort,
                                                      @RequestParam(required = false, defaultValue = "") String filter) {
        return ResponseEntity.ok(ticketService.getTicketsByAuthentication((User) authData.getPrincipal(), sort, false, filter));
    }

    /**
     * Get information about ticket.
     *
     * @param ticketId Ticket id.
     * @param authData Authentication data of the user who requests information.
     * @return Ticket information (DTO, if user has access to the ticket).
     */
    @GetMapping(value = "/{ticketId}")
    public ResponseEntity<TicketDto> getTicket(
            @NotBlank(message = "The id cannot be blank")
            @PathVariable final Integer ticketId,
            final Authentication authData) {
        return new ResponseEntity<>(ticketService.getTicketByUser((User) authData.getPrincipal(), ticketId), HttpStatus.OK);
    }

    /**
     * Get own tickets list.
     *
     * @param authData Authentication data of the user who requests the information.
     * @param sort     Sorting order.
     * @param filter   Fiter.
     * @return Tickets DTOs list.
     */
    @GetMapping(value = "my/")
    public ResponseEntity<List<TicketDto>> getOwnTickets(final Authentication authData,
                                                         @RequestParam(required = false, defaultValue = "default") String sort,
                                                         @RequestParam(required = false, defaultValue = "") String filter) {
        return ResponseEntity.ok(ticketService.getTicketsByAuthentication((User) authData.getPrincipal(), sort, true, filter));
    }

    /**
     * Add new ticket.
     *
     * @param authData  Authentication data of the user who creates the ticket.
     * @param ticketDto Ticket DTO.
     * @return Ticket id.
     */
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping(value = "/")
    public ResponseEntity<Integer> addTicket(Authentication authData,
                                             @RequestBody TicketDto ticketDto) {
        return new ResponseEntity<>(ticketService.addTicket((User) authData.getPrincipal(), ticketDto), HttpStatus.CREATED);
    }

    /**
     * Get ticket attachments list (id and filename).
     *
     * @param id Ticket id
     * @return Attachments list (only ids and filenames).
     */
    @GetMapping(value = "/attachments/{id}")
    public ResponseEntity<List<AttachmentSimpleDto>> getTicketFilelist(@NotBlank(message = "The id cannot be blank")
                                                                       @PathVariable final Integer id) {
        return new ResponseEntity<>(ticketService.getTicketFilelist(id), HttpStatus.OK);
    }

    /**
     * Modify ticket.
     *
     * @param authData  Authentication data of the user who modifies the ticket.
     * @param ticketId  Ticked id
     * @param ticketDto Ticket DTO.
     * @return HTTP Status OK (200).
     */
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PutMapping(value = "/{ticketId}")
    public ResponseEntity<Void> modifyTicket(Authentication authData,
                                             @NotBlank(message = "The id cannot be blank") @PathVariable final Integer ticketId,
                                             @RequestBody TicketDto ticketDto) {
        ticketService.modifyTicket((User) authData.getPrincipal(), ticketDto, ticketId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Get permissions list of the tickets that user is able to see.
     *
     * @param authData Authentication data of the user who requests the information.
     * @return Permissions list.
     */
    @GetMapping(value = "/permissions")
    public ResponseEntity<List<PermissionDto>> getPermissions(Authentication authData) {
        return new ResponseEntity<>(ticketService.getPermissions((User) authData.getPrincipal()), HttpStatus.OK);
    }

    /**
     * Cancel ticket.
     *
     * @param authData Authentication data of the user who requests the operation.
     * @param id       Ticket id.
     * @return HTTP Code NO_CONTENT.
     */
    @PutMapping(value = "/{id}/cancelService/")
    public ResponseEntity<Void> cancelTicket(Authentication authData,
                                             @NotBlank(message = "The id cannot be blank")
                                             @PathVariable final Integer id) {
        ticketService.cancelTicket(id, (User) authData.getPrincipal());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Submit ticket.
     *
     * @param authData Authentication data of the user who requests the operation.
     * @param id       Ticket id.
     * @return HTTP Code NO_CONTENT.
     */
    @PutMapping(value = "/{id}/submitService")
    public ResponseEntity<Void> submitTicket(Authentication authData,
                                             @NotBlank(message = "The id cannot be blank")
                                             @PathVariable final Integer id) {
        ticketService.submitTicket(id, (User) authData.getPrincipal());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Appovet ticket.
     *
     * @param authData Authentication data of the user who requests the operation.
     * @param id       Ticket id.
     * @return HTTP Code NO_CONTENT.
     */

    @PutMapping(value = "/{id}/approveService/")
    public ResponseEntity<Void> approveTicket(Authentication authData,
                                              @NotBlank(message = "The id cannot be blank")
                                              @PathVariable final Integer id) {
        ticketService.approveTicket(id, (User) authData.getPrincipal());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Decline ticket.
     *
     * @param authData Authentication data of the user who requests the operation.
     * @param id       Ticket id.
     * @return HTTP Code NO_CONTENT.
     */
    @PutMapping(value = "/{id}/declineService/")
    public ResponseEntity<Void> declineTicket(Authentication authData,
                                              @NotBlank(message = "The id cannot be blank")
                                              @PathVariable final Integer id) {
        ticketService.declineTicket(id, (User) authData.getPrincipal());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Submit assigning ticket to the requester.
     *
     * @param authData Authentication data of the user who requests the operation.
     * @param id       Ticket id.
     * @return HTTP Code NO_CONTENT.
     */
    @PutMapping(value = "/{id}/assignService/")
    public ResponseEntity<Void> assignToMeTicket(Authentication authData,
                                                 @NotBlank(message = "The id cannot be blank")
                                                 @PathVariable final Integer id) {
        ticketService.assignToMeTicket(id, (User) authData.getPrincipal());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Set ticket status to "DONE".
     *
     * @param authData Authentication data of the user who requests the operation.
     * @param id       Ticket id.
     * @return HTTP Code NO_CONTENT.
     */
    @PutMapping(value = "/{id}/completeService/")
    public ResponseEntity<Void> setTicketDoneState(Authentication authData,
                                                   @NotBlank(message = "The id cannot be blank")
                                                   @PathVariable final Integer id) {
        ticketService.setTicketStateDone(id, (User) authData.getPrincipal());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * Get history records of the ticket.
     *
     * @param authData  Authentication data of the user who requests the information.
     * @param id        Ticket id.
     * @param unlimited Mark to control history records count to return.
     *                  Must be not empty to get all history records.
     * @return List of the history DTOs items.
     */
    @GetMapping(value = "/histories/{id}")
    public ResponseEntity<List<HistoryDto>> getTicketHistory(
            Authentication authData,
            @NotBlank(message = "The id cannot be blank") @PathVariable final Integer id,
            @RequestParam(required = false, defaultValue = "") String unlimited) {

        return new ResponseEntity<>(ticketService.getTicketHistory((User) authData.getPrincipal(), id, unlimited), HttpStatus.OK);
    }

}

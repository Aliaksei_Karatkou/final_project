package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.service.FeedbackService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static epam.training.helpdesk.config.RestConfig.REST_API_URL_PREFIX;

/**
 * Feedback controller.
 */
@RestController
@RequestMapping(REST_API_URL_PREFIX + "/feedbacks")
public class FeedbackController {

    /**
     * Message if unable to add feedback.
     */
    private static final String UNABLE_TO_ADD_FEEDBACK_MESSAGE = "Unable to add feedback";

    /**
     * Feedback service.
     */
    private FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    /**
     * Add new feedback.
     *
     * @param authData    User's authentication data.
     * @param feedbackDto Feedback DTO to add.
     * @param ticketId    Ticket id which was "feedbacked".
     * @return Feedback id.
     */
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @PostMapping(value = "/{ticketId}")
    public ResponseEntity<Integer> addFeedback(Authentication authData,
                                               @RequestBody FeedbackDto feedbackDto,
                                               @NotBlank(message = "The id cannot be blank")
                                               @PathVariable final Integer ticketId) {
        Feedback feedback = feedbackService.addFeedback(feedbackDto, (User) authData.getPrincipal(), ticketId).orElseThrow(() -> new OperationFailedException(UNABLE_TO_ADD_FEEDBACK_MESSAGE));
        return new ResponseEntity<>(feedback.getId(), HttpStatus.CREATED);
    }

    /**
     * Get feedback to the ticket.
     *
     * @param authData User's authentication data.
     * @param ticketId Ticket id.
     * @return Feedback to the ticket. (If user has access to the ticket).
     */
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('MANAGER')")
    @GetMapping(value = "/{ticketId}")
    public ResponseEntity<FeedbackDto> getFeedback(Authentication authData,
                                                   @NotBlank(message = "The id cannot be blank")
                                                   @PathVariable final Integer ticketId) {
        FeedbackDto dto = feedbackService.getFeedback((User) authData.getPrincipal(), ticketId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}

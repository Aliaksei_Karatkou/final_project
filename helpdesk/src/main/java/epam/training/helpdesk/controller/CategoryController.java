package epam.training.helpdesk.controller;

import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.service.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static epam.training.helpdesk.config.RestConfig.REST_API_URL_PREFIX;

@RestController
@RequestMapping(REST_API_URL_PREFIX + "/categories")
public class CategoryController {

    /**
     * Category service.
     */
    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Get list of all categories.
     *
     * @return List of the categories.
     */
    @GetMapping(value = "/")
    public ResponseEntity<List<Category>> getCategories() {
        List<Category> categories = categoryService.getAllCategories();
        return ResponseEntity.ok(categories);
    }
}


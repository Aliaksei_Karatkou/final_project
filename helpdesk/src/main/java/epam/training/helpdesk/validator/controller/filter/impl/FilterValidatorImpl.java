package epam.training.helpdesk.validator.controller.filter.impl;

import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.filter.FilterValidator;
import org.springframework.stereotype.Component;

import static java.util.regex.Pattern.matches;

/**
 * Feedback validator implementation.
 */
@Component
public class FilterValidatorImpl implements FilterValidator {

    /**
     * Regexp with symbols that allowed to use.
     */
    private static final String FILTER_VALIDATION_REGEXP = "^[~\\.\"(),:;<>@\\[\\]!#$%&`\\*\\+\\-/=\\?^_'{}|a-zA-Z\\d ]*$";

    /**
     * Validation error message.
     */
    private static final String FILTER_VALIDATION_FAILED_MESSAGE = "Filter validation failed";

    /**
     * Validate filter.
     *
     * @param filter Filter to validate.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validate(String filter) {
        if (!matches(FILTER_VALIDATION_REGEXP, filter)) {
            throw new ValidationException(FILTER_VALIDATION_FAILED_MESSAGE);
        }
    }
}

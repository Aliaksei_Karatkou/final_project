package epam.training.helpdesk.validator.controller.attachment.impl;

import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.attachment.AttachmentValidator;
import org.springframework.stereotype.Component;

/**
 * Attachment validator implementation.
 */
@Component
public class AttachmentValidatorImpl implements AttachmentValidator {
    /**
     * Max attachment size.
     */
    private static final Integer MAX_ATTACHMENT_SIZE = 5 * 1024 * 1024;

    /**
     * Regexp with allowed file formats.
     */
    private static final String REGEXP_ALLOWED_EXTENTIONS = "(?i).*\\.(pdf|doc|docx|png|jpeg|jpg)$";

    /**
     * Message if validation fails.
     */
    private static final String VALIDATION_FAILED = "Attachment validation failed.";

    /**
     * Validate attachment size
     *
     * @param dto Attachment DTO.
     * @throws ValidationException if validation failed.
     */
    private void validateSize(AttachmentDto dto) {
        if (dto.getFileBody() == null || dto.getFileBody().length > MAX_ATTACHMENT_SIZE) {
            throw new ValidationException(VALIDATION_FAILED);
        }
    }

    /**
     * Validate attachment name
     *
     * @param attachmentDto AttachmentDTO to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateName(AttachmentDto attachmentDto) {
        if (attachmentDto.getName() == null ||
                !attachmentDto.getName().matches(REGEXP_ALLOWED_EXTENTIONS)) {
            throw new ValidationException(VALIDATION_FAILED);
        }
    }

    /**
     * Validate attachment dto.
     *
     * @param attachmentDto Attachment DTO to validate.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validate(AttachmentDto attachmentDto) {
        if (attachmentDto == null) {
            throw new ValidationException(VALIDATION_FAILED);
        }
        validateSize(attachmentDto);
        validateName(attachmentDto);
    }
}

package epam.training.helpdesk.validator.controller.sort.impl;

import epam.training.helpdesk.domain.SortingField;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.SortingTrend;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.sort.SortOrderValidator;
import org.springframework.stereotype.Component;

/**
 * Tickets sorting order validator implementation.
 */
@Component
public class SortOrderValidatorImpl implements SortOrderValidator {

    /**
     * Default sorting order parameter string.
     */
    private static final String ORDER_DEFAULT_STRING = "default";

    /**
     * Validation error message.
     */
    private static final String VALIDATION_FAILED_EXCEPTION_MESSAGE = "Failed validation of the sorting order";

    /**
     * Parameter string for sorting by id.
     */
    private static final String PARAMETER_SORT_BY_ID = "byid";

    /**
     * Parameter string for sorting by ticket name.
     */
    private static final String PARAMETER_SORT_BY_NAME = "byname";
    /**
     * Parameter string for sorting by desired resolution date.
     */
    private static final String PARAMETER_SORT_BY_DESIRED_DATE = "bydesireddate";

    /**
     * Parameter string for sorting by urgency.
     */
    private static final String PARAMETER_SORT_BY_URGENCY = "byurgency";

    /**
     * Parameter string for sorting by ticket status.
     */
    private static final String PARAMETER_SORT_BY_STATUS = "bystatus";

    /**
     * Validate sort order.
     *
     * @param order Sort order string.
     * @return Validated sorting order.
     * @throws ValidationException if validation failed.
     */
    @Override
    public SortingOrder validate(String order) {
        order = order.toLowerCase();
        SortingOrder resultOrder = new SortingOrder(SortingField.SORT_DEFAULT, SortingTrend.UNDEFINED);
        if (order.equalsIgnoreCase(ORDER_DEFAULT_STRING)) {
            resultOrder.setField(SortingField.SORT_DEFAULT);
            resultOrder.setTrend(SortingTrend.UNDEFINED);
        } else {
            if (order.length() < 2) {
                throw new ValidationException(VALIDATION_FAILED_EXCEPTION_MESSAGE);
            }
            char sign = order.charAt(0);
            if (sign == '-') {
                resultOrder.setTrend(SortingTrend.DESCENDING);
            } else if (sign == '+') {
                resultOrder.setTrend(SortingTrend.ASCENDING);
            } else {
                throw new ValidationException(VALIDATION_FAILED_EXCEPTION_MESSAGE);
            }
            String clearOrder = order.substring(1);
            switch (clearOrder) {
                case PARAMETER_SORT_BY_ID:
                    resultOrder.setField(SortingField.SORT_BY_ID);
                    break;
                case PARAMETER_SORT_BY_NAME:
                    resultOrder.setField(SortingField.SORT_BY_NAME);
                    break;
                case PARAMETER_SORT_BY_DESIRED_DATE:
                    resultOrder.setField(SortingField.SORT_BY_DESIRED_DATE);
                    break;
                case PARAMETER_SORT_BY_URGENCY:
                    resultOrder.setField(SortingField.SORT_BY_URGENCY);
                    break;
                case PARAMETER_SORT_BY_STATUS:
                    resultOrder.setField(SortingField.SORT_BY_STATUS);
                    break;
                default:
                    throw new ValidationException(VALIDATION_FAILED_EXCEPTION_MESSAGE);
            }
        }
        return resultOrder;
    }
}

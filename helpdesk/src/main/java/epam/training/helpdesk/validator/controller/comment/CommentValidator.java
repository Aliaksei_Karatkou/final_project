package epam.training.helpdesk.validator.controller.comment;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.validation.ValidationException;

/**
 * Comment validator interface.
 */
public interface CommentValidator {

    /**
     * Validate comment DTO
     *
     * @param dto  Comment DTO
     * @param user User who try to add comment.
     * @throws ValidationException if validation failed.
     */
    void validateDto(CommentDto dto, User user);
}

package epam.training.helpdesk.validator.controller.feedback.impl;

import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.validator.controller.feedback.FeedbackValidator;
import org.springframework.stereotype.Component;

/**
 * Feedback validator implementation.
 */

@Component
public class FeedbackValidatorImpl implements FeedbackValidator {

    /**
     * Mininal feedback rate value.
     */
    private static final Integer RATE_MIN_VALUE = 1;

    /**
     * Maximal feedback rate value.
     */
    private static final Integer RATE_MAX_VALUE = 5;

    /**
     * Feedback validation error message.
     */
    private static final String VALIDATION_FAILED_MESSAGE = "Feedback validation failed";

    /**
     * Max feedback text length.
     */
    private static final Integer MAX_TEXT_LENGTH = 500;

    /**
     * Validate feedback rate.
     * @param rate feedback rate.
     * @throws ValidationException if validation failed.
     */
    private void validateRate(Integer rate) {
        if (rate == null || rate < RATE_MIN_VALUE || rate > RATE_MAX_VALUE) {
            throw new ValidationException(VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate feedback text.
     * @param text Feedback text to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateText(String text) {
        if (text != null && text.length() > MAX_TEXT_LENGTH) {
            throw new ValidationException(VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate feedback DTO.
     * @param dto Feedback DTO to validate.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validate(FeedbackDto dto) {
        if (dto == null) {
            throw new ValidationException(VALIDATION_FAILED_MESSAGE);
        }
        validateRate(dto.getRate());
        validateText(dto.getText());
    }
}

package epam.training.helpdesk.validator.controller.attachment;

import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.exception.validation.ValidationException;

/**
 * Attachment validator interface.
 */

public interface AttachmentValidator {

    /**
     * Validate attachment dto.
     *
     * @param attachmentDto Attachment DTO to validate.
     * @throws ValidationException if validation failed.
     */
    void validate(AttachmentDto attachmentDto);
}

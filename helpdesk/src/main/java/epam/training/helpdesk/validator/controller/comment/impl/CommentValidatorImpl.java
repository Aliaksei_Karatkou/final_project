package epam.training.helpdesk.validator.controller.comment.impl;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.validation.MalformedDataException;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.comment.CommentValidator;
import org.springframework.stereotype.Component;

import static java.util.regex.Pattern.matches;

/**
 * Comment validator implementation.
 */
@Component
public class CommentValidatorImpl implements CommentValidator {

    /**
     * Regexp for comment text validation.
     */
    private static final String COMMENT_VALIDATION_REGEXP = "^[~\\.\"(),:;<>@\\[\\]!#$%&`\\*\\+\\-/=\\?^_'{}|a-zA-Z\\d ]*$";

    /**
     * Message to inform that comment validation failed.
     */
    private static final String COMMENT_VALIDATION_FAILED_MESSAGE = "Comment validation failed";

    /**
     * Max comment text length.
     */
    private static final Integer COMMENT_LENGTH_MAX = 500;

    /**
     * Min comment text length.
     */
    private static final Integer COMMENT_LENGTH_MIN = 1;

    /**
     * Ticket service repository.
     */
    private TicketService ticketService;

    public CommentValidatorImpl(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Validate comment text length.
     *
     * @param dto Comment dto to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateTextLength(CommentDto dto) {
        if ((dto.getText().length() < COMMENT_LENGTH_MIN) || dto.getText().length() > COMMENT_LENGTH_MAX) {
            throw new ValidationException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate comment text.
     *
     * @param text Comment text to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateTextChars(String text) {
        if (!matches(COMMENT_VALIDATION_REGEXP, text)) {
            throw new ValidationException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate ticket visibility.
     *
     * @param ticketId Ticket id.
     * @param user     ticket owner.
     * @throws ValidationException if validation failed.
     */
    private void validateTicket(Integer ticketId, User user) {
        if (!ticketService.isTicketVisible(ticketId, user)) {
            throw new ValidationException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate comment DTO
     *
     * @param dto  Comment DTO
     * @param user User who try to add comment.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validateDto(CommentDto dto, User user) {
        if (dto == null || dto.getText() == null || dto.getTicketId() == null) {
            throw new MalformedDataException(COMMENT_VALIDATION_FAILED_MESSAGE);
        }
        validateTextLength(dto);
        validateTextChars(dto.getText());
        validateTicket(dto.getTicketId(), user);
    }
}

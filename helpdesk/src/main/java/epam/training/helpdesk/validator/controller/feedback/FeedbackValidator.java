package epam.training.helpdesk.validator.controller.feedback;

import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.exception.validation.ValidationException;

/**
 * Feedback validator interface.
 */
public interface FeedbackValidator {

    /**
     * Validate feedback DTO.
     *
     * @param dto Feedback DTO to validate.
     * @throws ValidationException if validation failed.
     */
    void validate(FeedbackDto dto);
}

package epam.training.helpdesk.validator.controller.ticket;

import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.exception.validation.ValidationException;

/**
 * Ticket validator interface.
 */
public interface TicketValidator {

    /**
     * Validate ticket DTO.
     *
     * @param ticketDto Ticket DTO to validate.
     * @throws ValidationException if validation failed.
     */
    void validateDto(TicketDto ticketDto);

    /**
     * Validate ticket
     *
     * @param ticket Ticket to validate.
     * @throws ValidationException if validation failed.
     */
    void validate(Ticket ticket);
}

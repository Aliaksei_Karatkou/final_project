package epam.training.helpdesk.validator.controller.ticket.impl;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.exception.validation.MalformedDataException;
import epam.training.helpdesk.exception.validation.ValidationException;
import epam.training.helpdesk.service.AttachmentService;
import epam.training.helpdesk.service.CategoryService;
import epam.training.helpdesk.validator.controller.ticket.TicketValidator;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Set;

import static java.util.regex.Pattern.matches;

/**
 * Ticket validator implementation.
 */

@Component
public class TicketValidatorImpl implements TicketValidator {

    /**
     * Ticket validation failed error message.
     */
    private static final String TICKET_VALIDATION_FAILED_MESSAGE = "Ticket validation failed";

    /**
     * Regexp with allowed symbols in a ticket name.
     */
    private static final String NAME_VALIDATION_REGEXP = "^[~\\.\"(),:;<>@\\[\\]!#$%&`\\*\\+\\-/=\\?^_'{}|a-z\\d ]*$";

    /**
     * Max ticket name length.
     */
    private static final Integer NAME_LENGTH_MAX = 100;

    /**
     * Min ticket name length.
     */
    private static final Integer NAME_LENGTH_MIN = 1;

    /**
     * Attachment linking error message.
     */
    private static final String ATTACHMENT_LINKING_FAILED = "Cannot link attachment to the ticket";

    /**
     * Regexp with allowed symbols in a ticket description.
     */
    private static final String DESCRIPTION_VALIDATION_REGEXP = "^[~\\.\"(),:;<>@\\[\\]!#$%&`\\*\\+\\-/=\\?^_'{}|a-zA-Z\\d ]*$";

    /**
     * Max description text length.
     */
    private static final Integer DESCRIPTION_LENGTH_MAX = 500;

    /**
     * Min description text length.
     */
    private static final Integer DESCRIPTION_LENGTH_MIN = 0;

    /**
     * Attachment service.
     */
    private AttachmentService attachmentService;

    /**
     * Category service.
     */
    private CategoryService categoryService;

    public TicketValidatorImpl(AttachmentService attachmentService, CategoryService categoryService) {
        this.attachmentService = attachmentService;
        this.categoryService = categoryService;
    }

    /**
     * Validate ticket name chars.
     *
     * @param name Ticket name to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateNameChars(String name) {
        if (!matches(NAME_VALIDATION_REGEXP, name)) {
            throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate ticket name length
     *
     * @param name               Ticket name to validate
     * @param emptyStringAllowed true if empty name is allowed.
     * @throws ValidationException if validation failed.
     */
    private void validateNameLength(String name, Boolean emptyStringAllowed) {
        if ((!emptyStringAllowed && name.length() < NAME_LENGTH_MIN) || name.length() > NAME_LENGTH_MAX) {
            throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Valiadte ticket name.
     *
     * @param dto Ticket dto to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateName(TicketDto dto) {
        String name = dto.getName();
        if (name == null) {
            throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
        validateNameLength(name, dto.getState() == State.DRAFT);
        validateNameChars(name);
    }

    /**
     * Validate desired attachments list.
     * Attachment must be unlinked or be linked to the ticket.
     *
     * @param attachments attachment Id
     * @param ticketId    ticket id
     * @throws ValidationException if validation failed.
     */
    private void validateDesiredAttachmentList(Set<Integer> attachments, Integer ticketId) {
        if (attachments != null) {
            for (Integer attachmentId : attachments) {
                Attachment attachment = attachmentService.getAttachment(attachmentId).orElseThrow(() -> new NoSuchElementException(ATTACHMENT_LINKING_FAILED));
                if (attachment.getAttachmentTicket() != null && !attachment.getAttachmentTicket().getId().equals(ticketId)) {
                    throw new ValidationException(ATTACHMENT_LINKING_FAILED);
                }
            }
        }
    }

    /**
     * Validate ticket description length.
     *
     * @param description Description to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateDescriptionLength(String description) {
        if (description.length() < DESCRIPTION_LENGTH_MIN || description.length() > DESCRIPTION_LENGTH_MAX) {
            throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate ticket description chars.
     *
     * @param description description to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateDescriptionChars(String description) {
        if (!matches(DESCRIPTION_VALIDATION_REGEXP, description)) {
            throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
    }

    /**
     * Validate ticket description DTO
     *
     * @param ticketDto ticket DTO to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateDescription(TicketDto ticketDto) {
        if (ticketDto.getDescription() != null) {
            validateDescriptionLength(ticketDto.getDescription());
            validateDescriptionChars(ticketDto.getDescription());
        }
    }

    /**
     * Validate ticket category
     *
     * @param categoryId Ticket category id to validate.
     * @throws ValidationException if validation failed.
     */
    private void validateCategory(Integer categoryId) {
        if (categoryId != null) {
            categoryService.getCategory(categoryId).orElseThrow(() -> new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE));
        }
    }

    /**
     * Validate ticket desired resolution date.
     *
     * @param date Date from ticket DTO.
     * @throws ValidationException if validation failed.
     */
    private void validateDesiredResolutionDate(Date date) {
        if (date != null) {
            Date dateDto = new Date(date.getTime());
            LocalDate localDateDto = dateDto.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate localDateNow = LocalDate.now();
            if (localDateNow.isAfter(localDateDto)) {
                throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
            }
        }
    }

    /**
     * Validate ticket DTO.
     *
     * @param ticketDto Ticket DTO to validate.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validateDto(TicketDto ticketDto) {
        if (ticketDto == null || ticketDto.getState() == null)
            throw new MalformedDataException(TICKET_VALIDATION_FAILED_MESSAGE);
        if ((ticketDto.getState() != State.DRAFT)
                && (ticketDto.getName() == null
                || ticketDto.getCategoryId() == null
                || ticketDto.getUrgency() == null)) {
            throw new MalformedDataException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
        validateName(ticketDto);
        validateDescription(ticketDto);
        validateCategory(ticketDto.getCategoryId());
        validateDesiredResolutionDate(ticketDto.getDesiredResolutionDate());
        validateDesiredAttachmentList(ticketDto.getAttachmentsId(), ticketDto.getId());
    }

    /**
     * Validate ticket
     *
     * @param ticket Ticket to validate.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validate(Ticket ticket) {
        if (ticket.getState() == null ||
                ticket.getCategory() == null ||
                ticket.getName() == null) {
            throw new ValidationException(TICKET_VALIDATION_FAILED_MESSAGE);
        }
        validateDesiredResolutionDate(ticket.getDesiredResolutionDate());
    }
}

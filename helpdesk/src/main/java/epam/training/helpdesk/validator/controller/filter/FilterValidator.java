package epam.training.helpdesk.validator.controller.filter;

import epam.training.helpdesk.exception.validation.ValidationException;

/**
 * Filter validator interface.
 */
public interface FilterValidator {

    /**
     * Validate filter.
     *
     * @param filter Filter to validate.
     * @throws ValidationException if validation failed.
     */
    void validate(String filter);
}

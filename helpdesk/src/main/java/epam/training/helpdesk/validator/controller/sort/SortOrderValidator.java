package epam.training.helpdesk.validator.controller.sort;

import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.exception.validation.ValidationException;

/**
 * Tickets sorting order validator interface.
 */
public interface SortOrderValidator {
    /**
     * Validate sort order.
     *
     * @param order Sort order string.
     * @return Validated sorting order.
     * @throws ValidationException if validation failed.
     */
    SortingOrder validate(String order);
}

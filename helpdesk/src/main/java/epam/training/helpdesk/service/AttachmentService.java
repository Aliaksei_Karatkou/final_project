package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

/**
 * Attachment service interface.
 */
public interface AttachmentService {


    /**
     * Get attachment by attachment Id and user(requester).
     *
     * @param attachmentId  Attachment id
     * @param requesterUser User who requests attachment.
     * @return an Optional of the Attachment
     */
    Optional<Attachment> getAttachment(Integer attachmentId, User requesterUser);

    /**
     * Get attachment by Id.
     *
     * @param attachmentId Id of the attachment to get.
     * @return Attachment object.
     */
    Optional<Attachment> getAttachment(Integer attachmentId);

    /**
     * Add attachment to the repository.
     *
     * @param attachment Attachment to add.
     * @return an Optional of the added attachment.
     */
    Optional<Attachment> addAttachment(Attachment attachment);

    /**
     * Delete unlinked attachment.
     *
     * @param id Attachment id to delete.
     */
    void deleteAttachmentChecked(Integer id);

    /**
     * Get attachment DTO.
     *
     * @param id   Attachment id.
     * @param user User (person that want to download attachment).
     * @return Attachment dto.
     */
    AttachmentDto getAttachmentDto(Integer id, final User user);

    /**
     * Add new attachment into the attachment repository.
     *
     * @param multipartFile Uploaded multipartFile file.
     * @return New attachment id.
     * @throws IOException if IOException happens.
     */
    Integer uploadNewFile(MultipartFile multipartFile) throws IOException;

    /**
     * Get attachment file name by attachment id.
     *
     * @param attachmentId Attachment id
     * @return Attachment file name.
     */
    Optional<String> getAttachmentName(Integer attachmentId);

    /**
     * Delete set of the attachments.
     *
     * @param attachments Ids of the attachments to delete.
     */
    void deleteAttachments(Set<Integer> attachments);

}

package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.exception.authentication.UserNotFoundException;
import epam.training.helpdesk.repository.UserRepository;
import epam.training.helpdesk.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    /**
     * User repository to use.
     */
    private UserRepository userRepository;

    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Get user object by id.
     *
     * @param id user id
     * @return User object with required id.
     */
    @Override
    public User getUserById(Integer id) {
        return userRepository.getUserById(id).orElseThrow(() ->
                new UserNotFoundException("Username not found "));
    }

    /**
     * Get all managers.
     *
     * @return Managers list.
     */
    @Override
    public List<User> getManagers() {
        return userRepository.getManagers();
    }

    /**
     * Get all engineers.
     *
     * @return Engineers list.
     */
    @Override
    public List<User> getEngineers() {
        return userRepository.getEngineers();
    }
}

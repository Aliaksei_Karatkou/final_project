package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.User;

import java.util.List;

/**
 * User service interface.
 */
public interface UserService {

    /**
     * Get user object by id.
     *
     * @param id user id
     * @return User object with required id.
     */
    User getUserById(Integer id);

    /**
     * Get all managers.
     *
     * @return Managers list.
     */
    List<User> getManagers();

    /**
     * Get all engineers.
     *
     * @return Engineers list.
     */
    List<User> getEngineers();
}

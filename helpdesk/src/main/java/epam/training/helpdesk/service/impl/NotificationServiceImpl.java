package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.exception.notification.NotificationFailedException;
import epam.training.helpdesk.service.NotificationService;
import epam.training.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
@PropertySource("classpath:notifier.properties")
public class NotificationServiceImpl implements NotificationService {

    /**
     * Ticket overview url (without ticket id).
     */
    @Value("${frontend.ticketOvervew.url}")
    private String ticketOverviewUrl;

    /**
     * Mail sender.
     */
    private JavaMailSender mailSender;

    /**
     * Template engine.
     */
    private TemplateEngine htmlTemplateEngine;

    /**
     * User service.
     */
    private UserService userService;

    public NotificationServiceImpl(JavaMailSender mailSender, TemplateEngine htmlTemplateEngine, UserService userService) {
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
        this.userService = userService;
    }

    /**
     * Send notification message.
     *
     * @param users    List of users for whom notification must be sent.
     * @param ticketId Id of the corresponding ticket.
     * @param subject  email subject.
     * @param template Notification template to use.
     */
    private void sendCustomNotification(final List<User> users, Integer ticketId,
                                        String subject,
                                        String template) {

        final Context ctx = new Context(Locale.getDefault());
        ctx.setVariable("ticketUrl", ticketOverviewUrl + ticketId);
        if (users.size() == 1) {
            ctx.setVariable("firstName", users.get(0).getFirstName());
            ctx.setVariable("lastName", users.get(0).getLastName());
        }
        ctx.setVariable("ticketId", ticketId);

        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
        final String htmlContent = this.htmlTemplateEngine.process(template, ctx);
        String[] recipients = new String[users.size()];
        recipients = users.stream().map(User::getEmail).collect(Collectors.toList()).toArray(recipients);
        try {
            message.setSubject(subject);
            message.setTo(recipients);
            message.setText(htmlContent, true);
        } catch (MessagingException ex) {
            throw new NotificationFailedException("Cannot send email to " + recipients);
        }
        this.mailSender.send(mimeMessage);
    }


    /**
     * Notify that ticket state was changed to NEW.
     *
     * @param ticketId Id of the ticket which state was changed.
     */
    @Override
    public void notifyTicketChangedStateToNew(Integer ticketId) {
        List<User> managers = userService.getManagers();
        sendCustomNotification(managers, ticketId, "New ticket for approval", "html/template01.html");
    }

    /**
     * Notify that ticket was approved.
     *
     * @param ticketId Id of the ticket that was approved.
     */
    @Override
    public void notifyTicketApproved(Integer ticketId, User ticketOwner) {
        List<User> recipients = userService.getEngineers();
        recipients.add(ticketOwner);
        sendCustomNotification(recipients, ticketId, "Ticket was approved", "html/template02.html");
    }

    /**
     * Notify that ticket was declined.
     *
     * @param ticketId Id of the ticket that was declined.
     */
    @Override
    public void notifyTicketDeclined(Integer ticketId, User ticketOwner) {
        sendCustomNotification(Arrays.asList(ticketOwner), ticketId, "Ticket was declined", "html/template03.html");
    }

    /**
     * Notify that ticket was cancelled by a manager.
     *
     * @param ticketId Id of the ticket that was cancelled by a manager.
     */
    @Override
    public void notifyTicketCancelledByManager(Integer ticketId, User ticketOwner) {
        sendCustomNotification(Arrays.asList(ticketOwner), ticketId, "Ticket was cancelled", "html/template04.html");
    }

    /**
     * Notify that ticket was cancelled by an engineer.
     *
     * @param ticketId Id of the ticket that was cancelled by an engineer.
     */
    @Override
    public void notifyTicketCancelledByEngineer(Integer ticketId, User ticketOwner, User ticketAppover) {
        List<User> recipients = new ArrayList<>();
        recipients.add(ticketOwner);
        if (!ticketOwner.equals(ticketAppover)) {
            recipients.add(ticketAppover);
        }
        sendCustomNotification(recipients, ticketId, "Ticket was cancelled", "html/template05.html");
    }

    /**
     * Notify that ticket state was changed to DONE.
     *
     * @param ticketId Id of the ticket which state was changed.
     */
    @Override
    public void notifyTicketDone(Integer ticketId, User ticketOwner) {
        sendCustomNotification(Arrays.asList(ticketOwner), ticketId, "Ticket was done", "html/template06.html");
    }

    /**
     * Notify that feedback was provided to the ticket
     *
     * @param ticketId Id of the ticket which feedback was provided to.
     */
    @Override
    public void notifyTicketFeedbackProvided(Integer ticketId, User ticketAssignee) {
        sendCustomNotification(Arrays.asList(ticketAssignee), ticketId, "Feedback was provided", "html/template07.html");
    }

}
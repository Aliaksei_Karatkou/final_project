package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Category;

import java.util.List;
import java.util.Optional;

/**
 * Category service interface.
 */
public interface CategoryService {

    /**
     * Get all categories list.
     *
     * @return All categories as list.
     */
    List<Category> getAllCategories();

    /**
     * Get category by id.
     *
     * @param id Category id.
     * @return an Optional of the Category object.
     */
    Optional<Category> getCategory(Integer id);
}

package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;

import java.util.List;

/**
 * Comment service interface.
 */
public interface CommentService {

    /**
     * Add comment.
     *
     * @param commentDto Comment DTO with data to add.
     * @param user       User who adds the comment.
     * @return New comment id.
     */
    Integer addComment(CommentDto commentDto, User user);

    /**
     * Get comments for the ticket by ticket id.
     *
     * @param id            Ticket id.
     * @param unlimitedMark unlimitedMark comments.
     *                      Must be not empty to get all comments.
     * @return List of the comment DTOs.
     */
    List<CommentDto> getTicketComments(final Integer id, String unlimitedMark, User user);
}

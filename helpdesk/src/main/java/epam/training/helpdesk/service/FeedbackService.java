package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.feedback.FeedbackDto;

import java.util.Optional;

/**
 * Feedback service interface.
 */

public interface FeedbackService {

    /**
     * Add feedback.
     * @param dto Feedback DTO to add.
     * @param user User who performs the operation.
     * @param ticketId Ticket id to which feedback must be added.
     * @return An Optional of added feedback.
     */
    Optional<Feedback> addFeedback(FeedbackDto dto, User user, Integer ticketId);

    /**
     * Get feedback DTO to the ticket.
     *
     * @param user     User who requests the feedback.
     * @param ticketId Ticket id.
     * @return Feedback DTO.
     */
    FeedbackDto getFeedback(User user, Integer ticketId);
}

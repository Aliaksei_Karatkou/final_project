package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.repository.CategoryRepository;
import epam.training.helpdesk.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Category service implementation.
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    /**
     * Category repository to use.
     */
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    /**
     * Get all categories list.
     *
     * @return All categories as list.
     */
    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.getAllCategories();
    }

    /**
     * Get category by id.
     *
     * @param id Category id.
     * @return an Optional of the Category object.
     */
    @Override
    public Optional<Category> getCategory(Integer id) {
        return categoryRepository.getCategory(id);
    }
}
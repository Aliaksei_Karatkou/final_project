package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.history.HistoryDto;

import java.util.List;

/**
 * History service interface.
 */
public interface HistoryService {

    /**
     * Get ticket history DTO by ticket id with limit.
     *
     * @param ticketId      Ticket id
     * @param unlimitedMark unlimitedMark for history items.
     *                      Must be not empty to get all history items.
     * @return List of the HistoryDto objects.
     */
    List<HistoryDto> getTicketHistory(final Integer ticketId, String unlimitedMark);

    /**
     * Add history record.
     *
     * @param user        Acting user.
     * @param ticket      Related ticket.
     * @param action      Action.
     * @param description Additional string to add to the action.
     */
    void addHistory(User user, Ticket ticket, Action action, String description);
}

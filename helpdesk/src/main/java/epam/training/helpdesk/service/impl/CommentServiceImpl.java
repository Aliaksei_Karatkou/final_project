package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.CommentConverter;
import epam.training.helpdesk.domain.Comment;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.comment.CommentDto;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.repository.CommentRepository;
import epam.training.helpdesk.service.CommentService;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.comment.CommentValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Comment service implementation.
 */
@Service
public class CommentServiceImpl implements CommentService {

    /**
     * Message to inform that ticket not found or access denied.
     */
    private static final String TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED = "Ticket not found or access denied";

    /**
     * Message to inform that unable to save the comment.
     */
    private static final String UNABLE_TO_SAVE_COMMENT_MESSAGE = "Unable to save the comment";

    /**
     * Default comments limit.
     */
    private static final Integer DEFAULT_ITEMS_LIMIT = 5;

    /**
     * Comment repository.
     */
    private final CommentRepository commentRepository;

    /**
     * Comment validator.
     */
    private final CommentValidator validator;

    /**
     * Comment converter.
     */
    private final CommentConverter converter;

    /**
     * Ticket service.
     */
    private final TicketService ticketService;

    public CommentServiceImpl(CommentRepository commentRepository, CommentValidator validator, CommentConverter converter, TicketService ticketService) {
        this.commentRepository = commentRepository;
        this.validator = validator;
        this.converter = converter;
        this.ticketService = ticketService;
    }

    /**
     * Add comment.
     *
     * @param commentDto Comment DTO with data to add.
     * @param user       User who adds the comment.
     * @return New comment id.
     * @throws OperationFailedException if unable to add comment.
     */
    @Override
    @Transactional
    public Integer addComment(CommentDto commentDto, User user) {
        commentDto.setDate(new Timestamp(System.currentTimeMillis()));
        commentDto.setUserId(user.getId());
        validator.validateDto(commentDto, user);
        Comment comment = converter.convertFromDto(commentDto);
        comment = commentRepository.addComment(comment).orElseThrow(() -> new OperationFailedException(UNABLE_TO_SAVE_COMMENT_MESSAGE));
        return comment.getId();
    }

    /**
     * Get comments for the ticket by ticket id.
     *
     * @param id            Ticket id.
     * @param unlimitedMark Mark to get all comments.
     *                      Must be not empty to get all comments.
     * @param user          User who request comment.
     * @return List of the comment DTOs.
     * @throws DataNotFoundException if ticket not doesn't exist or is not visible to the user.
     */
    @Override
    public List<CommentDto> getTicketComments(final Integer id, String unlimitedMark, User user) {
        Integer limit = DEFAULT_ITEMS_LIMIT;
        if (unlimitedMark != null && !unlimitedMark.isEmpty()) {
            limit = 0;
        }
        if (!ticketService.isTicketVisible(id, user)) {
            throw new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED);
        }
        List<Comment> comments = commentRepository.getTicketComments(id, limit);
        return comments.stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
    }
}

package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.PermissionConverter;
import epam.training.helpdesk.converter.TicketConverter;
import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.Permission;
import epam.training.helpdesk.domain.Role;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentSimpleDto;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.dto.permission.PermissionDto;
import epam.training.helpdesk.dto.ticket.TicketDto;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.repository.TicketRepository;
import epam.training.helpdesk.service.AttachmentService;
import epam.training.helpdesk.service.HistoryService;
import epam.training.helpdesk.service.NotificationService;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.filter.FilterValidator;
import epam.training.helpdesk.validator.controller.sort.SortOrderValidator;
import epam.training.helpdesk.validator.controller.ticket.TicketValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TicketServiceImpl implements TicketService {

    /**
     * Error message if ticket not found.
     */
    private static final String TICKET_NOT_FOUND_MESSAGE = "Ticket not found";

    /**
     * Error message if access denied.
     */
    private static final String ACCESS_DENIED_MESSAGE = "Access denied";

    /**
     * Error message if ticket not found or access denied.
     */
    private static final String TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE = "Ticket not found or access denied";

    /**
     * Error message if new ticket DTO contains ticket id.
     */
    private static final String TICKET_ID_ASSIGNING_NOT_ALLOWED_MESSAGE = "You can't assign ticket number to new tickets";

    /**
     * Error message if user try to link already linked attachment.
     */
    private static final String ATTACHMENTS_REASSIGNING_NOT_ALLOWED_MESSAGE = "You can't attach files that were attached to another ticket";

    /**
     * Error message if user has illegal role.
     */
    private static final String ILLEGAL_ROLE_MESSAGE = "Illegal role";

    /**
     * Ticket repository.
     */
    private TicketRepository ticketRepository;

    /**
     * Ticket converter.
     */
    private TicketConverter ticketConverter;

    /**
     * Ticket validator.
     */
    private TicketValidator ticketValidator;

    /**
     * Tickets filter validator.
     */
    private FilterValidator filterValidator;

    /**
     * Attachments service.
     */
    private AttachmentService attachmentService;

    /**
     * Permission service.
     */
    private PermissionConverter permissionConverter;

    /**
     * History service.
     */
    private HistoryService historyService;

    /**
     * Notification service.
     */
    private NotificationService notificationService;

    /**
     * Sorting order validator.
     */
    private SortOrderValidator sortOrderValidator;

    public TicketServiceImpl(TicketRepository ticketRepository, TicketConverter ticketConverter, TicketValidator ticketValidator, FilterValidator filterValidator, AttachmentService attachmentService, PermissionConverter permissionConverter, HistoryService historyService, NotificationService notificationService, SortOrderValidator sortOrderValidator) {
        this.ticketRepository = ticketRepository;
        this.ticketConverter = ticketConverter;
        this.ticketValidator = ticketValidator;
        this.filterValidator = filterValidator;
        this.attachmentService = attachmentService;
        this.permissionConverter = permissionConverter;
        this.historyService = historyService;
        this.notificationService = notificationService;
        this.sortOrderValidator = sortOrderValidator;
    }

    /**
     * Check if the ticket is visible to the user.
     *
     * @param ticketId Ticket id
     * @param user     User
     * @return true if ticket is visible, false otherwise.
     */
    @Override
    public boolean isTicketVisible(Integer ticketId, User user) {
        return ticketRepository.isTicketVisible(ticketId, user);
    }

    /**
     * Get ticket for user.
     * Performs check for visibility by user.
     *
     * @param ticketId Requested ticket id.
     * @param user     User (requester).
     * @return Optional(ticket) if ticket exists and user can see this ticket.
     * Otherwise return value is Optional(null).
     */
    @Override
    public Optional<Ticket> getTicketChecked(Integer ticketId, User user) {
        return ticketRepository.getTicketChecked(ticketId, user);
    }


    /**
     * Get allowed to view user's tickets ids.
     *
     * @param user User
     * @return Tickets ids.
     */
    @Override
    @Transactional
    public List<Integer> getTicketsIdByUser(User user) {
        return ticketRepository.getAllowedTicketsId(user);
    }

    /**
     * Add ticket to the repository.
     *
     * @param ticket Ticket to add.
     * @return Added ticket.
     */
    public Optional<Ticket> addTicket(Ticket ticket) {
        return ticketRepository.addTicket(ticket);
    }

    /**
     * Get ticket by id.
     *
     * @param id Ticket id.
     * @return Optional(ticket) if ticket exists, otherwise Optional(null).
     */
    @Override
    @Transactional
    public Optional<Ticket> getTicket(Integer id) {
        return ticketRepository.getTicket(id);
    }

    /**
     * Get tickets, visible by user.
     *
     * @param user                User.
     * @param sortOrderParam      Tickets sorting order.
     * @param personalTicketsOnly True if "My tickets" filter must be applied.
     * @param filter              Searching filter
     * @return Tickets list.
     */
    @Override
    @Transactional
    public List<TicketDto> getTicketsByAuthentication(User user, String sortOrderParam, Boolean personalTicketsOnly, String filter) {
        filterValidator.validate(filter);
        SortingOrder sortingOrder = sortOrderValidator.validate(sortOrderParam);
        List<Ticket> tickets = ticketRepository.getTicketsByUser(user, sortingOrder, personalTicketsOnly, filter);
        return tickets.stream()
                .map(ticketConverter::convertToDto)
                .collect(Collectors.toList());
    }


    /**
     * Add new ticket using ticket DTO
     *
     * @param user      User.
     * @param ticketDto Ticket DTO
     * @return Added ticket id.
     * @throws AccessDeniedException    if ticketDto contains ticket id.
     * @throws OperationFailedException if unable to create ticket.
     */
    @Override
    @Transactional
    public Integer addTicket(User user, TicketDto ticketDto) {
        ticketDto.setOwnerId(user.getId());
        ticketValidator.validateDto(ticketDto);

        Ticket ticketFromDto = new Ticket();
        ticketConverter.convertFromDto(ticketDto, ticketFromDto);
        ticketFromDto.setOwner(user);
        ticketFromDto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
        if (ticketFromDto.getId() != null) {
            throw new AccessDeniedException(TICKET_ID_ASSIGNING_NOT_ALLOWED_MESSAGE);
        }
        Set<Integer> attachmentIds = ticketDto.getAttachmentsId();
        ticketDto.setAttachmentsId(new HashSet<Integer>());
        Ticket ticket = addTicket(ticketFromDto).orElseThrow(() -> new OperationFailedException("Cannot create ticket"));
        historyService.addHistory(user, ticket, Action.ACTION_TICKET_CREATED, "");

        linkAttachments(attachmentIds, ticket.getId(), user);
        return ticket.getId();
    }


    /**
     * Link attachmets set to the ticket.
     *
     * @param attachments Attachmets Ids set to link.
     * @param ticketId    ticket id to link with.
     * @param user        user that requests operation.
     * @throws DataNotFoundException if ticket not found.
     */
    private void linkAttachments(Set<Integer> attachments, Integer ticketId, User user) {
        ticketRepository.linkAttachments(attachments, ticketId);
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        for (Integer attachmentId : attachments) {
            historyService.addHistory(user, ticket, Action.ACTION_FILE_ATTACHED, ": \"" + attachmentService.getAttachmentName(attachmentId).get() + "\"");
        }
    }

    /**
     * Unlink (delete) attachment from the ticket.
     * Requested attachment will be deleted.
     * Attachment must not be linked to any ticket or it must be linked to the requested ticket.
     *
     * @param attachments attachments ids to delete
     * @param ticketId    ticket id.
     */
    private void deleteAttachments(Set<Integer> attachments, Integer ticketId, User user) {
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        for (Integer attachmentId : attachments) {
            historyService.addHistory(user, ticket, Action.ACTION_FILE_REMOVED, ": \"" + attachmentService.getAttachmentName(attachmentId).get() + "\"");
        }
        attachmentService.deleteAttachments(attachments);
    }


    /**
     * Check that all attachments in the set are not linked.
     *
     * @param attachmentsList Set with attachments ids.
     * @return true if checking was successful, false otherwise.
     */
    private boolean checkAllAttchementsAreUnlinked(Set<Integer> attachmentsList) {
        return (attachmentsList.size() == ticketRepository.getUnlinkedAttachmentCount(attachmentsList));
    }

    /**
     * Modify ticket using ticket DTO.
     *
     * @param user      User that requests operation.
     * @param ticketDto Ticket dto.
     * @param ticketId  Id of the ticket that must be modified.
     * @throws DataNotFoundException if user cannot see requested ticket.
     * @throws AccessDeniedException if ticket not is the "DRAFT" state,
     *                               or user is not ticket owner,
     *                               or user attempts to link already linked attachments to the ticket.
     */
    @Override
    @Transactional
    public void modifyTicket(User user, TicketDto ticketDto, Integer ticketId) {
        ticketValidator.validateDto(ticketDto);
        Ticket ticket = ticketRepository.getTicketChecked(ticketId, user).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE));
        if (ticket.getState() != State.DRAFT || !ticket.getOwner().equals(user)) {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
        Set<Integer> prevAttaches = new HashSet<>(ticketRepository.getTicketAttachmentsIds(ticketId));

        State newState = ticketDto.getState();
        if (newState == State.NEW) {
            notificationService.notifyTicketChangedStateToNew(ticketId);
            changeTicketStatus(ticketId, user, State.NEW);
        }
        ticketConverter.convertFromDto(ticketDto, ticket);
        Set<Integer> dtoIdAttaches = new HashSet<>(ticketDto.getAttachmentsId());
        Set<Integer> newAttaches = new HashSet<>(dtoIdAttaches);
        newAttaches.removeAll(prevAttaches);
        Set<Integer> attachesToDelete = new HashSet<>(prevAttaches);
        attachesToDelete.removeAll(dtoIdAttaches);

        if (!checkAllAttchementsAreUnlinked(newAttaches)) {
            throw new AccessDeniedException(ATTACHMENTS_REASSIGNING_NOT_ALLOWED_MESSAGE);
        }
        linkAttachments(newAttaches, ticketId, user);
        deleteAttachments(attachesToDelete, ticketId, user);
        ticket.setName(ticketDto.getName());
        ticketRepository.updateTicket(ticket);
        historyService.addHistory(user, ticket, Action.ACTION_TICKET_EDITED, "");

    }

    /**
     * Get ticket DTO by ticket id and user (requester).
     * Performs check for visibility by user
     *
     * @param user     User.
     * @param ticketId Ticket id.
     * @return ticket DTO.
     */
    @Override
    @Transactional
    public TicketDto getTicketByUser(User user, Integer ticketId) {
        Ticket ticket = getTicketChecked(ticketId, user).orElseThrow(() -> new OperationFailedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE));
        return ticketConverter.convertToDto(ticket);
    }

    /**
     * Get list of attachments (id, file name) for the ticket by ticket Id.
     *
     * @param ticketId Ticket Id.
     * @return List of the AttachmentSimpleDto objects.
     * @throws DataNotFoundException if ticket not found.
     */
    @Override
    @Transactional
    public List<AttachmentSimpleDto> getTicketFilelist(Integer ticketId) {
        Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        Set<Attachment> attachments = ticket.getAttachments();
        List<AttachmentSimpleDto> filelist = new ArrayList<>();
        for (Attachment attachment : attachments) {
            filelist.add(new AttachmentSimpleDto(attachment.getId(), attachment.getName()));
        }
        return filelist;
    }

    /**
     * Get user's permission DTOs for tickets that are visible for the user.
     *
     * @param user User
     * @return List of permission DTOs.
     * @throws IllegalStateException if user has illegal role.
     */
    @Override
    @Transactional
    public List<PermissionDto> getPermissions(User user) {
        List<Integer> visibleTickets = getTicketsIdByUser(user);
        int hashMapCapacity = (int) (1 + (visibleTickets.size() / 0.75));
        Map<Integer, Integer> ticketIdToArrayIndexMap = new HashMap<>(hashMapCapacity);
        Permission[] permissions = new Permission[visibleTickets.size()];
        for (int i = 0; i < permissions.length; i++) {
            permissions[i] = new Permission();
            ticketIdToArrayIndexMap.put(visibleTickets.get(i), i);
        }
        List<Integer> ticketsAllowedForSubmitAction = new ArrayList<>();
        List<Integer> ticketsAllowedForCancelAction;
        List<Integer> ticketsAllowedForDeclineAction = new ArrayList<>();
        List<Integer> ticketsAllowedForApproveAction = new ArrayList<>();
        List<Integer> ticketsAllowedForAssignToMeAction = new ArrayList<>();
        List<Integer> ticketsAllowedForSetDoneStateAction = new ArrayList<>();
        List<Integer> ticketsAllowedToViewFeedback = new ArrayList<>();
        List<Integer> ticketsAllowedForFeedbackAttaching = new ArrayList<>();
        Integer userId = user.getId();
        switch (user.getRole()) {
            case EMPLOYEE:
                ticketsAllowedForSubmitAction = getEmployeeActionSubmitAllowedTicketIds(userId, visibleTickets);
                ticketsAllowedForCancelAction = getEmployeeActionCancelAllowedTicketIds(userId, visibleTickets);
                ticketsAllowedToViewFeedback = getEmployeeAllowedActionViewFeedbackTicketIds(userId, visibleTickets);
                ticketsAllowedForFeedbackAttaching = getEmployeeAllowedActionAddFeedbackTicketsIds(userId, visibleTickets);
                break;
            case MANAGER:
                ticketsAllowedForCancelAction = getManagerAllowedActionCancelTicketIds(userId, visibleTickets);
                ticketsAllowedForSubmitAction = getManagerAllowedActionSubmitTicketIds(userId, visibleTickets);
                ticketsAllowedForApproveAction = getManagerAllowedActionApproveTicketIds(userId, visibleTickets);
                ticketsAllowedForDeclineAction = getManagerAllowedActionDeclineTicketIds(userId, visibleTickets);
                ticketsAllowedToViewFeedback = getManagerAllowedActionViewFeedbackTicketsIds(userId, visibleTickets);
                ticketsAllowedForFeedbackAttaching = getManagerAllowedActionAddFeedbackTicketsIds(userId, visibleTickets);
                break;
            case ENGINEER:
                ticketsAllowedForAssignToMeAction = getEngineerAllowedActionAssignToMeTicketIds(visibleTickets);
                ticketsAllowedForCancelAction = getEngineerAllowedActionCancelTicketIds(visibleTickets);
                ticketsAllowedForSetDoneStateAction = getEngineerAllowedActionSetDoneStateTicketIds(userId, visibleTickets);
                break;
            default:
                throw new IllegalStateException(ILLEGAL_ROLE_MESSAGE);

        }

        for (Integer ticketId : ticketsAllowedForSubmitAction) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setSubmitAllowed(true);
        }

        for (Integer ticketId : ticketsAllowedForCancelAction) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setCancelAllowed(true);
        }

        for (Integer ticketId : ticketsAllowedForDeclineAction) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setDeclineAllowed(true);
        }

        for (Integer ticketId : ticketsAllowedForApproveAction) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setApproveAllowed(true);
        }
        for (Integer ticketId : ticketsAllowedForAssignToMeAction) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setAssignToMeAllowed(true);
        }
        for (Integer ticketId : ticketsAllowedForSetDoneStateAction) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setDoneAllowed(true);
        }

        for (Integer ticketId : ticketsAllowedToViewFeedback) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setFeedbackViewingAllowed(true);
        }

        for (Integer ticketId : ticketsAllowedForFeedbackAttaching) {
            Integer permissionArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            permissions[permissionArrayIndex].setFeedbackAdditionAllowed(true);
        }

        List<PermissionDto> dtoList = new ArrayList<>();
        for (Integer ticketId : visibleTickets) {
            Integer permissionsArrayIndex = ticketIdToArrayIndexMap.get(ticketId);
            Permission permission = permissions[permissionsArrayIndex];
            dtoList.add(permissionConverter.convertToDto(permission, ticketId));
        }
        return dtoList;
    }

    /**
     * Cancel ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     * @throws AccessDeniedException if ticket not found or user hasn't appropriate permission.
     * @throws DataNotFoundException if unable to read the ticket.
     */
    @Override
    @Transactional
    public void cancelTicket(Integer ticketId, User user) {
        if (ticketRepository.checkUserIsAllowedToCancelTicket(user, ticketId)) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            updateApproverIfItsNecessary(ticket, user);
            updateAssigneeIfItsNecessary(ticket, user);
            changeTicketStatus(ticketId, user, State.CANCELLED);
            if (prevState == State.NEW) {
                notificationService.notifyTicketCancelledByManager(ticketId, ticket.getOwner());
            } else if (prevState == State.APPROVED) {
                notificationService.notifyTicketCancelledByEngineer(ticketId, ticket.getOwner(), ticket.getApprover());
            }
        } else {
            throw new AccessDeniedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
    }

    /**
     * Submit ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     * @throws AccessDeniedException if ticket not found or user hasn't appropriate permission.
     * @throws DataNotFoundException if unable to read the ticket.
     */
    @Override
    @Transactional
    public void submitTicket(Integer ticketId, User user) {
        if (ticketRepository.checkUserIsAllowedToSubmitTicket(user, ticketId)) {
            Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            ticketValidator.validate(ticket);
            State prevState = ticket.getState();
            changeTicketStatus(ticketId, user, State.NEW);
            if (prevState == State.DRAFT || prevState == State.DECLINED) {
                notificationService.notifyTicketChangedStateToNew(ticketId);
            }
        } else {
            throw new AccessDeniedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
    }

    /**
     * Approve ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     * @throws AccessDeniedException if ticket not found or user hasn't appropriate permission.
     * @throws DataNotFoundException if unable to read the ticket.
     */
    @Override
    @Transactional
    public void approveTicket(Integer ticketId, User user) {
        if (ticketRepository.checkUserIsAllowedToApproveTicket(user, ticketId)) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            updateApproverIfItsNecessary(ticket, user);
            ticketRepository.updateTicket(ticket);
            changeTicketStatus(ticketId, user, State.APPROVED);
            if (prevState == State.NEW) {
                notificationService.notifyTicketApproved(ticketId, ticket.getOwner());
            }
        } else {
            throw new AccessDeniedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
    }

    /**
     * Update approver (to the user) of the ticket if it necessary.
     *
     * @param ticket ticket id
     * @param user   user that does some operation with the ticket.
     */
    private void updateApproverIfItsNecessary(Ticket ticket, User user) {
        if (user.getRole() == Role.MANAGER) {
            ticket.setApprover(user);
            ticketRepository.updateTicket(ticket);
        }
    }

    /**
     * Update assignee (to the user) of the ticket if it necessary.
     *
     * @param ticket ticket id
     * @param user   user that does some operation with the ticket.
     */
    private void updateAssigneeIfItsNecessary(Ticket ticket, User user) {
        if (user.getRole() == Role.ENGINEER) {
            ticket.setAssignee(user);
            ticketRepository.updateTicket(ticket);
        }
    }

    /**
     * Decline ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     * @throws AccessDeniedException if ticket not found or user hasn't appropriate permission.
     * @throws DataNotFoundException if unable to read the ticket.
     */
    @Override
    @Transactional
    public void declineTicket(Integer ticketId, User user) {
        if (ticketRepository.checkUserIsAllowedToDeclineTicket(user, ticketId)) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            updateApproverIfItsNecessary(ticket, user);
            changeTicketStatus(ticketId, user, State.DECLINED);
            if (prevState == State.NEW) {
                notificationService.notifyTicketDeclined(ticketId, ticket.getOwner());
            }
        } else {
            throw new AccessDeniedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
    }

    /**
     * Make action "Assign to me" for the ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     * @throws AccessDeniedException if ticket not found or user hasn't appropriate permission.
     * @throws DataNotFoundException if unable to read the ticket.
     */
    @Override
    @Transactional
    public void assignToMeTicket(Integer ticketId, User user) {
        if (ticketRepository.checkUserIsAllowedToAssignToMeTicket(user, ticketId)) {
            Ticket ticket = ticketRepository.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            updateAssigneeIfItsNecessary(ticket, user);
            changeTicketStatus(ticketId, user, State.IN_PROGRESS);
        } else {
            throw new AccessDeniedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
    }

    /**
     * Change ticket state to "DONE" by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     * @throws AccessDeniedException if ticket not found or user hasn't appropriate permission.
     * @throws DataNotFoundException if unable to read the ticket.
     */
    @Override
    @Transactional
    public void setTicketStateDone(Integer ticketId, User user) {
        if (ticketRepository.checkUserIsAllowedToSetDoneStateTicket(user, ticketId)) {
            Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            changeTicketStatus(ticketId, user, State.DONE);
            if (prevState == State.IN_PROGRESS) {
                notificationService.notifyTicketDone(ticketId, ticket.getOwner());
            }
        } else {
            throw new AccessDeniedException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
    }

    /**
     * Change ticket status to new one.
     *
     * @param ticketId ticket id.
     * @param user     User that requests operation.
     * @param newState new ticket state.
     * @throws DataNotFoundException if ticket not found.
     */
    private void changeTicketStatus(Integer ticketId, User user, State newState) {
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        State prevState = ticket.getState();
        ticketRepository.changeTicketStatus(ticketId, newState);
        historyService.addHistory(user, ticket, Action.ACTION_TICKET_CHANGED,
                " from '" + prevState + "' to '" + newState + "'");
    }

    /**
     * Get ticket history DTOs by ticket id, user that requests history.
     *
     * @param user      User that requests history.
     * @param id        ticket id.
     * @param unlimited Mark that indicates how deep must be history.
     *                  If not null and not empty then history depth
     *                  must not be limited.
     * @return List of the HistoryDto objects with requested data.
     * @throws DataNotFoundException if ticket not found or not visible.
     */
    @Transactional
    public List<HistoryDto> getTicketHistory(User user, Integer id, String unlimited) {
        List<Integer> allowedToViewTickets = getTicketsIdByUser(user);
        if (!allowedToViewTickets.contains(id)) {
            throw new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE_OR_ACCESS_DENIED_MESSAGE);
        }
        return historyService.getTicketHistory(id, unlimited);
    }

    /**
     * Get employee's tickets ids that are allowed to be submitted by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEmployeeActionSubmitAllowedTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getEmployeeAllowedActionSubmitTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get employee's tickets ids that are allowed to be cancelled by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEmployeeActionCancelAllowedTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getEmployeeAllowedActionCancelTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get manager's tickets ids that are allowed to be submitted by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionSubmitTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getManagerAllowedActionSubmitTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get manager's tickets ids that are allowed to be approved by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionApproveTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getManagerAllowedActionApproveTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get manager's tickets ids that are allowed to be declined by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionDeclineTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getManagerAllowedActionDeclineTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get engineer's tickets ids that are allowed to be assigned to himself by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEngineerAllowedActionAssignToMeTicketIds(List<Integer> requestedTicketsIds) {
        return ticketRepository.getEngineerAllowedActionAssignToMeTicketIds(requestedTicketsIds);
    }

    /**
     * Get engineer's tickets ids that are allowed to be cancelled by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEngineerAllowedActionCancelTicketIds(List<Integer> requestedTicketsIds) {
        return ticketRepository.getEngineerAllowedActionCancelTicketIds(requestedTicketsIds);
    }

    /**
     * Get engineer's tickets ids that are allowed to be marked as done by the engineer.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEngineerAllowedActionSetDoneStateTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getEngineerAllowedActionSetDoneStateTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get manager's tickets ids that are allowed to be cancelled by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionCancelTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getManagerAllowedActionCancelTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get ids of the tickets, which feedback can be viewed by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEmployeeAllowedActionViewFeedbackTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getEmployeeAllowedActionViewFeedbackTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get ids of the tickets, which feedback can be viewed by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionViewFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getManagerAllowedActionViewFeedbackTicketsIds(userId, requestedTicketsIds);
    }

    /**
     * Get ids of the tickets, which feedback can be added by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEmployeeAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getEmployeeAllowedActionAddFeedbackTicketsIds(userId, requestedTicketsIds);
    }

    /**
     * Get ids of the tickets, that feedback can be added by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        return ticketRepository.getManagerAllowedActionAddFeedbackTicketsIds(userId, requestedTicketsIds);
    }

    /**
     * Check if user is allowed to view ticket feedback.
     *
     * @param user     User who requests feedback viewing.
     * @param ticketId Ticket id
     * @return true if user is allowed to view feedback to the ticket,
     * false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToViewFeedback(User user, Integer ticketId) {
        return ticketRepository.checkUserIsAllowedToViewFeedback(user, ticketId);
    }

    /**
     * Check if user is allowed to add feedback to the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToAddFeedback(User user, Integer ticketId) {
        return ticketRepository.checkUserIsAllowedToAddFeedback(user, ticketId);
    }
}

package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentSimpleDto;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.dto.permission.PermissionDto;
import epam.training.helpdesk.dto.ticket.TicketDto;

import java.util.List;
import java.util.Optional;

public interface TicketService {

    /**
     * Get tickets, visible by user.
     *
     * @param user                User.
     * @param sortOrderParam      Tickets sorting order.
     * @param personalTicketsOnly True if "My tickets" filter must be applied.
     * @param filter              Searching filter
     * @return Tickets list.
     */
    List<TicketDto> getTicketsByAuthentication(User user, String sortOrderParam, Boolean personalTicketsOnly, String filter);

    /**
     * Get allowed to view user's tickets ids.
     *
     * @param user User
     * @return Tickets ids.
     */
    List<Integer> getTicketsIdByUser(User user);

    /**
     * Check if the ticket is visible to the user.
     *
     * @param ticketId Ticket id
     * @param user     User
     * @return true if ticket is visible, false otherwise.
     */
    boolean isTicketVisible(Integer ticketId, User user);

    /**
     * Get ticket for user.
     * Performs check for visibility by user.
     *
     * @param ticketId Requested ticket id.
     * @param user     User (requester).
     * @return Optional(ticket) if ticket exists and user can see this ticket.
     * Otherwise return value is Optional(null).
     */
    Optional<Ticket> getTicketChecked(Integer ticketId, User user);

    /**
     * Add ticket to the repository.
     *
     * @param ticket Ticket to add.
     * @return Added ticket.
     */
    Optional<Ticket> addTicket(Ticket ticket);

    /**
     * Add new ticket using ticket DTO
     *
     * @param user      User.
     * @param ticketDto Ticket DTO
     * @return Added ticket id.
     */
    Integer addTicket(User user, TicketDto ticketDto);

    /**
     * Modify ticket using ticket DTO.
     *
     * @param user      User that requests operation.
     * @param ticketDto Ticket dto.
     * @param ticketId  Id of the ticket that must be modified.
     */
    void modifyTicket(User user, TicketDto ticketDto, Integer ticketId);

    /**
     * Get ticket by id.
     *
     * @param id Ticket id.
     * @return Optional(ticket) if ticket exists, otherwise Optional(null).
     */
    Optional<Ticket> getTicket(Integer id);

    /**
     * Get ticket DTO by ticket id and user (requester).
     * Performs check for visibility by user
     *
     * @param user     User.
     * @param ticketId Ticket id.
     * @return ticket DTO.
     */
    TicketDto getTicketByUser(User user, Integer ticketId);

    /**
     * Get list of attachments (id, file name) for the ticket by ticket Id.
     *
     * @param ticketId Ticket Id.
     * @return List of the AttachmentSimpleDto objects.
     */
    List<AttachmentSimpleDto> getTicketFilelist(Integer ticketId);

    /**
     * Get user's permission DTOs for tickets that are visible for the user.
     *
     * @param user User
     * @return List of permission DTOs.
     */
    List<PermissionDto> getPermissions(User user);

    /**
     * Cancel ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     */
    void cancelTicket(Integer ticketId, User user);

    /**
     * Submit ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     */
    void submitTicket(Integer ticketId, User user);

    /**
     * Approve ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     */
    void approveTicket(Integer ticketId, User user);

    /**
     * Decline ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     */
    void declineTicket(Integer ticketId, User user);

    /**
     * Make action "Assign to me" for the ticket by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     */
    void assignToMeTicket(Integer ticketId, User user);

    /**
     * Change ticket state to "DONE" by ticket id and user that requests operation.
     *
     * @param ticketId ticket id
     * @param user     User that requests operation.
     */
    void setTicketStateDone(Integer ticketId, User user);

    /**
     * Get ticket history DTOs by ticket id, user that requests history.
     *
     * @param user      User that requests history.
     * @param id        ticket id.
     * @param unlimited Mark that indicates how deep must be history.
     *                  If not null and not empty then history depth
     *                  must not be limited.
     * @return List of the HistoryDto objects with requested data.
     */
    List<HistoryDto> getTicketHistory(User user, Integer id, String unlimited);

    /**
     * Check if user is allowed to view ticket feedback.
     *
     * @param user     User who requests feedback viewing.
     * @param ticketId Ticket id
     * @return true if user is allowed to view feedback to the ticket,
     * false otherwise.
     */
    boolean checkUserIsAllowedToViewFeedback(User user, Integer ticketId);

    /**
     * Check if user is allowed to add feedback to the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToAddFeedback(User user, Integer ticketId);
}

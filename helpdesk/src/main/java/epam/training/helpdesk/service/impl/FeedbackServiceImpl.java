package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.FeedbackConverter;
import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.feedback.FeedbackDto;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.repository.FeedbackRepository;
import epam.training.helpdesk.service.FeedbackService;
import epam.training.helpdesk.service.NotificationService;
import epam.training.helpdesk.service.TicketService;
import epam.training.helpdesk.validator.controller.feedback.FeedbackValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Optional;

/**
 * Feedback service implementation.
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

    /**
     * Message to inform that ticket not found.
     */
    private static final String TICKET_NOT_FOUND_MESSAGE = "Ticket not found";

    /**
     * Message to inform that requested ticket is not visible to the user.
     */
    private static final String NO_VISIBLE_TICKET_MESSAGE = "Request is denied or corresponding ticket doesn't exist.";

    /**
     * Feedback converter.
     */
    private final FeedbackConverter converter;

    /**
     * Feedback validator.
     */
    private final FeedbackValidator validator;

    /**
     * Ticket service.
     */
    private final TicketService ticketService;

    /**
     * Feedback repository.
     */
    private final FeedbackRepository feedbackRepository;

    /**
     * Notify service.
     */
    private final NotificationService notificationService;

    public FeedbackServiceImpl(FeedbackConverter converter, FeedbackValidator validator, TicketService ticketService, FeedbackRepository feedbackRepository, NotificationService notificationService) {
        this.converter = converter;
        this.validator = validator;
        this.ticketService = ticketService;
        this.feedbackRepository = feedbackRepository;
        this.notificationService = notificationService;
    }

    /**
     * Add feedback.
     *
     * @param dto      Feedback DTO to add.
     * @param user     User who performs the operation.
     * @param ticketId Ticket id to which feedback must be added.
     * @return An Optional of added feedback.
     * @throws DataNotFoundException if requested ticket without feedback not visible to the user.
     */
    @Override
    @Transactional
    public Optional<Feedback> addFeedback(FeedbackDto dto, User user, Integer ticketId) {
        validator.validate(dto);
        if (ticketService.checkUserIsAllowedToAddFeedback(user, ticketId)) {
            Feedback feedback = converter.convertFromDto(dto);
            feedback.setDate(new Timestamp(System.currentTimeMillis()));
            Ticket ticket = ticketService.getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
            State prevState = ticket.getState();
            feedback.setFeedbackedTicket(ticket);
            feedback.setFeedbackAuthor(user);
            Optional<Feedback> result = feedbackRepository.addFeedback(feedback);
            if (prevState == State.DONE) {
                notificationService.notifyTicketFeedbackProvided(ticketId, ticket.getAssignee());
            }
            return result;
        } else {
            throw new AccessDeniedException(NO_VISIBLE_TICKET_MESSAGE);
        }
    }

    /**
     * Get feedback DTO to the ticket.
     *
     * @param user     User who requests the feedback.
     * @param ticketId Ticket id.
     * @return Feedback DTO.
     * @throws AccessDeniedException if requested ticket isn't visible to the user.
     */
    @Override
    @Transactional
    public FeedbackDto getFeedback(User user, Integer ticketId) {
        if (ticketService.checkUserIsAllowedToViewFeedback(user, ticketId)) {
            Feedback feedback = feedbackRepository.getFeedback(ticketId);
            return converter.convertToDto(feedback);
        } else {
            throw new AccessDeniedException(NO_VISIBLE_TICKET_MESSAGE);
        }
    }

}

package epam.training.helpdesk.service;

import epam.training.helpdesk.domain.User;

public interface NotificationService {

    /**
     * Notify that ticket state was changed to NEW.
     *
     * @param ticketId Id of the ticket which state was changed.
     */
    void notifyTicketChangedStateToNew(Integer ticketId);

    /**
     * Notify that ticket was approved.
     *
     * @param ticketId Id of the ticket that was approved.
     */

    void notifyTicketApproved(Integer ticketId, User ticketOwner);

    /**
     * Notify that ticket was declined.
     *
     * @param ticketId Id of the ticket that was declined.
     */

    void notifyTicketDeclined(Integer ticketId, User ticketOwner);

    /**
     * Notify that ticket was cancelled by a manager.
     *
     * @param ticketId Id of the ticket that was cancelled by a manager.
     */
    void notifyTicketCancelledByManager(Integer ticketId, User ticketOwner);

    /**
     * Notify that ticket was cancelled by an engineer.
     *
     * @param ticketId Id of the ticket that was cancelled by an engineer.
     */
    void notifyTicketCancelledByEngineer(Integer ticketId, User ticketOwner, User ticketAppover);

    /**
     * Notify that ticket state was changed to DONE.
     *
     * @param ticketId Id of the ticket which state was changed.
     */
    void notifyTicketDone(Integer ticketId, User ticketOwner);

    /**
     * Notify that feedback was provided to the ticket
     *
     * @param ticketId Id of the ticket which feedback was provided to.
     */
    void notifyTicketFeedbackProvided(Integer ticketId, User ticketAssignee);

}

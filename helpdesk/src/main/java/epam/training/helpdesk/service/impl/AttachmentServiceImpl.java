package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.AttachmentConverter;
import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.attachment.AttachmentDto;
import epam.training.helpdesk.exception.authorization.AccessDeniedException;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.exception.general.OperationFailedException;
import epam.training.helpdesk.repository.AttachmentRepository;
import epam.training.helpdesk.repository.TicketRepository;
import epam.training.helpdesk.service.AttachmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Attachment service implementation.
 */
@Service
public class AttachmentServiceImpl implements AttachmentService {

    /**
     * Message to inform that access to the attachment is denied.
     */
    private static final String ATTACHMENT_ACCESS_DENIED_MESSAGE = "Access to the attachment is denied";

    /**
     * Message to inform that the attachment not found.
     */
    private static final String ATTACHMENT_NOT_FOUND_MESSAGE = "Attachment not found";

    /**
     * Message to inform that user cannot add attachment to the database.
     */
    private static final String ATTACHMENT_UNABLE_TO_SAVE_MESSAGE = "Cannot add attachment to the database";

    /**
     * Message to inform that user cannot delete linked attachments.
     */
    private static final String ATTACHMENT_UNABLE_UNLINK_LINKED_ATTACHMENT = "You cannot delete attachments from any created tickets using REST API.";

    /**
     * Attachment repository.
     */
    private final AttachmentRepository attachmentRepository;

    /**
     * Ticket repository.
     */
    private final TicketRepository ticketRepository;

    /**
     * Attachment converter.
     */
    private final AttachmentConverter converter;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository, TicketRepository ticketRepository, AttachmentConverter converter) {
        this.attachmentRepository = attachmentRepository;
        this.ticketRepository = ticketRepository;
        this.converter = converter;
    }

    /**
     * Get attachment by attachment Id and user(requester).
     *
     * @param attachmentId  Attachment id
     * @param requesterUser User who requests attachment.
     * @return an Optional of the Attachment
     * @throws AccessDeniedException if attachment is linked to the ticket that is not visible for the user.
     */
    @Override
    public Optional<Attachment> getAttachment(Integer attachmentId, User requesterUser) {
        Optional<Attachment> queryResult = attachmentRepository.getAttachment(attachmentId);
        if (queryResult.isPresent()) {
            if (queryResult.get().getAttachmentTicket() != null) {
                Integer ticketId = queryResult.get().getAttachmentTicket().getId();
                if (ticketId != null && !ticketRepository.isTicketVisible(ticketId, requesterUser)) {
                    throw new AccessDeniedException(ATTACHMENT_ACCESS_DENIED_MESSAGE);
                }
            }
        }
        return queryResult;
    }

    /**
     * Get attachment by Id.
     *
     * @param attachmentId Id of the attachment to get.
     * @return Attachment object.
     */
    @Override
    public Optional<Attachment> getAttachment(Integer attachmentId) {
        return attachmentRepository.getAttachment(attachmentId);
    }

    /**
     * Add attachment to the repository.
     *
     * @param attachment Attachment to add.
     * @return an Optional of the added attachment.
     */
    @Override
    public Optional<Attachment> addAttachment(Attachment attachment) {
        return attachmentRepository.addAttachment(attachment);
    }

    /**
     * Delete unlinked attachment.
     *
     * @param id Attachment id to delete.
     * @throws DataNotFoundException if attachment not found.
     * @throws AccessDeniedException if attachment is linked.
     */
    @Override
    @Transactional
    public void deleteAttachmentChecked(Integer id) {
        Attachment attachment = getAttachment(id).orElseThrow(() -> new DataNotFoundException(ATTACHMENT_NOT_FOUND_MESSAGE));
        if (attachment.getAttachmentTicket() == null) {
            Set<Integer> attachmentsToDelete = new HashSet<>();
            attachmentsToDelete.add(id);
            attachmentRepository.deleteAttachments(attachmentsToDelete);
        } else {
            throw new AccessDeniedException(ATTACHMENT_UNABLE_UNLINK_LINKED_ATTACHMENT);
        }
    }

    /**
     * Get attachment DTO.
     *
     * @param id   Attachment id
     * @param user User (person that want to download attachment).
     * @return Attachment dto.
     * @throws DataNotFoundException if attachment not found or access denied.
     */
    @Override
    @Transactional
    public AttachmentDto getAttachmentDto(Integer id, final User user) {
        Attachment attachment = getAttachment(id, user)
                .orElseThrow(() -> new DataNotFoundException(ATTACHMENT_NOT_FOUND_MESSAGE));
        return converter.convertToDto(attachment);
    }

    /**
     * Add new attachment into the attachment repository.
     *
     * @param multipartFile Uploaded multipartFile file.
     * @return New attachment id.
     * @throws IOException              if IOException happens.
     * @throws OperationFailedException if unable to save attachment.
     */
    @Override
    @Transactional
    public Integer uploadNewFile(MultipartFile multipartFile) throws IOException {
        AttachmentDto dto = new AttachmentDto();
        dto.setName(multipartFile.getOriginalFilename());
        byte[] body = multipartFile.getBytes();
        Byte[] bodyByteWrapper = new Byte[body.length];
        for (int i = 0; i < body.length; i++) {
            bodyByteWrapper[i] = body[i];
        }
        dto.setFileBody(bodyByteWrapper);
        Attachment attachment = addAttachment(converter.convertFromDto(dto))
                .orElseThrow(() -> new OperationFailedException(ATTACHMENT_UNABLE_TO_SAVE_MESSAGE));
        return attachment.getId();
    }

    /**
     * Get attachment file name by attachment id.
     *
     * @param attachmentId Attachment id
     * @return Attachment file name.
     */
    @Override
    public Optional<String> getAttachmentName(Integer attachmentId) {
        return attachmentRepository.getAttachmentName(attachmentId);
    }

    /**
     * Delete set of the attachments.
     *
     * @param attachments Ids of the attachments to delete.
     */
    @Override
    public void deleteAttachments(Set<Integer> attachments) {
        attachmentRepository.deleteAttachments(attachments);
    }
}

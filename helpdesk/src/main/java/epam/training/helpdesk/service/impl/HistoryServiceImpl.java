package epam.training.helpdesk.service.impl;

import epam.training.helpdesk.converter.HistoryConverter;
import epam.training.helpdesk.converter.impl.HistoryConverterImpl;
import epam.training.helpdesk.domain.Action;
import epam.training.helpdesk.domain.History;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.dto.history.HistoryDto;
import epam.training.helpdesk.repository.HistoryRepository;
import epam.training.helpdesk.service.HistoryService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

/**
 * History service implementation.
 */
@Service
public class HistoryServiceImpl implements HistoryService {
    /**
     * Default count of the history records to get.
     */
    private static final Integer DEFAULT_ITEMS_LIMIT = 5;

    /**
     * History repository.
     */
    private HistoryRepository historyRepository;

    public HistoryServiceImpl(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    /**
     * Get ticket history DTO by ticket id with limit.
     *
     * @param ticketId      Ticket id
     * @param unlimitedMark unlimitedMark for history items.
     *                      Must be not empty to get all history items.
     * @return List of the HistoryDto objects.
     */
    public List<HistoryDto> getTicketHistory(final Integer ticketId, String unlimitedMark) {
        Integer limit = DEFAULT_ITEMS_LIMIT;
        if (unlimitedMark != null && !unlimitedMark.isEmpty()) {
            limit = 0;
        }

        List<History> history = historyRepository.getTicketHistory(ticketId, limit);
        HistoryConverter converter = new HistoryConverterImpl();
        return history.stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Add history record.
     *
     * @param user        Acting user.
     * @param ticket      Related ticket.
     * @param action      Action.
     * @param description Additional string to add to the action.
     */
    @Override
    public void addHistory(User user, Ticket ticket, Action action, String description) {
        History history = new History();
        history.setActingUser(user);
        history.setDate(new Timestamp(System.currentTimeMillis()));
        history.setHistoryTicket(ticket);
        history.setAction(action.getActionText());
        history.setDescription(action.getActionText() + description);
        historyRepository.addHistory(history);
    }
}

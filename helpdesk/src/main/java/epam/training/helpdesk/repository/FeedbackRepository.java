package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Feedback;

import java.util.Optional;

/**
 * Feedback repository interface.
 */
public interface FeedbackRepository {
    /**
     * Add feedback.
     * @param feedback Feedback to add.
     * @return Added feedback.
     */
    Optional<Feedback> addFeedback(Feedback feedback);

    /**
     * Get feedback to the ticket.
     * @param ticketId Ticket id.
     * @return Feedback to the ticket.
     */
    Feedback getFeedback(Integer ticketId);

}

package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Attachment;

import java.util.Optional;
import java.util.Set;

/**
 * Attachments repository interface.
 */
public interface AttachmentRepository {

    /**
     * Get attachment by Id.
     *
     * @param attachmentId Id of the attachment to get.
     * @return Attachment object.
     */
    Optional<Attachment> getAttachment(Integer attachmentId);

    /**
     * Add attachment to the repository.
     *
     * @param attachment Attachment to add.
     * @return Added attachment.
     */
    Optional<Attachment> addAttachment(Attachment attachment);

    /**
     * Get attachment file name by Id.
     *
     * @param attachmentId Id of the attachment to get.
     * @return Attachment file name.
     */
    Optional<String> getAttachmentName(Integer attachmentId);

    /**
     * Delete set of the attachments.
     *
     * @param attachments Ids of the attachments to delete.
     */
    void deleteAttachments(Set<Integer> attachments);
}

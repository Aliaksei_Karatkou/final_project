package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Category;
import epam.training.helpdesk.repository.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * Categories repository implementation.
 */
@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    /**
     * Query to get all categories.
     */
    private static final String QUERY_GET_ALL_CATEGORIES = "from Category";

    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    /**
     * Constructor.
     *
     * @param sessionFactory Session factory to use.
     */
    public CategoryRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Get all categories list.
     *
     * @return All categories as list.
     */
    @Override
    @Transactional
    public List<Category> getAllCategories() {
        Session session = sessionFactory.getCurrentSession();
        TypedQuery<Category> query = session.createQuery(QUERY_GET_ALL_CATEGORIES, Category.class);
        return query.getResultList();
    }

    /**
     * Get category by id.
     *
     * @param id Category id.
     * @return an Optional of the Category object.
     */
    @Override
    public Optional<Category> getCategory(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Category> q = cb.createQuery(Category.class);
        Root<Category> root = q.from(Category.class);
        q.select(root);
        q.where(cb.equal(root.get("id"), id));
        return Optional.ofNullable(session.createQuery(q).getSingleResult());
    }
}

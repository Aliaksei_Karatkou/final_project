package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.User;

import java.util.List;
import java.util.Optional;

/**
 * Users repository interface.
 */
public interface UserRepository {

    /**
     * Return User object by login name.
     *
     * @param login login name.
     * @return User object.
     */
    Optional<User> getUserByLogin(String login);

    /**
     * Get user by id.
     * @param id User's id.
     * @return User object.
     */
    Optional<User> getUserById(Integer id);

    /**
     * Get all managers.
     * @return Managers list.
     */
    List<User> getManagers();

    /**
     * Get all engineers.
     * @return Engineers list.
     */
    List<User> getEngineers();
}

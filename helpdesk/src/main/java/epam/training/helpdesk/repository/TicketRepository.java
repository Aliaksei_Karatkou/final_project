package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Ticket repository interface.
 */
public interface TicketRepository {

    /**
     * Get tickets, visible by user.
     *
     * @param user                User.
     * @param sortingOrder        Tickets sorting order.
     * @param personalTicketsOnly True if "My tickets" filter must be applied.
     * @param filter              Searching filter
     * @return Tickets list.
     */
    List<Ticket> getTicketsByUser(User user, SortingOrder sortingOrder, boolean personalTicketsOnly, String filter);

    /**
     * Get allowed to view user's tickets ids.
     *
     * @param user User
     * @return Tickets ids.
     */
    List<Integer> getAllowedTicketsId(User user);

    /**
     * Add ticket to the repository.
     *
     * @param ticket Ticket to add.
     * @return Added ticket.
     */
    Optional<Ticket> addTicket(Ticket ticket);

    /**
     * Update ticket in the repository.
     *
     * @param ticket Ticket to update
     * @return Updated ticket.
     */
    Optional<Ticket> updateTicket(Ticket ticket);

    /**
     * Check ticket visibility for the user by ticket id.
     *
     * @param ticketId Ticket id.
     * @param user     User.
     * @return True if ticket is visible, false otherwise.
     */
    boolean isTicketVisible(Integer ticketId, User user);

    /**
     * Get ticket for user.
     * Performs check for visibility by user.
     *
     * @param ticketId Requested ticket id.
     * @param user     User (requester).
     * @return Optional(ticket) if ticket exists and user can see this ticket.
     * Otherwise return value is Optional(null).
     */
    Optional<Ticket> getTicketChecked(Integer ticketId, User user);

    /**
     * Get ticket by id.
     *
     * @param id Ticket id.
     * @return Optional(ticket) if ticket exists, otherwise Optional(null).
     */
    Optional<Ticket> getTicket(Integer id);

    /**
     * Change ticket status.
     *
     * @param ticketId Ticket id to change.
     * @param state    New ticket status.
     */
    void changeTicketStatus(Integer ticketId, State state);

    /**
     * Get attachments ids that belong to the ticket.
     *
     * @param ticketId Ticket id
     * @return List of the attachments ids.
     */
    List<Integer> getTicketAttachmentsIds(Integer ticketId);

    /**
     * Return count of the "unlinked" attachments form the set of attachments.
     * Set of the attachments defined by set of the attachments ids.
     *
     * @param attachmentIds Attachments ids.
     * @return Return count of the "unlinked" attachments.
     */
    Long getUnlinkedAttachmentCount(Set<Integer> attachmentIds);

    /**
     * Link attachments to the ticket.
     *
     * @param attachments Set of the attachments ids.
     * @param ticketId    Ticket id.
     */
    void linkAttachments(Set<Integer> attachments, Integer ticketId);

    /**
     * Get employee's tickets ids that are allowed to be submitted by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEmployeeAllowedActionSubmitTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get employee's tickets ids that are allowed to be cancelled by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEmployeeAllowedActionCancelTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get manager's tickets ids that are allowed to be cancelled by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getManagerAllowedActionCancelTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get manager's tickets ids that are allowed to be submitted by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getManagerAllowedActionSubmitTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get manager's tickets ids that are allowed to be approved by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getManagerAllowedActionApproveTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get manager's tickets ids that are allowed to be declined by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getManagerAllowedActionDeclineTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get engineer's tickets ids that are allowed to be assigned to himself by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEngineerAllowedActionAssignToMeTicketIds(List<Integer> requestedTicketsIds);

    /**
     * Get engineer's tickets ids that are allowed to be cancelled by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEngineerAllowedActionCancelTicketIds(List<Integer> requestedTicketsIds);

    /**
     * Get engineer's tickets ids that are allowed to be marked as done by the engineer.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEngineerAllowedActionSetDoneStateTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get ids of the tickets, which feedback can be viewed by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEmployeeAllowedActionViewFeedbackTicketIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get ids of the tickets, which feedback can be viewed by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getManagerAllowedActionViewFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Get ids of the tickets, which feedback can be added by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getEmployeeAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds);


    /**
     * Get ids of the tickets, that feedback can be added by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    List<Integer> getManagerAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds);

    /**
     * Check if user is allowed to submit the ticket.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToSubmitTicket(User user, Integer ticketId);

    /**
     * Check if user is allowed to cancel the ticket.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToCancelTicket(User user, Integer ticketId);

    /**
     * Check if user is allowed to decline the ticket.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToDeclineTicket(User user, Integer ticketId);


    /**
     * Check if user is allowed to approve the ticket.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToApproveTicket(User user, Integer ticketId);

    /**
     * Check if user is allowed to assign the ticket to himself.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToAssignToMeTicket(User user, Integer ticketId);

    /**
     * Check if user is allowed to set the ticket to the "done" state.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToSetDoneStateTicket(User user, Integer ticketId);

    /**
     * Check if user is allowed to view feedback to the ticket.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToViewFeedback(User user, Integer ticketId);

    /**
     * Check if user is allowed to add feedback to the ticket.
     * @param user user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    boolean checkUserIsAllowedToAddFeedback(User user, Integer ticketId);

}

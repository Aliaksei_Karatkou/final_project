package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Attachment;
import epam.training.helpdesk.repository.AttachmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;
import java.util.Set;

/**
 * Attachments repository implementation.
 */
@Repository
public class AttachmentRepositoryImpl implements AttachmentRepository {

    /**
     * Query parameter name for a set of the ids.
     */
    private static final String QUERY_PARAMETER_IDS_SET = "idsSet";

    /**
     * Query parameter name for the attachment id.
     */
    private static final String QUERY_PARAMETER_ATTACHMENT_ID = "attachmentId";

    /**
     * Query for deleting a set of the attachments with known ids.
     */
    private static final String QUERY_DELETE_ATTACHMENTS_SET =
            "DELETE from Attachment att WHERE (att.id in :" + QUERY_PARAMETER_IDS_SET + ")";

    /**
     * Query for selecting attachment file name by attachment id.
     */

    private static final String QUERY_GET_ATTACHMENT_NAME =
            "Select att.name from Attachment att where att.id = :" + QUERY_PARAMETER_ATTACHMENT_ID;

    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    public AttachmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Get attachment by Id.
     *
     * @param attachmentId Id of the attachment to get.
     * @return Attachment object.
     */
    @Override
    public Optional<Attachment> getAttachment(final Integer attachmentId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Attachment> q = cb.createQuery(Attachment.class);
        Root<Attachment> root = q.from(Attachment.class);
        q.select(root);
        q.where(cb.equal(root.get("id"), attachmentId));
        Attachment attachment = session.createQuery(q).getSingleResult();
        return Optional.ofNullable(attachment);
    }


    /**
     * Add attachment to the repository.
     *
     * @param attachment Attachment to add.
     * @return Added attachment.
     */
    @Override
    public Optional<Attachment> addAttachment(Attachment attachment) {
        Session session = sessionFactory.getCurrentSession();
        session.save(attachment);
        return Optional.ofNullable(attachment);
    }

    /**
     * Get attachment file name by Id.
     *
     * @param attachmentId Id of the attachment to get.
     * @return Attachment file name.
     */
    @Override
    public Optional<String> getAttachmentName(Integer attachmentId) {
        Session session = sessionFactory.getCurrentSession();
        String name;
        try {
            name = session.createQuery(QUERY_GET_ATTACHMENT_NAME, String.class)
                    .setParameter(QUERY_PARAMETER_ATTACHMENT_ID, attachmentId)
                    .getSingleResult();
        } catch (NoResultException e) {
            name = null;
        }
        return Optional.ofNullable(name);
    }


    /**
     * Delete set of the attachments.
     *
     * @param attachments Ids of the attachments to delete.
     */
    @Override
    public void deleteAttachments(Set<Integer> attachments) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery(QUERY_DELETE_ATTACHMENTS_SET)
                .setParameter(QUERY_PARAMETER_IDS_SET, attachments)
                .executeUpdate();
    }
}

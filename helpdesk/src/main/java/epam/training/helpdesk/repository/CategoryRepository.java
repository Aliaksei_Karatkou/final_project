package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Category;

import java.util.List;
import java.util.Optional;

/**
 * Categories repository interface.
 */
public interface CategoryRepository {

    /**
     * Get all categories list.
     *
     * @return All categories as list.
     */
    List<Category> getAllCategories();

    /**
     * Get category by id.
     *
     * @param id Category id.
     * @return an Optional of the Category object.
     */
    Optional<Category> getCategory(Integer id);
}

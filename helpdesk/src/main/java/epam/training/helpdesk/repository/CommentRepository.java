package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.Comment;

import java.util.List;
import java.util.Optional;

/**
 * Comments repository interface.
 */
public interface CommentRepository {

    /**
     * Add comment.
     * @param comment Comment object.
     * @return Optional of the added comment object.
     */
    Optional<Comment> addComment(Comment comment);

    /**
     * Get comments for the ticket by ticket id.
     * @param ticketId Ticket id.
     * @param limit Max comment items count.
     * @return List of the comments.
     */
    List<Comment> getTicketComments(Integer ticketId, Integer limit);
}

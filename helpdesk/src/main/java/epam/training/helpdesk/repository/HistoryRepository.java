package epam.training.helpdesk.repository;

import epam.training.helpdesk.domain.History;

import java.util.List;
import java.util.Optional;

/**
 * History repository interface.
 */
public interface HistoryRepository {

    /**
     * Get history records for the ticket by ticket id (with limit).
     * @param ticketId Ticket id
     * @param limit Limit history records count ( value "0" means no limit).
     * @return History records for the ticket.
     */
    List<History> getTicketHistory(Integer ticketId, Integer limit);

    /**
     * Add history record.
     * @param history History record.
     * @return Added history record.
     */
    Optional<History> addHistory(History history);
}

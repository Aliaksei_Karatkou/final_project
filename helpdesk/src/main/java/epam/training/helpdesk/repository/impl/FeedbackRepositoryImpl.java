package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.Feedback;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.repository.FeedbackRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * Feedback repository implementation.
 */

@Repository
@Transactional
public class FeedbackRepositoryImpl implements FeedbackRepository {

    /**
     * Error message if requested feedback doesn't exist.
     */
    private static final String NO_SUCH_FEEDBACK_MESSAGE = "No such feedback";

    /**
     * Database field name with ticket id.
     */
    private static final String DB_TABLE_TICKET_FIELD_NAME = "feedbackedTicket";

    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    /**
     * Constructor.
     *
     * @param sessionFactory Hibernate session factory to use.
     */
    public FeedbackRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Add feedback.
     *
     * @param feedback Feedback to add.
     * @return Added feedback.
     */
    @Override
    public Optional<Feedback> addFeedback(Feedback feedback) {
        Session session = sessionFactory.getCurrentSession();
        session.save(feedback);
        return Optional.ofNullable(feedback);
    }

    /**
     * Get feedback to the ticket.
     *
     * @param ticketId Ticket id.
     * @return Feedback to the ticket.
     */
    @Override
    public Feedback getFeedback(Integer ticketId) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Feedback> q = cb.createQuery(Feedback.class);
        Root<Feedback> root = q.from(Feedback.class);
        q.select(root);
        q.where(cb.equal(root.get(DB_TABLE_TICKET_FIELD_NAME), ticketId));
        Feedback feedback;
        try {
            feedback = session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            throw new DataNotFoundException(NO_SUCH_FEEDBACK_MESSAGE);
        }
        return feedback;
    }
}

package epam.training.helpdesk.repository.impl;

import epam.training.helpdesk.domain.SortingField;
import epam.training.helpdesk.domain.SortingOrder;
import epam.training.helpdesk.domain.SortingTrend;
import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Ticket;
import epam.training.helpdesk.domain.User;
import epam.training.helpdesk.exception.data.DataNotFoundException;
import epam.training.helpdesk.repository.TicketRepository;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static epam.training.helpdesk.domain.Role.EMPLOYEE;

/**
 * Ticket repository implementation.
 */
@Repository
public class TicketRepositoryImpl implements TicketRepository {

    /**
     * Query parameter for state.
     */
    private static final String QUERY_PARAMETER_STATE = "state";

    /**
     * Query parameter for role.
     */
    private static final String QUERY_PARAMETER_ROLE = "role";

    /**
     * Query parameter for ticket id.
     */
    private static final String QUERY_PARAMETER_NAME_TICKET_ID = "ticketId";

    /**
     * Query parameter for a set of attachments ids.
     */
    private static final String QUERY_PARAMETER_ATTACHMENTS_IDS_SET = "idsSet";

    /**
     * Query parameter for user's id.
     */
    private static final String QUERY_PARAMETER_USER_ID = "userId";

    /**
     * Query parameter for a set of tickets ids.
     */
    private static final String QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS = "requestedTicketsIds";

    /**
     * Error message for unknown role.
     */
    private static final String UNKNOWN_ROLE_MESSAGE = "Unknown role";

    /**
     * Error message if ticket not found.
     */
    private static final String TICKET_NOT_FOUND_MESSAGE = "Ticket not found";

    /**
     * HQL query for selecting tickets ids.
     */
    private static final String QUERY_SELECT_OPTION_TICKETS_ID = "select t.id from Ticket t where ";

    /**
     * HQL query for selecting tickets.
     */
    private static final String QUERY_SELECT_OPTION_TICKETS = "select t from Ticket t where ";

    /**
     * HQL query clause to find tickets by owner.
     */
    private static final String QUERY_CLAUSE_OWN_TICKETS = "t.owner.id = :" + QUERY_PARAMETER_USER_ID;

    /**
     * HQL query clause to find employee's visible tickets.
     */
    private static final String QUERY_CLAUSE_EMPLOYEE_ALLOWED_TICKETS = QUERY_CLAUSE_OWN_TICKETS;

    /**
     * HQL query to find employee's visible tickets ids.
     */
    private static final String QUERY_GET_EMPLOYEE_ALLOWED_TICKETS_ID =
            QUERY_SELECT_OPTION_TICKETS_ID + QUERY_CLAUSE_EMPLOYEE_ALLOWED_TICKETS;

    /**
     * HQL query to find employee's visible tickets.
     */
    private static final String QUERY_GET_EMPLOYEE_ALLOWED_TICKETS =
            QUERY_SELECT_OPTION_TICKETS + QUERY_CLAUSE_EMPLOYEE_ALLOWED_TICKETS;

    /**
     * HQL query clause to fine engineer's visible tickets.
     */
    private static final String QUERY_CLAUSE_ENGINEER_ALLOWED_TICKETS =
            "((t.owner.role IN ('EMPLOYEE','MANAGER') and t.state = 'APPROVED')"
                    + " or ((t.assignee.id = :" + QUERY_PARAMETER_USER_ID + ") and (t.state in ('IN_PROGRESS','DONE'))))";

    /**
     * Engineer's "My tickets" filter clause.
     */
    private static final String QUERY_CLAUSE_ENGINEER_MY_TICKETS_FILTER = " and (t.assignee.id = :" + QUERY_PARAMETER_USER_ID + ")";

    /**
     * HQL query to get engineer's visible tickets.
     */
    private static final String QUERY_GET_ENGINEER_ALLOWED_TICKETS = QUERY_SELECT_OPTION_TICKETS + QUERY_CLAUSE_ENGINEER_ALLOWED_TICKETS;

    /**
     * HQL query to get engineer's visible tickets ids.
     */
    private static final String QUERY_GET_ENGINEER_ALLOWED_TICKETS_ID = QUERY_SELECT_OPTION_TICKETS_ID + QUERY_CLAUSE_ENGINEER_ALLOWED_TICKETS;


    /**
     * HQL query clause to find visible tickets for the manager.
     */
    private static final String QUERY_CLAUSE_MANAGER_ALLOWED_TICKETS =
            "((t.owner.id = :" + QUERY_PARAMETER_USER_ID + ") or ( (t.owner.role = 'EMPLOYEE') and (t.state = 'NEW'))" +
                    " or ( (t.approver.id = :" + QUERY_PARAMETER_USER_ID + ") and (t.state in ('APPROVED', 'DECLINED','CANCELLED', 'IN_PROGRESS', 'DONE'))))";

    /**
     * Manager's "My tickets" filter clause.
     */
    private static final String QUERY_CLAUSE_MANAGER_MY_TICKETS_FILTER = " and ((t.owner.id = :" + QUERY_PARAMETER_USER_ID + ") or ((t.approver.id = :" + QUERY_PARAMETER_USER_ID + ") and (t.state in ('APPROVED'))))";

    /**
     * HQL query to get tickets visible for the manager.
     */
    private static final String QUERY_GET_MANAGER_ALLOWED_TICKETS = QUERY_SELECT_OPTION_TICKETS + QUERY_CLAUSE_MANAGER_ALLOWED_TICKETS;

    /**
     * HQL query to get tickets ids visible for the manager.
     */
    private static final String QUERY_GET_MANAGER_ALLOWED_TICKETS_ID = QUERY_SELECT_OPTION_TICKETS_ID + QUERY_CLAUSE_MANAGER_ALLOWED_TICKETS;


    /**
     * Query to find attachments ids of the ticket.
     */
    private static final String QUERY_GET_TICKET_ATTACHMENTS_IDS = "select att.id from Attachment att where att.attachmentTicket.id = :" + QUERY_PARAMETER_NAME_TICKET_ID;

    /**
     * Query to find unlinked attachments count (from a limited list).
     */
    private static final String QUERY_GET_UNKLINKED_ATTACHMENTS_COUNT = "select count (att.id) from Attachment att where (att.attachmentTicket is null) and (att.id in :idsList)";

    /**
     * Query to link (add) attachment to the ticket.
     */
    private static final String QUERY_LINK_ATTACHMENTS = "UPDATE Attachment att SET att.attachmentTicket.id = :" + QUERY_PARAMETER_NAME_TICKET_ID + " WHERE (att.id in :" + QUERY_PARAMETER_ATTACHMENTS_IDS_SET + ")";

    /**
     * Query clause to limit tickets by the required ticket.
     */
    private static final String QUERY_CLAUSE_FILTER_TICKET_ID = " and (t.id = :" + QUERY_PARAMETER_NAME_TICKET_ID + ")";

    /**
     * Sort order for sorting by the urgency field.
     */
    private static final String URGENCY_SORT =
            "(CASE WHEN t.urgency = 'LOW' THEN 1" +
                    " WHEN t.urgency = 'AVERAGE' THEN '2'" +
                    " WHEN t.urgency = 'HIGH' THEN '3'" +
                    " WHEN t.urgency = 'CRITICAL' THEN '4'" +
                    " ELSE t.urgency END)";

    /**
     * Sorting order for default sorting order.
     */
    private static final String QUERY_DEFAULT_SORTING_ORDER = " order by " + URGENCY_SORT + " DESC, t.desiredResolutionDate DESC";

    /**
     * Query to get tickets, that a manager or an employee can add feedback to.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_EMPLOYEE_AND_MANAGER_ADD_FEEDBACK_PERMISSION =
            "select t.id from Ticket t LEFT JOIN Feedback f on t.id = f.feedbackedTicket.id " +
                    "where f.feedbackedTicket is null and t.owner.id = :" + QUERY_PARAMETER_USER_ID + " and t.state=:" + QUERY_PARAMETER_STATE + " and t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + ")";

    /**
     * Query to get tickets, that the employee is able to view feedback.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_PERMISSION_EMPLOYEE_VIEW_FEEDBACK =
            "select t.id from Ticket t INNER JOIN Feedback f on t.id = f.feedbackedTicket.id " +
                    "where t.owner.id = :" + QUERY_PARAMETER_USER_ID + " and t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + ")";

    /**
     * Query to get tickets, that the manager is able to view feedback.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_PERMISSION_MANAGER_VIEW_FEEDBACK =
            "select t.id from Ticket t INNER JOIN Feedback f on t.id = f.feedbackedTicket.id " +
                    "where t.owner.id = :" + QUERY_PARAMETER_USER_ID + " or t.owner.role=:" + QUERY_PARAMETER_ROLE + " and t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + ")";

    /**
     * Cause to filter tickets.
     * (By entering text to the filter field at frontend).
     */
    private static final String QUERY_CLAUSE_TICKETS_FILTERING =
            " and (( CAST(t.id  as string) like :filter) or ( t.name like :filter) or " +
                    "(FORMATDATETIME(t.desiredResolutionDate, 'dd/MM/yyyy') like :filter) or (CAST(t.urgency as string) like :filter) or (t.state like :filter))";


    /**
     * Query to get tickets that employee allowed to submit or cancel.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_EMPLOYEE_ALLOWED_ACTION_SUBMIT_OR_CANCEL_TICKETS_IDS =
            "select t.id from Ticket t where t.owner.id = :" + QUERY_PARAMETER_USER_ID + " and (t.state in ('DRAFT','DECLINED')) and (t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + "))";

    /**
     * Query to get tickets that manager allowed to cancel.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_MANAGER_ALLOWED_ACTION_CANCEL_TICKETS_IDS =
            "select t.id from Ticket t where (t.owner.id = :" + QUERY_PARAMETER_USER_ID + " and (t.state in ('DRAFT','NEW','DECLINED'))) and (t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + "))";

    /**
     * Query to get tickets that manager allowed to submit.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_MANAGER_ALLOWED_ACTION_SUBMIT_TICKETS_IDS =
            "select t.id from Ticket t where (t.owner.id = :" + QUERY_PARAMETER_USER_ID + " and (t.state in ('DRAFT','DECLINED'))) and (t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + "))";

    /**
     * Query to get tickets that manager allowed to approve or decline.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_MANAGER_ALLOWED_ACTION_APPROVE_OR_DECLINE_TICKETS_IDS =
            "select t.id from Ticket t where ( (t.owner.id = :" + QUERY_PARAMETER_USER_ID + " or t.owner.role = 'EMPLOYEE') and  (t.state = 'NEW')) and (t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + "))";

    /**
     * Query to get tickets that engineer is allowed to assign to himself.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_ENGINEER_ALLOWED_ACTION_ASSIGN_TO_ME_OR_CANCEL_TICKETS_IDS =
            "select t.id from Ticket t where ( t.state = 'APPROVED' and t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + "))";

    /**
     * Query to get tickets that engineer is allowed to set to the "done" state.
     * Tickets list is limited by set of the tickets to check.
     */
    private static final String QUERY_GET_ENGINEER_ALLOWED_ACTION_SET_DONE_STATE_TICKETS_IDS =
            "select t.id from Ticket t where ( t.state = 'IN_PROGRESS' and t.id in (:" + QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS + "))";

    /**
     * Session factory for Hibernate to use.
     */
    private final SessionFactory sessionFactory;

    /**
     * Constructor.
     *
     * @param sessionFactory Session factory to use.
     */
    public TicketRepositoryImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Return hql query sorting part according to the reuired sorting order.
     *
     * @param sortingOrder Required sorting order
     * @return Sorting part of the hql query.
     */
    private String sortingOrderToSqlQuery(SortingOrder sortingOrder) {
        String order;
        if (sortingOrder.getField() == SortingField.SORT_DEFAULT) {
            order = QUERY_DEFAULT_SORTING_ORDER;
        } else {
            if (sortingOrder.getField() == SortingField.SORT_BY_URGENCY) {
                order = " ORDER BY " + URGENCY_SORT;
            } else {
                order = " ORDER BY " + sortingOrder.getField().getFieldName();
            }
            if (sortingOrder.getTrend() == SortingTrend.DESCENDING) {
                order = order + " DESC";
            }
        }
        return order;
    }

    /**
     * Get tickets, visible by user.
     *
     * @param user                User.
     * @param sortingOrder        Tickets sorting order.
     * @param personalTicketsOnly True if "My tickets" filter must be applied.
     * @param filter              Searching filter
     * @return Tickets list.
     */
    @Override
    public List<Ticket> getTicketsByUser(User user, SortingOrder sortingOrder, boolean personalTicketsOnly, String filter) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS;
                if (personalTicketsOnly) {
                    hql = hql + QUERY_CLAUSE_ENGINEER_MY_TICKETS_FILTER;
                }
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS;
                if (personalTicketsOnly) {
                    hql = hql + QUERY_CLAUSE_MANAGER_MY_TICKETS_FILTER;
                }
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        if (!filter.isEmpty()) {
            hql = hql + QUERY_CLAUSE_TICKETS_FILTERING;
        }
        hql = hql + sortingOrderToSqlQuery(sortingOrder);
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Ticket.class);
        if (!filter.isEmpty()) {
            query.setParameter("filter", "%" + filter + "%");
        }
        query.setParameter(QUERY_PARAMETER_USER_ID, user.getId());
        return query.getResultList();
    }

    /**
     * Check ticket visibility for the user by ticket id.
     *
     * @param ticketId Ticket id.
     * @param user     User.
     * @return True if ticket is visible, false otherwise.
     */
    @Override
    @Transactional
    public boolean isTicketVisible(Integer ticketId, User user) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS_ID + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS_ID + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS_ID + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Integer.class);
        query.setParameter(QUERY_PARAMETER_USER_ID, user.getId());
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        boolean found = true;
        try {
            query.getSingleResult();
        } catch (NoResultException e) {
            found = false;
        }
        return found;
    }

    /**
     * Get ticket for user.
     * Performs check for visibility by user.
     *
     * @param ticketId Requested ticket id.
     * @param user     User (requester).
     * @return (Optional) Ticket if ticket exists and user can see this ticket.
     * Otherwise return value is (Optional) null.
     */
    @Override
    public Optional<Ticket> getTicketChecked(Integer ticketId, User user) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS + QUERY_CLAUSE_FILTER_TICKET_ID;
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Ticket.class);
        query.setParameter(QUERY_PARAMETER_USER_ID, user.getId());
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        Ticket ticket;
        try {
            ticket = (Ticket) query.getSingleResult();
        } catch (NoResultException e) {
            ticket = null;
        }
        return Optional.ofNullable(ticket);
    }

    /**
     * Get allowed to view user's tickets ids.
     *
     * @param user User
     * @return Tickets ids.
     */
    @Override
    public List<Integer> getAllowedTicketsId(User user) {
        String hql;
        switch (user.getRole()) {
            case EMPLOYEE:
                hql = QUERY_GET_EMPLOYEE_ALLOWED_TICKETS_ID;
                break;
            case MANAGER:
                hql = QUERY_GET_MANAGER_ALLOWED_TICKETS_ID;
                break;
            case ENGINEER:
                hql = QUERY_GET_ENGINEER_ALLOWED_TICKETS_ID;
                break;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql, Integer.class);
        query.setParameter(QUERY_PARAMETER_USER_ID, user.getId());
        return query.getResultList();
    }

    /**
     * Add ticket to the repository.
     *
     * @param ticket Ticket to add.
     * @return Added ticket.
     */
    @Override
    public Optional<Ticket> addTicket(Ticket ticket) {
        Session session = sessionFactory.getCurrentSession();
        session.save(ticket);
        session.flush();
        return Optional.ofNullable(ticket);
    }

    /**
     * Update ticket in the repository.
     *
     * @param ticket Ticket to update
     * @return Updated ticket.
     */
    @Override
    public Optional<Ticket> updateTicket(Ticket ticket) {
        Session session = sessionFactory.getCurrentSession();
        session.update(ticket);
        return Optional.ofNullable(ticket);
    }

    /**
     * Get ticket by id.
     *
     * @param id Ticket id.
     * @return Optional(ticket) if ticket exists, otherwise Optional(null).
     */
    @Override
    public Optional<Ticket> getTicket(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Ticket> q = cb.createQuery(Ticket.class);
        Root<Ticket> root = q.from(Ticket.class);
        q.select(root);
        q.where(cb.equal(root.get("id"), id));
        Ticket ticket = null;
        try {
            ticket = session.createQuery(q).getSingleResult();
        } catch (NoResultException e) {
            throw new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE);
        }
        Hibernate.initialize(ticket.getHistoryRecords());
        return Optional.ofNullable(ticket);
    }

    /**
     * Get attachments ids that belong to the ticket.
     *
     * @param ticketId Ticket id
     * @return List of the attachments ids.
     */
    @Override
    public List<Integer> getTicketAttachmentsIds(Integer ticketId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_TICKET_ATTACHMENTS_IDS, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId);
        return query.getResultList();
    }

    /**
     * Return count of the "unlinked" attachments form the set of attachments.
     * Set of the attachments defined by set of the attachments ids.
     *
     * @param attachmentIds Attachments ids.
     * @return Return count of the "unlinked" attachments.
     */
    @Override
    public Long getUnlinkedAttachmentCount(Set<Integer> attachmentIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_UNKLINKED_ATTACHMENTS_COUNT, Long.class);
        query.setParameter("idsList", new ArrayList<Integer>(attachmentIds));
        return (Long) query.getSingleResult();
    }

    /**
     * Link attachments to the ticket.
     *
     * @param attachments Set of the attachments ids.
     * @param ticketId    Ticket id.
     */
    @Override
    public void linkAttachments(Set<Integer> attachments, Integer ticketId) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery(QUERY_LINK_ATTACHMENTS)
                .setParameter(QUERY_PARAMETER_NAME_TICKET_ID, ticketId)
                .setParameter(QUERY_PARAMETER_ATTACHMENTS_IDS_SET, attachments)
                .executeUpdate();
    }

    /**
     * Change ticket status.
     *
     * @param ticketId Ticket id to change.
     * @param state    New ticket status.
     */
    @Override
    public void changeTicketStatus(Integer ticketId, State state) {
        Session session = sessionFactory.getCurrentSession();
        Ticket ticket = getTicket(ticketId).orElseThrow(() -> new DataNotFoundException(TICKET_NOT_FOUND_MESSAGE));
        ticket.setState(state);
        session.update(ticket);
        session.flush();
    }

    /**
     * Get employee's tickets ids that are allowed to be submitted or cancelled by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEmployeeAllowedActionSubmitOrCancelTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_EMPLOYEE_ALLOWED_ACTION_SUBMIT_OR_CANCEL_TICKETS_IDS, Integer.class)
                .setParameter(QUERY_PARAMETER_USER_ID, userId)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get employee's tickets ids that are allowed to be submitted by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getEmployeeAllowedActionSubmitTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return getEmployeeAllowedActionSubmitOrCancelTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get employee's tickets ids that are allowed to be cancelled by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getEmployeeAllowedActionCancelTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return getEmployeeAllowedActionSubmitOrCancelTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get manager's tickets ids that are allowed to be cancelled by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getManagerAllowedActionCancelTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_MANAGER_ALLOWED_ACTION_CANCEL_TICKETS_IDS, Integer.class)
                .setParameter(QUERY_PARAMETER_USER_ID, userId)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get manager's tickets ids that are allowed to be submitted by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getManagerAllowedActionSubmitTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_MANAGER_ALLOWED_ACTION_SUBMIT_TICKETS_IDS, Integer.class)
                .setParameter(QUERY_PARAMETER_USER_ID, userId)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get manager's tickets ids that are allowed to be approved or declined by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getManagerAllowedActionApproveOrDeclineTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_MANAGER_ALLOWED_ACTION_APPROVE_OR_DECLINE_TICKETS_IDS, Integer.class)
                .setParameter(QUERY_PARAMETER_USER_ID, userId)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get manager's tickets ids that are allowed to be approved by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getManagerAllowedActionApproveTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return getManagerAllowedActionApproveOrDeclineTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get manager's tickets ids that are allowed to be declined by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getManagerAllowedActionDeclineTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        return getManagerAllowedActionApproveOrDeclineTicketIds(userId, requestedTicketsIds);
    }

    /**
     * Get engineer's tickets ids that are allowed to be cancelled or assigned to himself by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */

    private List<Integer> getEngineerAllowedActionAssignToMeOrCancelTicketIds(List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_ENGINEER_ALLOWED_ACTION_ASSIGN_TO_ME_OR_CANCEL_TICKETS_IDS, Integer.class)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get engineer's tickets ids that are allowed to be assigned to himself by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getEngineerAllowedActionAssignToMeTicketIds(List<Integer> requestedTicketsIds) {
        return getEngineerAllowedActionAssignToMeOrCancelTicketIds(requestedTicketsIds);
    }

    /**
     * Get engineer's tickets ids that are allowed to be cancelled by the engineer.
     *
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getEngineerAllowedActionCancelTicketIds(List<Integer> requestedTicketsIds) {
        return getEngineerAllowedActionAssignToMeOrCancelTicketIds(requestedTicketsIds);
    }

    /**
     * Get engineer's tickets ids that are allowed to be marked as done by the engineer.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    @Override
    public List<Integer> getEngineerAllowedActionSetDoneStateTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_ENGINEER_ALLOWED_ACTION_SET_DONE_STATE_TICKETS_IDS, Integer.class)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get ids of the tickets, which feedback can be viewed by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    public List<Integer> getEmployeeAllowedActionViewFeedbackTicketIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_PERMISSION_EMPLOYEE_VIEW_FEEDBACK, Integer.class)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds)
                .setParameter(QUERY_PARAMETER_USER_ID, userId);
        return query.getResultList();
    }

    /**
     * Get ids of the tickets, which feedback can be viewed by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    public List<Integer> getManagerAllowedActionViewFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_PERMISSION_MANAGER_VIEW_FEEDBACK, Integer.class)
                .setParameter(QUERY_PARAMETER_ROLE, EMPLOYEE)
                .setParameter(QUERY_PARAMETER_USER_ID, userId)
                .setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        return query.getResultList();
    }

    /**
     * Get ids of the tickets, that feedback can be added by the employee or the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    private List<Integer> getEmployeeOrManagerAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(QUERY_GET_EMPLOYEE_AND_MANAGER_ADD_FEEDBACK_PERMISSION, Integer.class);
        query.setParameter(QUERY_PARAMETER_NAME_REQUESTED_TICKETS_IDS, requestedTicketsIds);
        query.setParameter(QUERY_PARAMETER_USER_ID, userId);
        query.setParameter(QUERY_PARAMETER_STATE, State.DONE);
        return query.getResultList();
    }

    /**
     * Get ids of the tickets, that feedback can be added by the employee.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    public List<Integer> getEmployeeAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        return getEmployeeOrManagerAllowedActionAddFeedbackTicketsIds(userId, requestedTicketsIds);

    }

    /**
     * Get ids of the tickets, that feedback can be added by the manager.
     *
     * @param userId              User id.
     * @param requestedTicketsIds Set of the tickets that must be analyzed.
     * @return Tickets ids list.
     */
    public List<Integer> getManagerAllowedActionAddFeedbackTicketsIds(Integer userId, List<Integer> requestedTicketsIds) {
        return getEmployeeOrManagerAllowedActionAddFeedbackTicketsIds(userId, requestedTicketsIds);
    }

    /**
     * Check if user is allowed to submit the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToSubmitTicket(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
                return !getEmployeeAllowedActionSubmitTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case MANAGER:
                return !getManagerAllowedActionSubmitTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case ENGINEER:
                return false;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to cancel the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToCancelTicket(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
                return !getEmployeeAllowedActionCancelTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case MANAGER:
                return !getManagerAllowedActionCancelTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case ENGINEER:
                return !getEngineerAllowedActionCancelTicketIds(Collections.singletonList(ticketId)).isEmpty();
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to decline the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToDeclineTicket(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
            case ENGINEER:
                return false;
            case MANAGER:
                return !getManagerAllowedActionDeclineTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to approve the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToApproveTicket(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
            case ENGINEER:
                return false;
            case MANAGER:
                return !getManagerAllowedActionApproveTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to assign the ticket to himself.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToAssignToMeTicket(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
            case MANAGER:
                return false;
            case ENGINEER:
                return !getEngineerAllowedActionAssignToMeTicketIds(Collections.singletonList(ticketId)).isEmpty();
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to set the ticket to the "done" state.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToSetDoneStateTicket(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
            case MANAGER:
                return false;
            case ENGINEER:
                return !getEngineerAllowedActionSetDoneStateTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to view feedback to the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToViewFeedback(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
                return !getEmployeeAllowedActionViewFeedbackTicketIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case MANAGER:
                return !getManagerAllowedActionViewFeedbackTicketsIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case ENGINEER:
                return false;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }

    /**
     * Check if user is allowed to add feedback to the ticket.
     *
     * @param user     user's id
     * @param ticketId ticket id
     * @return true if action is allowed, false otherwise.
     */
    @Override
    public boolean checkUserIsAllowedToAddFeedback(User user, Integer ticketId) {
        switch (user.getRole()) {
            case EMPLOYEE:
                return !getEmployeeAllowedActionAddFeedbackTicketsIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case MANAGER:
                return !getManagerAllowedActionAddFeedbackTicketsIds(user.getId(), Collections.singletonList(ticketId)).isEmpty();
            case ENGINEER:
                return false;
            default:
                throw new IllegalStateException(UNKNOWN_ROLE_MESSAGE);
        }
    }
}


package epam.training.helpdesk.dto.history;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * History item Dto.
 */
public class HistoryDto {

    /**
     * History item id.
     */
    private Integer id;

    /**
     * Date and time of the event in the history.
     */
    private Timestamp date;

    /**
     * Action.
     */
    private String action;

    /**
     * Username related to the history event.
     */
    private String username;

    /**
     * History item description.
     */
    private String description;

    /**
     * Constructor.
     * @param id History item id.
     * @param date History event date and time.
     * @param action Action.
     * @param username Username related to the history event.
     * @param description History item description
     */
    public HistoryDto(Integer id, Timestamp date, String action, String username, String description) {
        this.id = id;
        this.date = date;
        this.action = action;
        this.username = username;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryDto dto = (HistoryDto) o;
        return Objects.equals(id, dto.id) &&
                Objects.equals(date, dto.date) &&
                Objects.equals(action, dto.action) &&
                Objects.equals(username, dto.username) &&
                Objects.equals(description, dto.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, action, username, description);
    }
}

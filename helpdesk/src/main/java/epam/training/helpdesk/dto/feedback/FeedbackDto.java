package epam.training.helpdesk.dto.feedback;

import java.util.Objects;

/**
 * Feedback Dto.
 */
public class FeedbackDto {

    /**
     * Feedback rate.
     */
    private Integer rate;

    /**
     * Feedback text.
     */
    private String text;

    /***
     * Default constructor.
     */
    public FeedbackDto() {
    }

    /**
     * Constructor.
     * @param rate Feedback rate.
     * @param text Feedback text.
     */
    public FeedbackDto(Integer rate, String text) {
        this.rate = rate;
        this.text = text;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeedbackDto that = (FeedbackDto) o;
        return rate.equals(that.rate) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rate, text);
    }
}

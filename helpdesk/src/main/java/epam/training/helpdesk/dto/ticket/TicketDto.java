package epam.training.helpdesk.dto.ticket;

import epam.training.helpdesk.domain.State;
import epam.training.helpdesk.domain.Urgency;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

/**
 * Ticket Dto.
 */
public class TicketDto {

    /**
     * Ticket id.
     */
    private Integer id;
    /**
     * Name of the ticket.
     */
    private String name;
    /**
     * Description of the ticket.
     */
    private String description;
    /**
     * Ticket creation date and time.
     */
    private Timestamp createdOn;
    /**
     * Ticket desired resulution date.
     */
    private Date desiredResolutionDate;
    /**
     * Ticket assignee's id.
     */
    private Integer assigneeId;
    /**
     * Ticket owner's id.
     */
    private Integer ownerId;
    /**
     * Ticket state.
     */
    private State state;
    /**
     * Ticket urgency.
     */
    private Urgency urgency;
    /**
     * Ticket category id.
     */
    private Integer categoryId;
    /**
     * Set of the ticket attachments id.
     */
    private Set<Integer> attachmentsId;
    /**
     * Ticket approver's id.
     */
    private Integer approverId;

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public Set<Integer> getAttachmentsId() {
        return attachmentsId;
    }

    public void setAttachmentsId(Set<Integer> attachmentsId) {
        this.attachmentsId = attachmentsId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
package epam.training.helpdesk.dto.user;

/**
 * User dto.
 */
public class UserDto {
    /**
     * User's id.
     */
    private Integer id;
    /**
     * User's first name.
     */
    private String firstName;
    /**
     * User's last name.
     */
    private String lastName;

    /**
     * Constructor.
     * @param id User's id
     * @param firstName User's first name.
     * @param lastName User's last name.
     */
    public UserDto(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

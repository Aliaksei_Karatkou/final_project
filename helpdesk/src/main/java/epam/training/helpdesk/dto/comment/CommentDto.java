package epam.training.helpdesk.dto.comment;

import java.sql.Timestamp;

/**
 * Ticket comment Dto.
 */
public class CommentDto {

    /**
     * Comment id.
     */
    private Integer id;

    /**
     * Comment text.
     */
    private String text;

    /**
     * Comment date and time.
     */
    private Timestamp date;

    /**
     * Id of the user who commented the ticket.
     */
    private Integer userId;

    /**
     * Id of the commented ticket.
     */
    private Integer ticketId;

    /**
     * Commentator's name.
     */
    private String username;

    /**
     * Default constructor.
     */
    public CommentDto() {
    }

    /**
     * Constructor.
     * @param id Comment id
     * @param text Comment text
     * @param date Comment date
     * @param username Commentator's username.
     */
    public CommentDto(Integer id, String text, Timestamp date, String username) {
        this.text = text;
        this.date = date;
        this.username = username;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}

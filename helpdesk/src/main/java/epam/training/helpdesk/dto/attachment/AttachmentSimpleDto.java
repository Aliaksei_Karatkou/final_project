package epam.training.helpdesk.dto.attachment;

import java.util.Objects;

/**
 * Simple attachment data transfer object.
 * Doesn't include file body.
 */
public class AttachmentSimpleDto {

    /**
     * Attachment id.
     */
    private Integer id;

    /**
     * Attachment file name.
     */
    private String filename;

    /**
     * Constructor.
     * @param id Attachment Id.
     * @param filename Attachment file name.
     */
    public AttachmentSimpleDto(Integer id, String filename) {
        this.id = id;
        this.filename = filename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttachmentSimpleDto that = (AttachmentSimpleDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(filename, that.filename);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, filename);
    }
}

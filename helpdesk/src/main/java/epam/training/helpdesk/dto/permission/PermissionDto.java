package epam.training.helpdesk.dto.permission;

import java.util.Objects;

/**
 * Ticket permissions dto.
 */
public class PermissionDto {

    /**
     * Ticket id.
     */
    private Integer ticketId;

    /**
     * Permission to submit the ticket.
     */
    private boolean submitAllowed;

    /**
     * Permission to cancel the ticket.
     */
    private boolean cancelAllowed;

    /**
     * Permission to approve the ticket.
     */
    private boolean approveAllowed;

    /**
     * Permission to decline the ticket.
     */
    private boolean declineAllowed;

    /**
     * Permission to do "assign to me" action.
     */
    private boolean assignToMeAllowed;

    /**
     * Permission to set ticket "done" state.
     */
    private boolean doneAllowed;

    /**
     * Permission to view feedback.
     */
    private boolean feedbackViewingAllowed;

    /**
     * Permission to add feedback.
     */
    private boolean feedbackAddAllowed;

    /**
     *
     * @param ticketId Ticket id.
     * @param submitAllowed Permission to submit the ticket.
     * @param cancelAllowed Permission to cancel the ticket.
     * @param approveAllowed Permission to approve the ticket.
     * @param declineAllowed Permission to decline the ticket.
     * @param assignToMeAllowed Permission to do "assign to me" action.
     * @param doneAllowed Permission to set ticket "done" state.
     * @param feedbackViewingAllowed Permission to view feedback.
     * @param feedbackAddAllowed Permission to add feedback.
     */
    public PermissionDto(Integer ticketId, boolean submitAllowed, boolean cancelAllowed,
                         boolean approveAllowed, boolean declineAllowed, boolean assignToMeAllowed,
                         boolean doneAllowed, boolean feedbackViewingAllowed, boolean feedbackAddAllowed) {
        this.submitAllowed = submitAllowed;
        this.cancelAllowed = cancelAllowed;
        this.approveAllowed = approveAllowed;
        this.declineAllowed = declineAllowed;
        this.assignToMeAllowed = assignToMeAllowed;
        this.doneAllowed = doneAllowed;
        this.feedbackViewingAllowed = feedbackViewingAllowed;
        this.feedbackAddAllowed = feedbackAddAllowed;
        this.ticketId = ticketId;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public boolean isSubmitAllowed() {
        return submitAllowed;
    }

    public void setSubmitAllowed(boolean submitAllowed) {
        this.submitAllowed = submitAllowed;
    }

    public boolean isCancelAllowed() {
        return cancelAllowed;
    }

    public void setCancelAllowed(boolean cancelAllowed) {
        this.cancelAllowed = cancelAllowed;
    }

    public boolean isApproveAllowed() {
        return approveAllowed;
    }

    public void setApproveAllowed(boolean approveAllowed) {
        this.approveAllowed = approveAllowed;
    }

    public boolean isDeclineAllowed() {
        return declineAllowed;
    }

    public void setDeclineAllowed(boolean declineAllowed) {
        this.declineAllowed = declineAllowed;
    }

    public boolean isAssignToMeAllowed() {
        return assignToMeAllowed;
    }

    public void setAssignToMeAllowed(boolean assignToMeAllowed) {
        this.assignToMeAllowed = assignToMeAllowed;
    }

    public boolean isDoneAllowed() {
        return doneAllowed;
    }

    public void setDoneAllowed(boolean doneAllowed) {
        this.doneAllowed = doneAllowed;
    }

    public boolean isFeedbackViewingAllowed() {
        return feedbackViewingAllowed;
    }

    public void setFeedbackViewingAllowed(boolean feedbackViewingAllowed) {
        this.feedbackViewingAllowed = feedbackViewingAllowed;
    }

    public boolean isFeedbackAddAllowed() {
        return feedbackAddAllowed;
    }

    public void setFeedbackAddAllowed(boolean feedbackAddAllowed) {
        this.feedbackAddAllowed = feedbackAddAllowed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PermissionDto that = (PermissionDto) o;
        return submitAllowed == that.submitAllowed &&
                cancelAllowed == that.cancelAllowed &&
                approveAllowed == that.approveAllowed &&
                declineAllowed == that.declineAllowed &&
                assignToMeAllowed == that.assignToMeAllowed &&
                doneAllowed == that.doneAllowed &&
                feedbackViewingAllowed == that.feedbackViewingAllowed &&
                feedbackAddAllowed == that.feedbackAddAllowed &&
                Objects.equals(ticketId, that.ticketId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, submitAllowed, cancelAllowed, approveAllowed, declineAllowed, assignToMeAllowed, doneAllowed, feedbackViewingAllowed, feedbackAddAllowed);
    }
}

package epam.training.helpdesk.dto.attachment;

import java.util.Arrays;
import java.util.Objects;

/**
 * Attachment data transfer object.
 */
public class AttachmentDto {

    /**
     * Attachment id.
     */
    private Integer id;

    /**
     * Attachment file body.
     */
    private Byte[] fileBody;

    /**
     * Attachment file name.
     */
    private String name;

    /**
     * Constructor.
     * @param id Attachment id.
     * @param fileBody File body.
     * @param name File name.
     */
    public AttachmentDto(Integer id, Byte[] fileBody, String name) {
        this.id = id;
        this.fileBody = fileBody;
        this.name = name;
    }

    public AttachmentDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte[] getFileBody() {
        return fileBody;
    }

    public void setFileBody(Byte[] fileBody) {
        this.fileBody = fileBody;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttachmentDto dto = (AttachmentDto) o;
        return Objects.equals(id, dto.id) &&
                Arrays.equals(fileBody, dto.fileBody) &&
                Objects.equals(name, dto.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name);
        result = 31 * result + Arrays.hashCode(fileBody);
        return result;
    }
}

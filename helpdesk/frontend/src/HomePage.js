import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import ReactDatatable from '@ashvin27/react-datatable';
import axios from 'axios';
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import { validateFilter } from './_helpers/inputValidators.js';


const apiUrl = "http://localhost:8080/helpdesk/api/1.0.0/";
const ROLE_EMPLOYEE = "EMPLOYEE";
const ROLE_MANAGER = "MANAGER";
const URL_SUBMIT_ACTION_NAME = "/submitService/";
const URL_CANCEL_ACTION_URL = "/cancelService/";
const URL_APPROVE_ACTION_URL = "/approveService/";
const URL_DECLINE_ACTION_URL = "/declineService/";
const URL_ASSIGN_TO_ME_ACTION_URL = "/assignService/";
const URL_DONE_ACTION_URL = "/completeService/";

class HomePage extends React.Component {

    SortOptions = [
        { value: 'default', label: 'default' },
        { value: '+byId', label: 'by ID, ascending' },
        { value: '-byId', label: 'by ID, descending' },
        { value: '+byName', label: 'by Name, ascending' },
        { value: '-byName', label: 'by Name, descending' },
        { value: '+byDesiredDate', label: 'by Desired date, ascending' },
        { value: '-byDesiredDate', label: 'by Desired date, descending' },
        { value: '+byUrgency', label: 'by Urgency, ascending' },
        { value: '-byUrgency', label: 'by Urgency, descending' },
        { value: '+byStatus', label: 'by Status, ascending' },
        { value: '-byStatus', label: 'by Status, descending' }
    ]

    constructor(props) {
        super(props);
        this.onAllTicketsButton = this.onAllTicketsButton.bind(this);
        this.onMyTicketsButton = this.onMyTicketsButton.bind(this);
        this.reloadTickets = this.reloadTickets.bind(this);
        this.submitTicketAction = this.submitTicketAction.bind(this);
        this.cancelTicketAction = this.cancelTicketAction.bind(this);
        this.approveTicketAction = this.approveTicketAction.bind(this);
        this.declineTicketAction = this.declineTicketAction.bind(this);
        this.assignToMeTicketAction = this.assignToMeTicketAction.bind(this);
        this.doneTicketAction = this.doneTicketAction.bind(this);
        this.requestTicketAction = this.requestTicketAction.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);

        this.state = {
            showAllTickets: true,
            ticketsOrder: "default",
            users: [],
            reloadList: false,
            filterText: "",
            sortingParameters: {
                sort: "default"
            },
            ticketsTable: {
                columns: [
                    {
                        key: "id",
                        text: "id",
                        className: "id",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "name",
                        text: "Name",
                        className: "name",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "desiredDate",
                        text: "Desired date",
                        className: "desiredDate",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "urgency",
                        text: "Urgency",
                        className: "urgency",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "status",
                        text: "Status",
                        className: "status",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "action",
                        text: "Action",
                        className: "action",
                        width: 100,
                        align: "left",
                        sortable: false,
                        cell: record => {
                            return (
                                <Fragment>
                                    {record.permissions != null && record.permissions.submitAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            onClick={() => this.submitTicketAction(record)}
                                            style={{ marginRight: '5px' }}>
                                            <i className="glyphicon glyphicon-edit fa fa-edit">Submit</i>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.cancelAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            onClick={() => this.cancelTicketAction(record)}
                                            style={{ marginRight: '5px' }}>
                                            <i className="glyphicon glyphicon-edit fa fa-edit">Cancel</i>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.approveAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            onClick={() => this.approveTicketAction(record)}
                                            style={{ marginRight: '5px' }}>
                                            <i className="glyphicon glyphicon-edit fa fa-edit">Approve</i>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.declineAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            onClick={() => this.declineTicketAction(record)}
                                            style={{ marginRight: '5px' }}>
                                            <i className="glyphicon glyphicon-edit fa fa-edit">Decline</i>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.assignToMeAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            onClick={() => this.assignToMeTicketAction(record)}
                                            style={{ marginRight: '5px' }}>
                                            <i className="glyphicon glyphicon-edit fa fa-edit">AssignToMe</i>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.doneAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            onClick={() => this.doneTicketAction(record)}
                                            style={{ marginRight: '5px' }}>
                                            <i className="glyphicon glyphicon-edit fa fa-edit">Done</i>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.feedbackAddAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            style={{ marginRight: '5px' }}>
                                            <Link to={"/Feedback/" + record.permissions.ticketId + "/add"} className="btn">Add feedback</Link>
                                        </button>
                                    }
                                    {record.permissions != null && record.permissions.feedbackViewingAllowed &&
                                        <button
                                            className="btn btn-primary btn-sm"
                                            style={{ marginRight: '5px' }}>
                                            <Link to={"/Feedback/" + record.permissions.ticketId + "/view"} className="btn">View feedback</Link>
                                        </button>

                                    }
                                </Fragment>
                            );
                        }
                    }
                ],
                config: {
                    page_size: 10,
                    length_menu: [10, 20, 50],
                    sort: {},
                    button: {},
                    show_info: true,
                    show_filter: false
                },
                records: [
                ],
                extraButtons: [
                ],
            }
        };
    }
    timestampToString(timestamp) {
        let date = new Date(timestamp);
        return date.toLocaleDateString() + " " + date.toLocaleTimeString();
    }
    timestampToDateString(timestamp) {
        let date = new Date(timestamp);
        let year = date.getFullYear();
        let month = 1 + date.getMonth();
        let day = date.getDate()
        return [
            (day > 9 ? '' : '0') + day,
            (month > 9 ? '' : '0') + month,
            year
        ].join('/');
    }

    requestTicketAction(ticketId, actionName) {
        let url = apiUrl + "tickets/" + ticketId + actionName;
        let dataStub = {};
        axios.put(url,
            dataStub, {
                headers: { authorization: 'Basic ' + localStorage.getItem('authdata') },
            })
            .then(res => {
                this.reloadTickets();
            })
    }
    submitTicketAction(record) {
        this.requestTicketAction(record.id, URL_SUBMIT_ACTION_NAME);
    }
    cancelTicketAction(record) {
        this.requestTicketAction(record.id, URL_CANCEL_ACTION_URL);
    }
    approveTicketAction(record) {
        this.requestTicketAction(record.id, URL_APPROVE_ACTION_URL);
    }
    declineTicketAction(record) {
        this.requestTicketAction(record.id, URL_DECLINE_ACTION_URL);
    }
    assignToMeTicketAction(record) {
        this.requestTicketAction(record.id, URL_ASSIGN_TO_ME_ACTION_URL);
    }
    doneTicketAction(record) {
        this.requestTicketAction(record.id, URL_DONE_ACTION_URL);
    }

    componentDidMount() {
        let params = {
            sort: "default"
        };
        this.reloadTickets(false, params);
    }
    _onSortingSelect = (event) => {
        let params = {
            sort: event.value
        };
        const isMyTicketsMode = !this.state.showAllTickets;
        let prepState = this.state;
        prepState.ticketsOrder = event.label;
        prepState.ticketReqParams = params;
        prepState.ticketsOrderValue = event.value;
        this.setState(prepState);
        this.reloadTickets(isMyTicketsMode, params);
    }

    getButtonClassName(isActive) {
        if (isActive) {
            return "btn btn-primary";
        }
        else {
            return "btn btn-secondary";
        }
    }
    onAllTicketsButton() {
        let prepState = this.state;
        prepState.showAllTickets = true;
        this.setState(prepState);
        this.reloadTickets(false, this.state.ticketReqParams)
    }
    onMyTicketsButton() {
        let prepState = this.state;
        prepState.showAllTickets = false;
        this.setState(prepState);
        this.reloadTickets(true, this.state.ticketReqParams)
    }

    findTicketTableIndexByTicketId(ticketsTable, ticketId) {
        for (let i = 0; i < ticketsTable.length; i++) {
            if (ticketsTable[i].id == ticketId) {
                return i;
            }
        }
        return null;
    }

    onFilterChange(event) {

        if (validateFilter(event.target.value)) {
            let prepState = this.state;
            prepState.filterText = event.target.value;
            this.setState(prepState);
            this.reloadTickets(!this.state.showAllTickets, {
                sort: this.state.ticketsOrderValue

            })

        }
        else {
            event.target.value = this.state.filterText;
        }
    }

    reloadTickets(isMyTicketsView, params) {
        let url = apiUrl + "tickets/";
        if (params == null) {
            params = {};
        }
        params.filter = this.state.filterText;
        if (isMyTicketsView) {
            url = url + "my/";
        }
        axios.get(url, {
            headers: { authorization: 'Basic ' + localStorage.getItem('authdata') },
            params
        })
            .then(res => {
                const tickets = res.data;
                this.setState({ tickets });
                let prepState = this.state;
                prepState.ticketsTable.records = [];
                tickets.forEach(element => {
                    prepState.ticketsTable.records.push({
                        id: element.id,
                        name: <Link to={"ViewTicketDetails/" + element.id}>{element.name}</Link>,
                        desiredDate: element.desiredResolutionDate != null ? this.timestampToDateString(element.desiredResolutionDate) : "",
                        urgency: element.urgency,
                        status: element.state
                    });
                })
                this.setState(prepState);
            })
            .then(
                axios.get(apiUrl + "tickets/permissions/", {
                    headers: { authorization: 'Basic ' + localStorage.getItem('authdata') }
                })
                    .then(res => {
                        const permissions = res.data;
                        let prepState = this.state;
                        res.data.forEach(permissions => {
                            let visibleTableRowIndex = this.findTicketTableIndexByTicketId(prepState.ticketsTable.records, permissions.ticketId);
                            if (visibleTableRowIndex != null) {
                                prepState.ticketsTable.records[visibleTableRowIndex].permissions = permissions;
                            }
                        })
                        this.setState(prepState);
                    })
            )
    }

    isAddTicketButtonEnabled() {
        let role = localStorage.getItem('role');
        return (role == ROLE_EMPLOYEE || role == ROLE_MANAGER);
    }

    render() {
        return (
            <div>
                <p><h5>Please, refresh this page if you cannot see the changes and/or buttons</h5></p>
                {this.isAddTicketButtonEnabled() && <div> <Link to="/addTicket" className="btn btn-primary">Add new ticket</Link></div>}
                <p></p>
                <button type="submit"
                    className="{this.getButtonClassName(this.state.showAllTickets)}"
                    style={{ width: 300 + 'px' }}
                    onClick={this.onAllTicketsButton}>All tickets
                </button>
                <button type="submit"
                    className="col-sm-5 col-sm-offset-1"
                    onClick={this.onMyTicketsButton}
                    style={{ width: 300 + 'px' }}>My tickets
                </button>
                <p>Select sorting order:</p>
                <div className="col-sm-5 col-sm-offset-1">
                    <Dropdown options={this.SortOptions}
                        onChange={this._onSortingSelect}
                        placeholder={"Soring order: " + this.state.ticketsOrder} />
                </div>
                Filter (case-sentitive): <input type="text" value={this.state.filterText} onChange={this.onFilterChange} placeholder="write filter here" />
                <ReactDatatable
                    config={this.state.ticketsTable.config}
                    records={this.state.ticketsTable.records}
                    columns={this.state.ticketsTable.columns}
                    extraButtons={this.state.ticketsTable.extraButtons}
                    onChange={this.onChangeMy}
                />
                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}
export default HomePage;

import "react-datepicker/dist/react-datepicker.css";
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Dropdown from 'react-dropdown'
import DatePicker from "react-datepicker";
import validateTicketName, { validateTicketDescription, validateFileSize, validateFileExtention } from './_helpers/inputValidators.js';
import ReactDatatable from '@ashvin27/react-datatable';

const API_URL = "http://localhost:8080/helpdesk/api/1.0.0/"

const MSG_FILE_SIZE_ERROR = "The size of attached file should not be greater than 5 Mb. Please select another file."
const MSG_FILE_EXTENDION_ERROR = "The selected file type is not allowed. Please select a file of one of the following types: pdf, png, doc, docx, jpg, jpeg."
const MAX_NAME_LENGTH = 100;
const MAX_DESCRIPTION_LENGTH = 500;
const MAX_COMMENT_LENGTH = 500;

class EditTicket extends React.Component {
    constructor(props) {
        super(props);
        this.onTicketDescriptionChange = this.onTicketDescriptionChange.bind(this);
        this.onTicketNameChange = this.onTicketNameChange.bind(this);
        this.DownloadAttachAction = this.DownloadAttachAction.bind(this);
        this.DeleteAttachAction = this.DeleteAttachAction.bind(this);
        this.submitTicket = this.submitTicket.bind(this);
        this.saveTicketAsDraft = this.saveTicketAsDraft.bind(this);
        this.onCategorySelect = this.onCategorySelect.bind(this);
        this.onUrgencySelect = this.onUrgencySelect.bind(this);
        this.onTicketCommentChange = this.onTicketCommentChange.bind(this);
        this.DeleteAttachAction = this.DeleteAttachAction.bind(this);
        this.dismissChanges = this.dismissChanges.bind(this);
        this.loadTicketInfo = this.loadTicketInfo.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.state = {
            file: '',
            error: '',
            ticketValidatioErrorMsg: '',
            msg: '',
            startDate: null,
            categoriesList: [],
            urgencyList: [],
            ticketIdEdit: this.props.match.params.ticketId,
            ticketData: {
                tickedId: null,
                categoryList: [],
                categorySelectedOption: undefined,
                categoryId: undefined,
                urgencyList: [],
                urgencyId: undefined,
                urgencySelectedOption: undefined,
                name: "",
                description: "",
                desiredResolutionDate: undefined,
                attachments: [],
                comment: "",
                resultAttachmentsIdsList: [],
                notLinkedAttachments: [],
            },

            attachesTableHeader: {
                columns: [
                    {
                        key: "id",
                        text: "id",
                        className: "id",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "name",
                        text: "Name",
                        className: "name",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "action",
                        text: "Action",
                        className: "action",
                        width: 100,
                        align: "left",
                        sortable: false,
                        cell: record => {
                            return (
                                <Fragment>
                                    <button
                                        className="btn btn-primary btn-sm"
                                        onClick={() => this.DeleteAttachAction(record)}
                                        style={{ marginRight: '5px' }}>
                                        <i className="glyphicon glyphicon-edit fa fa-edit">Delete</i>
                                    </button>
                                    <button
                                        className="btn btn-primary btn-sm"
                                        onClick={() => this.DownloadAttachAction(record)}
                                        style={{ marginRight: '5px' }}>
                                        <i className="glyphicon glyphicon-edit fa fa-edit">Download</i>
                                    </button>
                                </Fragment>
                            );
                        }
                    }

                ],
                config: {
                    page_size: 10,
                    length_menu: [10, 20, 50],
                    sort: {},
                    button: {
                    },
                    show_info: true
                },
                records: [
                ],
                extraButtons: [
                ],
            }
        }
    }

    DownloadAttachAction(record) {
        this.downloadAttachment(record.idDb);
    }
    DeleteAttachAction(record) {
        let idDbToRemove = record.idDb;
        // We will remove only attaches, that were created
        // during this (last) ticket creation/editing.
        // We will pass to backend full list of required attachments
        // and backend will delete unwanted items.
        let prepState = this.state;
        prepState.attachesTableHeader.records.splice(record.id - 1, 1);
        let idNotLinked = prepState.ticketData.notLinkedAttachments.indexOf(idDbToRemove);
        if (idNotLinked >= 0) { // new attachment was deleted, so we should delete it from server.
            prepState.ticketData.notLinkedAttachments.splice(idNotLinked, 1);
            axios.delete(API_URL + "attachments/" + idDbToRemove,
                { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } });
        }
        let idFullList = prepState.ticketData.resultAttachmentsIdsList.indexOf(idDbToRemove);
        prepState.ticketData.resultAttachmentsIdsList.splice(idFullList, 1);
        for (let i = 0; i < prepState.attachesTableHeader.records.length; i++) {
            prepState.attachesTableHeader.records[i].id = i + 1;
        }
        this.setState(prepState);
    }

    loadCategories() {
        axios.get(API_URL + "categories/",
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let categories = json;
                let prepState = this.state;
                prepState.ticketData.categories = [];
                categories.forEach(element => {
                    this.state.categoriesList.push({
                        value: element.id,
                        label: element.name
                    })
                })
                this.setState(prepState);
            })
    }

    locadUrgencyList() {
        axios.get(API_URL + "urgency/",
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let urgencyListArray = json;
                let prepState = this.state;
                prepState.ticketData.urgencyList = urgencyListArray;
                this.state.urgencyList = urgencyListArray;
                this.setState(prepState);
            })
    }

    loadFilelist(ticketId) {
        axios.get(API_URL + "tickets/attachments/" + ticketId,
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let records = [];
                let loadedAttachmetsIds = [];
                for (let i = 0; i < json.length; i++) {
                    records.push({
                        id: i + 1,
                        name: json[i].filename,
                        idDb: json[i].id,
                    });
                    loadedAttachmetsIds.push(json[i].id);
                }
                let prepState = this.state;
                prepState.attachesTableHeader.records = records;
                prepState.ticketData.resultAttachmentsIdsList = loadedAttachmetsIds;
                this.setState(prepState);
                return json;
            })
    }

    loadTicketInfo(ticketId) {
        axios.get(API_URL + "tickets/" + ticketId,
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let prepState = this.state;
                prepState.ticketData.id = json.id;
                prepState.ticketData.name = json.name;
                prepState.ticketData.description = json.description;
                prepState.ticketData.createdOn = json.createdOn;
                prepState.ticketData.desiredResolutionDate = json.desiredResolutionDate;
                prepState.ticketData.assigneeId = json.assigneeId;
                prepState.ticketData.ownerId = json.ownerId;
                prepState.ticketData.state = json.state;
                prepState.ticketData.urgency = json.urgency;
                prepState.ticketData.categoryId = json.categoryId - 1;
                prepState.ticketData.attachmentsId = json.attachmentsId;
                prepState.ticketData.approverId = json.approverId;
                prepState.ticketData.historyIds = json.historyIds;
                prepState.ticketData.categoryId = json.categoryId;
                let newRowData;
                for (let i = 0; i < this.state.categoriesList.length; i++) {
                    if (this.state.categoriesList[i].value == prepState.ticketData.categoryId) {
                        newRowData = this.state.categoriesList[i];
                        break;
                    }
                }
                prepState.ticketData.categorySelectedOption = newRowData;
                prepState.ticketData.urgencySelectedOption = { "value": json.urgency, "label": json.urgency }

                this.setState(prepState);
                return json;
            })
            .then(json => {
                this.loadFilelist(json.id);
            })
    }

    componentDidMount() {
        this.loadCategories();
        this.locadUrgencyList();
        let prepState = this.state;
        this.setState(prepState);
        if (this.props.match.params.ticketId != null) {
            this.loadTicketInfo(this.props.match.params.ticketId);
        }
    }
    handleChangeDesiredDate = date => {
        let prepState = this.state;
        prepState.ticketData.desiredResolutionDate = date.setHours(0, 0, 0, 0);
        this.setState(prepState);
    };
    downloadAttachment(id) {
        axios.get(API_URL + "attachments/" + id,
            {
                headers: {
                    authorization: 'Basic ' + localStorage.getItem('authdata'),
                    responseType: 'blob',
                }
            })
            .then(
                function (response) {
                    const filename = response.request.getResponseHeader('Content-Disposition').slice("attachment; filename=".length);
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename);
                    document.body.appendChild(link);
                    link.click();
                }
            )
    }
    onTicketNameChange(event) {
        if (validateTicketName(event.target.value)) {
            let prepState = this.state;
            prepState.ticketData.name = event.target.value;
            this.setState(prepState);
        }
        else {
            event.target.value = this.state.ticketData.name;
        }
    }
    onTicketDescriptionChange(event) {
        if (validateTicketDescription(event.target.value)) {
            let prepState = this.state;
            prepState.ticketData.description = event.target.value;
            this.setState(prepState);
        }
        else {
            event.target.value = this.state.ticketData.description;
        }
    }
    validateCommentDescription(text) {
        return validateTicketDescription(text);
    }
    onTicketCommentChange(event) {
        if (this.validateCommentDescription(event.target.value)) {
            let prepState = this.state;
            prepState.ticketData.comment = event.target.value;
            this.setState(prepState);
        }
        else {
            event.target.value = this.state.ticketData.comment;
        }
    }

    uploadFileAction = (event) => {
        event.preventDefault();
        let file = this.state.file;
        if (!file) {
            alert("Please select a file");
            return;
        }
        let error = false;
        let errorMsg = "";
        if (!validateFileSize(file.size)) {
            errorMsg = MSG_FILE_SIZE_ERROR;
            error = true;
        }
        if (!validateFileExtention(file.name)) {
            errorMsg = errorMsg + MSG_FILE_EXTENDION_ERROR;
            error = true;
        }
        if (error) {
            this.setState({ error: errorMsg })
            alert(errorMsg);
            return;
        }
        this.setState({ error: '', msg: '' });
        let data = new FormData();
        data.append('file', file);
        data.append('name', file.name);
        fetch(API_URL + "attachments/", {
            method: 'POST',
            body: data,
            headers: { authorization: 'Basic ' + localStorage.getItem('authdata') }
        })
            .then(function (response) {
                if (response.status != 201) {
                    throw new Error("HTTP status " + response.status);
                }
                return response.json();
            })
            .then(response => {
                let prepState = this.state;
                prepState.attachesTableHeader.records.push({
                    id: prepState.attachesTableHeader.records.length + 1,
                    name: file.name,
                    idDb:  response
                });
                prepState.ticketData.attachments.push(response);
                prepState.ticketData.resultAttachmentsIdsList.push(response);
                prepState.ticketData.notLinkedAttachments.push(response);
                this.setState(prepState);
                this.setState({ error: '', msg: 'Sucessfully uploaded file' });
            }).catch(err => {
                this.setState({ error: err });
            });
    }
    onFileSelectionChange = (event) => {
        this.setState({
            file: event.target.files[0],
            error: ""
        });
    }
    onCategorySelect(selecterRow) {
        let prepState = this.state;
        prepState.ticketData.categoryId = selecterRow.value;
        prepState.ticketData.categorySelectedOption = selecterRow;
        this.setState(prepState);
    }
    onUrgencySelect(selecterRow) {
        let prepState = this.state;
        prepState.ticketData.urgencyId = selecterRow.value;
        prepState.ticketData.urgencySelectedOption = selecterRow;
        this.setState(prepState);
    }
    dismissChanges(event) {
        for (let i = 0; i < this.state.ticketData.notLinkedAttachments.length; i++) {
            axios.delete(API_URL + "attachments/" + this.state.ticketData.notLinkedAttachments[i],
                { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } });
        }
    }

    saveChanges(event, draft) {
        if (!draft) {
            if ((this.state.ticketData.name.length == 0) ||
                (this.state.ticketData.categorySelectedOption == null) ||
                (this.state.ticketData.urgencySelectedOption == null)) {
                alert("You must fill required fields");
                event.preventDefault();
                return;
            }
        }
        if (this.state.ticketData.desiredResolutionDate != null) {
            let currentDate = new Date().setHours(0, 0, 0, 0);
            let desiredDate = new Date(this.state.ticketData.desiredResolutionDate).setHours(0, 0, 0, 0);
            if (currentDate > desiredDate) {
                alert("Desired resolution date is wrong");
                event.preventDefault();
                return;
            }
        }
        let ticket = {
            name: this.state.ticketData.name,
            urgency: this.state.ticketData.urgencySelectedOption != null ? this.state.ticketData.urgencySelectedOption.value : null,
            categoryId: this.state.ticketData.categorySelectedOption != null ? this.state.ticketData.categorySelectedOption.value : null,
            description: this.state.ticketData.description,
            state: this.state.ticketData.state,
            desiredResolutionDate: this.state.ticketData.desiredResolutionDate,
            attachmentsId: this.state.ticketData.resultAttachmentsIdsList,
        }
        ticket.state = draft ? "DRAFT" : "NEW";
        let url = API_URL + "tickets/";
        let method = 'POST';
        if (this.props.match.params.ticketId != null) {
            method = 'PUT';
            ticket.id = this.props.match.params.ticketId;
            url = url + ticket.id;
        }
        let comment = this.state.ticketData.comment;
        fetch
            (url, {
                method: method,
                body: JSON.stringify(ticket),
                headers: {
                    authorization: 'Basic ' + localStorage.getItem('authdata'),
                    'Content-Type': 'application/json'
                }
            })
            .then(function (response) {
                if ((response.status != 201) && (response.status != 200)) {
                    throw new Error("HTTP status " + response.status);
                }
                return response.json();
            })
            .then(response => {
                if (comment != "") {
                    let commentDto = {
                        text: comment,
                        ticketId: response
                    };
                    fetch
                        (API_URL + "comments/", {
                            method: 'POST',
                            body: JSON.stringify(commentDto),
                            headers: {
                                authorization: 'Basic ' + localStorage.getItem('authdata'),
                                'Content-Type': 'application/json'
                            }
                        })
                }
            })
            .catch(err => {
                this.setState({ error: err });
            });
    }

    submitTicket(event) {
        this.saveChanges(event, false);
    }

    saveTicketAsDraft(event) {
        this.saveChanges(event, true);
    }

    render() {
        const isEditMode = this.props.match.params.ticketId == null ? false : true;
        const ticketId = this.props.match.params.ticketId;
        const backUrl = isEditMode ? "/ViewTicketDetails/" + ticketId : "/";

        const DateCustomInput = ({ value, onClick }) => (
            <button className="example-custom-input" onClick={onClick}>
                {value}
            </button>
        );

        return (
            <div>
                <p>Refresh this page if you cannot see expected content</p>
                <div><Link to={backUrl} className="btn btn-primary" onClick={this.dismissChanges}>Return to the ticket list</Link></div>
                {(this.state.ticketIdEdit != null) ? <div>Edit ticket</div> : <div>Add ticket</div>}
                <p>Select category:*</p>
                <div className="col-sm-9 col-sm-offset-1">
                    <Dropdown options={this.state.categoriesList}
                        onChange={this.onCategorySelect}
                        value={this.state.ticketData.categorySelectedOption}
                        placeholder={"Choose category"} />
                </div>
                Name* <input id={"ticketName" + this.props.match.params.ticketId} type="text" value={this.state.ticketData.name} maxLength={MAX_NAME_LENGTH} onChange={this.onTicketNameChange} placeholder="Enter ticket name" />
                <p></p>Description:<p></p>
                <textarea cols={80} rows={7} maxLength={MAX_DESCRIPTION_LENGTH} value={this.state.ticketData.description} onChange={this.onTicketDescriptionChange} placeholder="Enter description"></textarea>
                <p></p>Urgency:*<div className="col-sm-7 col-sm-offset-30">
                    <Dropdown options={this.state.urgencyList}
                        onChange={this.onUrgencySelect}
                        value={this.state.ticketData.urgencySelectedOption}
                        placeholder={"Choose urgency"}
                    />
                </div>
                Desired date:
                <DatePicker
                    selected={this.state.ticketData.desiredResolutionDate}
                    onChange={this.handleChangeDesiredDate}
                    dateFormat="dd/MM/yyyy"
                    minDate={new Date()}
                    customInput={<DateCustomInput />}
                />
                <p></p>Comment:<p></p>
                <textarea cols={80} rows={7} maxLength={MAX_COMMENT_LENGTH} onChange={this.onTicketCommentChange} placeholder="Enter comment"></textarea>

                {(this.state.attachesTableHeader.records.length != 0) &&
                    <div>
                        <h2>Attached files:</h2>
                        <ReactDatatable
                            config={this.state.attachesTableHeader.config}
                            records={this.state.attachesTableHeader.records}
                            columns={this.state.attachesTableHeader.columns}
                            extraButtons={this.state.attachesTableHeader.extraButtons}
                        />
                    </div>
                }
                <div className="App-intro">
                    <h3>Upload a file</h3>
                    <input onChange={this.onFileSelectionChange} type="file"></input>
                    <button type="file" onClick={this.uploadFileAction}>Upload selected file</button>
                </div>
                <Link to={backUrl} className="btn btn-primary" onClick={this.saveTicketAsDraft}>Save as draft</Link>
                <Link to={backUrl} className="btn btn-primary" onClick={this.submitTicket}>Save</Link>
            </div>

        )
    }
}
export default EditTicket; 
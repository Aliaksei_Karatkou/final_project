import React from 'react';

import { userService } from '../_services';


const MAX_EMAIL_LENGTH = 100;
const MAX_PASSWORD_LENGTH = 20;
const MIN_PASSWORD_LENGTH = 6;


class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        userService.logout();
        this.state = {
            username: '',
            password: '',
            submitted: false,
            loading: false,
            error: '',
            passwordFormatOk: true,
            emailFormatOk: true
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validateEmailFieldRequirements(email) {
        if (email.length == 0) { return true };
        if (/^[@\.]/.test(email)) { return false };
        if (/[@\.]$/.test(email)) { return false };
        if (!(/@/.test(email))) { return false };
        if (!(/\./.test(email))) { return false };
        return true;
    }
    validatePasswordFieldRequirements(text) {
        if (text.length == 0) { return true };
        if (text.length < MIN_PASSWORD_LENGTH) { return false };
        if (!(/[a-z]/.test(text))) { return false };
        if (!(/[A-Z]/.test(text))) { return false };
        if (!(/\d/.test(text))) { return false };
        if (!(/[~\."(),:;<>@[\]!#$%&'*+-/=?^_`{|}]/.test(text))) { return false };
        return true;
    }

    onEmailChange = (e) => {
        this.setState({
            username: e.target.value,
            error: ""
        })
    }

    onPasswordChange = (e) => {
        this.setState({
            password: e.target.value,
            error: ""
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;

        // stop here if form is invalid
        if (!(username && password)) {
            return;
        }
        // do not try to authenticate if password or username doesn't meet requirements.
        if (!(this.validatePasswordFieldRequirements(password) && this.validateEmailFieldRequirements(username))) {
            return;
        }

        this.setState({ loading: true });
        userService.login(username, password)
            .then(
                user => {
                    const { from } = this.props.location.state || { from: { pathname: "/" } };
                    this.props.history.push(from);
                },
                error => {
                    this.setState({ error, loading: false })
                }
            );
    }

    render() {
        const { username, password, submitted, loading, error } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Login</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" maxLength={MAX_EMAIL_LENGTH} className="form-control" name="username" value={username} onChange={this.onEmailChange} />
                        {submitted && !username &&
                            <div className="alert-danger help-block">Please fill out the required field.</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" maxLength={MAX_PASSWORD_LENGTH} className="form-control" name="password" value={password} onChange={this.onPasswordChange} />
                        {submitted && !password &&
                            <div className="alert-danger help-block">Please fill out the required field.</div>
                        }
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary" disabled={loading}>Login</button>
                        {loading &&
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                    </div>

                    <div>
                        {!(this.validatePasswordFieldRequirements(password) && this.validateEmailFieldRequirements(username)) &&
                            <div className="alert-danger help-block">Please make sure you are using a valid email or password</div>
                        }
                    </div>
                    <div>{error && <div className="alert-danger help-block">{error}</div>}</div>

                </form>
            </div>
        );
    }
}

export default LoginPage; 
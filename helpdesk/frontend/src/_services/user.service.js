const apiUrl = "http://localhost:8080/helpdesk/api/1.0.0/"

export const userService = {
    login,
    logout,
};

function login(username, password) {
    let requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Accept-Encoding': "gzip, deflate",
            'Authorization': 'Basic ' + window.btoa(username + ':' + password)
        }
    };

    return fetch(`${apiUrl}login`, requestOptions)
        .then(handleResponse)
        .then(response => {
            localStorage.setItem('user', username);
            localStorage.setItem('password', password);
            localStorage.setItem('authdata', btoa(username + ':' + password));
            localStorage.setItem('role',response.role);
            return true
        })
}

function logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('password');
    localStorage.removeItem('authdata');
    localStorage.removeItem('role');
}

function handleResponse(response) {

    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                logout();
            }
            let error = response.statusText;
            return Promise.reject(error);
        }
        return data;
    });
}

import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Dropdown from 'react-dropdown'
const API_URL = "http://localhost:8080/helpdesk/api/1.0.0/"
class Feedback extends React.Component {
    constructor(props) {
        super(props);
        this.onRateSelect = this.onRateSelect.bind(this);
        this.addFeedback = this.addFeedback.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.state = {
            rateList: [1, 2, 3, 4, 5],
            rateValue: undefined,
            textValue: undefined,
            viewMode: true
        }
    }
    componentDidMount() {
        const ticketId = this.props.match.params.ticketId;
        const mode = this.props.match.params.mode;
        let prepState = this.state;
        prepState.mode = mode;
        prepState.ticketId = ticketId;
        prepState.viewMode = mode == "view";
        this.setState(prepState);
        if (this.state.viewMode) {
            let url = API_URL + "feedbacks/" + this.state.ticketId;
            axios.get(API_URL + "feedbacks/" + this.state.ticketId, {
                headers: { authorization: 'Basic ' + localStorage.getItem('authdata') }
            })
                .then(res => {
                    let prepState = this.state;
                    this.state.commentValue = res.data.text;
                    prepState.rateValueRow = { "value": res.data.rate, "label": res.data.rate }
                    prepState.rateValue = res.data.rate;
                    this.setState(prepState);
                    return;
                })
        }
    }
    onTextChange(event) {
        if (this.setState.viewMode) { return; }
        let prepState = this.state;
        prepState.textValue = event.target.value;
        this.setState(prepState);
    }

    submitButtonAction = (event) => {
        if (this.state.rateValue == null) {
            alert("You must rate this solution")
            event.preventDefault();
        }
        else {
            this.addFeedback(this.props.match.params.ticketId, this.state.rateValueRow.value, this.state.textValue);
        }
    }
    addFeedback(ticketId, rate, text) {
        let newFeedbackBody = {
            rate: rate,
            text: text
        }
        let url = API_URL + "feedbacks/" + ticketId
        fetch
            (url, {
                method: 'POST',
                body: JSON.stringify(newFeedbackBody),
                headers: {
                    authorization: 'Basic ' + localStorage.getItem('authdata'),
                    'Content-Type': 'application/json'
                }
            })
    }
    onRateSelect(event) {
        let prepState = this.state;
        prepState.rateValueRow = event;
        prepState.rateValue = event.value;
        this.setState(prepState);
    }
    render() {
        return (
            <div>
                <h5>Feedback</h5>
                {this.state.viewMode ? <div>View mode.<p></p>This ticket was rated with rate "{this.state.rateValue}"</div> :
                    <div><p>Edit mode </p>
                        <p>Please rate your satisfaction with the solutuon:*</p>
                        <div className="col-sm-9 col-sm-offset-1">
                            <Dropdown options={this.state.rateList}
                                onChange={this.onRateSelect}
                                value={this.state.rateValueRow}
                                placeholder={"Choose rate"} />
                        </div>
                    </div>
                }
                { this.state.viewMode ?
                <div><h5>Feedback was commented with this text message:</h5>
                <textarea cols={160} rows={7} value={this.state.commentValue} onChange={this.onTextChange}></textarea>
                </div>
                :
                <div><h5>You can say some words about this solution:</h5>
                <textarea cols={160} rows={7} value={this.state.commentValue} onChange={this.onTextChange} placeholder="Say something..."></textarea>
                </div>
                    }
                <Link to="/" className="btn btn-primary" >Back</Link>
                {!this.state.viewMode && <Link to="/" className="btn btn-primary" onClick={this.submitButtonAction}>Submit</Link>}
            </div>
        )
    }
}
export default Feedback;
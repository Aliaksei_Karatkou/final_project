import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LoginPage from './LoginPage/LoginPage.jsx'
import PrivateRoute from './_components/PrivateRoute';
import HomePage from './HomePage'
import EditTicket from './EditTicket.js'
import ViewTicketDetails from './ViewTicketDetails.js';
import Feedback from './Feedback.js'

class App extends Component {
    render() {
        return (
            <div >
                <div >
                    <div>
                        <Router>
                            <div>
                                <PrivateRoute exact path="/" component={HomePage} />
                                <PrivateRoute path="/addTicket" component={EditTicket} />
                                <PrivateRoute path="/editTicket/:ticketId" component={EditTicket} />
                                <PrivateRoute path="/ViewTicketDetails/:ticketId" component={ViewTicketDetails} />
                                <PrivateRoute path="/feedback/:ticketId/:mode" component={Feedback} />

                                <Route path="/login" component={LoginPage} />
                            </div>
                        </Router>

                    </div>
                </div>
            </div>
        );
    }

}

export default App;


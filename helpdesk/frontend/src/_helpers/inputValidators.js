const MAX_TICKET_NAME_LENGTH = 100;
const MAX_TICKET_DESCRIPTIONS_LENGTH = 100;
const MAX_FILE_SIZE =  5*1024*1024;

export default function validateTicketName(text) {
    if (text.length >MAX_TICKET_NAME_LENGTH ) return false;
   return /^[ ~\."(),:;<>@[\]!#$%&'*+-/=?^_`{|}a-z\d]*$/.test(text)   ;
}
export  function validateTicketDescription(text) {
    if (text.length >MAX_TICKET_DESCRIPTIONS_LENGTH ) return false;
   return /^[ ~\."(),:;<>@[\]!#$%&'*+-/=?^_`{|}a-zA-Z\d]*$/.test(text)   ;
}
export function validateFileSize(size) {
    return size<=MAX_FILE_SIZE;
}
export function validateFileExtention(filename) {
    let re = /^.*\.(pdf|png|doc|docx|jpg|jpeg)$/i;
    return re.test(filename);
}
export  function validateFilter(text) {
    if (text.length >MAX_TICKET_DESCRIPTIONS_LENGTH ) return false;
   return /^[ ~\."(),:;<>@[\]!#$%&'*+-/=?^_`{|}a-zA-Z\d]*$/.test(text)   ;
}


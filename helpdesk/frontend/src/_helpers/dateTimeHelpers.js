export function getDateStr(date) {
    return date != null ? timestampToDateString(date) : "";
}
function timestampToDateString(timestamp) {
    let date = new Date(timestamp);
    let year = date.getFullYear();
    let month = 1 + date.getMonth();
    let day = date.getDate()
    return [
        (day > 9 ? '' : '0') + day,
        (month > 9 ? '' : '0') + month,
        year
    ].join('/');
}
export function timestampToString(timestamp) {
    let date = new Date(timestamp);
    return date.toLocaleDateString() + " " + date.toLocaleTimeString();
}
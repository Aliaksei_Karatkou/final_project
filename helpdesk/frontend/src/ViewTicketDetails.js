import React, { Fragment } from 'react';
import axios from 'axios';
import { getDateStr, timestampToString } from './_helpers/dateTimeHelpers.js';
import { Link } from 'react-router-dom';
import { validateTicketDescription } from './_helpers/inputValidators.js';
import ReactDatatable from '@ashvin27/react-datatable';

const API_URL = "http://localhost:8080/helpdesk/api/1.0.0/"
const MAX_COMMENT_LENGTH = 500;

class ViewTicketDetails extends React.Component {
    constructor(props) {
        super(props);
        this.getCategoryName = this.getCategoryName.bind(this);
        this.downloadAttachment = this.downloadAttachment.bind(this);
        this.switchFullViewCommentsHistoryList = this.switchFullViewCommentsHistoryList.bind(this)
        this.locaHistory = this.locaHistory.bind(this);
        this.switchCommentsOrHistory = this.switchCommentsOrHistory.bind(this);
        this.validateCommentDescription = this.validateCommentDescription.bind(this);
        this.onTicketCommentChange = this.onTicketCommentChange.bind(this);
        this.onAddComment = this.onAddComment.bind(this);
        this.state = {
            test_mark_overview: "nn",
            ticketData: {},
            attachments: [],
            showAllRecords: false,
            showHistory: false,
            newComment: "",

            attachmentsTable: {
                columns: [
                    {
                        key: "id",
                        text: "id",
                        className: "id",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "filename",
                        text: "File name",
                        className: "filename",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "action",
                        text: "Action",
                        className: "action",
                        width: 100,
                        align: "left",
                        sortable: false,
                        cell: record => {
                            return (
                                <Fragment>
                                    <button
                                        className="btn btn-primary btn-sm"
                                        onClick={() => this.downloadAttachment(record.id)}
                                        style={{ marginRight: '5px' }}>
                                        <i className="glyphicon glyphicon-edit fa fa-edit">Download</i>
                                    </button>
                                </Fragment>
                            );
                        }
                    }
                ],
                config: {
                    page_size: 10,
                    length_menu: [10, 20, 50],
                    sort: {},
                    button: {},
                    show_info: true
                },
                records: [
                ],
                extraButtons: [
                ],
            },
            commentsTable: {
                columns: [
                    {
                        key: "date",
                        text: "Timestamp",
                        className: "date1",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "username",
                        text: "User",
                        className: "username1",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "text",
                        text: "comment",
                        className: "comment1",
                        align: "left",
                        sortable: false,
                    },
                ],
                config: {
                    page_size: 10,
                    length_menu: [10, 20, 50],
                    sort: {},
                    button: {},
                    show_info: true
                },
                records: [
                ],
                extraButtons: [
                ],
            },
            historyTable: {
                columns: [
                    {
                        key: "date",
                        text: "Date/Time",
                        className: "date",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "username",
                        text: "User",
                        className: "username",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "action",
                        text: "Action",
                        className: "comment",
                        align: "left",
                        sortable: false,
                    },
                    {
                        key: "description",
                        text: "Description",
                        className: "description",
                        align: "left",
                        sortable: false,
                    },

                ],
                config: {
                    page_size: 10,
                    length_menu: [10, 20, 50],
                    sort: {},
                    button: {},
                    show_info: true
                },
                records: [
                ],
                extraButtons: [
                ],
            }
        }
    }

    loadUser(id, userKind) {
        axios.get(API_URL + "users/" + id, {
            headers: { authorization: 'Basic ' + localStorage.getItem('authdata') }
        })
            .then(res => {
                let user = res.data;
                let prepState = this.state;
                let userFullName = user.firstName + " " + user.lastName;
                if (userKind == "OWNER") {
                    prepState.ticketData.ownerObj = user;
                    prepState.ticketData.ownerFullName = userFullName;
                }
                if (userKind == "APPROVER") {
                    prepState.ticketData.approverObj = user;
                    prepState.ticketData.approverFullName = userFullName;
                }
                if (userKind == "ASSIGNEE") {
                    prepState.ticketData.assigneeObj = user;
                    prepState.ticketData.assigneeFullName = userFullName;
                }
                this.setState(prepState);
            })

    }

    loadCategories() {
        axios.get(API_URL + "categories/", {
            headers: { authorization: 'Basic ' + localStorage.getItem('authdata') }
        })
            .then(res => {
                let categories = res.data;
                let prepState = this.state;
                let newCategories = [];
                categories.forEach(element => {
                    newCategories.push({
                        value: element.id,
                        label: element.name
                    })
                })
                prepState = this.state;
                prepState.categories = newCategories;
                this.setState(prepState);
            })
    }

    locadUrgencyList() {
        axios.get(API_URL + "urgency/",
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let urgencyListArray = json.urgencyList;
                let prepState = this.state;
                prepState.ticketData.urgencyList = [];
                urgencyListArray.forEach(element => {
                    prepState.ticketData.urgencyList.push({
                        id: element.id,
                        name: element.name,
                    });
                    this.state.urgencyList.push({
                        value: element.id,
                        label: element.name
                    })
                })
                this.setState(prepState);
            })
    }

    loadFilelist(ticketId) {
        axios.get(API_URL + "tickets/attachments/" + ticketId,
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let prepState = this.state;
                prepState.attachmentsTable.records = json;
                this.setState(prepState);
                return json;
            })
    }

    loadTicketInfo(ticketId) {
        axios.get(API_URL + "tickets/" + ticketId,
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let prepState = this.state;
                prepState.ticketData = json;
                this.setState(prepState);
                return json;
            })
            .then(json => {
                if (json.ownerId != null) { this.loadUser(json.ownerId, "OWNER"); }
                return json
            })
            .then(json => {
                if (json.approverId != null) { this.loadUser(json.approverId, "APPROVER"); }
                return json
            })
            .then(json => {
                if (json.assigneeId != null) { this.loadUser(json.assigneeId, "ASSIGNEE") };
                return json
            })
            .then(json => {
                if (json.assigneeId != null) { this.loadUser(json.assigneeId, "ASSIGNEE") };
                return json
            })
            .then(json => {
                this.loadFilelist(json.id);
            })
    }
    locaComments(ticketId) {
        let url = API_URL + "comments/tickets/" + ticketId;
        if (this.state.showAllRecords) {
            url = url + "?unlimited=true";
        }
        axios.get(url,
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let prepState = this.state;
                let newComments = [];
                res.data.forEach(element => {
                    newComments.push({
                        id: element.id,
                        text: element.text,
                        date: timestampToString(element.date),
                        username: element.username
                    })
                })
                prepState.commentsTable.records = newComments;
                this.setState(prepState);
                return json;
            })
    }

    locaHistory(ticketId) {
        let url = API_URL + "tickets/histories/" + ticketId;
        if (this.state.showAllRecords) {
            url = url + "?unlimited=true";
        }
        axios.get(url,
            { headers: { authorization: 'Basic ' + localStorage.getItem('authdata') } })
            .then(res => {
                const json = res.data;
                let prepState = this.state;
                prepState.historyTable.records = res.data;
                for (let i = 0; i < prepState.historyTable.records.length; i++) {
                    prepState.historyTable.records[i].date = timestampToString(prepState.historyTable.records[i].date);
                }
                this.setState(prepState);
                return json;
            })
    }

    downloadAttachment(id) {
        axios.get(API_URL + "attachments/" + id,
            {
                headers: {
                    authorization: 'Basic ' + localStorage.getItem('authdata'),
                    responseType: 'blob',
                }
            })
            .then(
                function (response) {
                    const filename = response.request.getResponseHeader('Content-Disposition').slice("attachment; filename=".length);
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename);
                    document.body.appendChild(link);
                    link.click();
                }
            )
    }

    componentDidMount() {
        const ticketId = this.props.match.params.ticketId
        this.loadCategories();
        this.loadTicketInfo(ticketId);
        this.locaComments(ticketId);
        this.locaHistory(ticketId);
    }

    getCategoryName() {
        let st = this.state;
        let categoryIndex = st.ticketData.categoryId;
        if (categoryIndex == null) {
            return "";
        }
        if (st.categories == null) {
            return "FullCategoryList Not loaded jet";
        }
        return st.categories[st.ticketData.categoryId - 1].label;
    }

    switchFullViewCommentsHistoryList() {
        let prepState = this.state;
        prepState.showAllRecords = !prepState.showAllRecords;
        this.setState(prepState);
        this.locaComments(this.props.match.params.ticketId);
        this.locaHistory(this.props.match.params.ticketId);
    }
    switchCommentsOrHistory() {
        let prepState = this.state;
        prepState.showHistory = !prepState.showHistory;
        this.setState(prepState);
    }
    isCommentsView() {
        return !this.state.showHistory;
    }
    validateCommentDescription(text) {
        return validateTicketDescription(text);
    }
    onTicketCommentChange(event) {
        if (this.validateCommentDescription(event.target.value)) {
            let prepState = this.state;
            prepState.newComment = event.target.value;
            this.setState(prepState);
        }
        else {
            event.target.value = this.state.newComment;
        }
    }
    isEditAllowed(ticketState) {
        return (ticketState=='DRAFT') && (localStorage.getItem('role') == "EMPLOYEE" || localStorage.getItem('role') == "MANAGER");
    }
    onAddComment(event) {
        if (this.state.newComment.length == 0) {
            return;
        }
        let newCommentBody = {
            text: this.state.newComment,
            ticketId: this.props.match.params.ticketId
        }
        fetch
            (API_URL + "comments/", {
                method: 'POST',
                body: JSON.stringify(newCommentBody),

                headers: {
                    authorization: 'Basic ' + localStorage.getItem('authdata'),
                    'Content-Type': 'application/json'
                }
            })
            .then(function (response) {
                if (response.status != 201) {
                    throw new Error("HTTP status " + response.status);
                }
                this.locaComments(this.props.match.params.ticketId);
                this.locaHistory(this.props.match.params.ticketId);
            }.bind(this))
    }

    render() {
        return (
            <div>
                <b><h2>Ticket overview</h2></b><br></br>
                <b>Ticket name:</b> {this.state.ticketData.name}<br></br>
                <b>Status</b> {this.state.ticketData.state}<br></br>
                <b>Description</b> {this.state.ticketData.description}<br></br>
                <b>CreatedOn:</b> {getDateStr(this.state.ticketData.createdOn)}
                <br></br><b>Category:</b> {this.getCategoryName()}
                <br></br><b>Urgency:</b> {this.state.ticketData.urgency}
                <br></br><b>Desired resolution date:</b> {getDateStr(this.state.ticketData.desiredResolutionDate)}
                <br></br><b>Owner:</b> {this.state.ticketData.ownerFullName}
                <br></br><b>Approver:</b> {this.state.ticketData.approverFullName}
                <br></br><b>Assignee:</b> {this.state.ticketData.assigneeFullName}
                <br></br><h2>Attachmets:</h2>
                {this.state.attachmentsTable.records.length != 0 ?
                    <div>
                        <ReactDatatable
                            config={this.state.attachmentsTable.config}
                            records={this.state.attachmentsTable.records}
                            columns={this.state.attachmentsTable.columns}
                            extraButtons={this.state.attachmentsTable.extraButtons}
                        />
                    </div>
                    : <div>
                        Absent.
                    </div>
                }
                <button onClick={this.switchFullViewCommentsHistoryList}>switch Show all</button>
                <button onClick={this.switchCommentsOrHistory}>switch comments/history</button>
                {this.isCommentsView() ?
                    <div><h2>Comments</h2>
                        <ReactDatatable
                            config={this.state.commentsTable.config}
                            records={this.state.commentsTable.records}
                            columns={this.state.commentsTable.columns}
                            extraButtons={this.state.commentsTable.extraButtons}
                        /></div>
                    :
                    <div><h2>History</h2>
                        <ReactDatatable
                            config={this.state.historyTable.config}
                            records={this.state.historyTable.records}
                            columns={this.state.historyTable.columns}
                            extraButtons={this.state.historyTable.extraButtons}
                        /></div>
                }
                <p></p>Add Comment:<p></p>
                <textarea cols={80} rows={7} maxLength={MAX_COMMENT_LENGTH} onChange={this.onTicketCommentChange} placeholder="Enter comment"></textarea>
                <button onClick={this.onAddComment}>Add comment</button>
                <br>
                </br>
                <Link to="/" className="btn btn-primary">Return to the ticket list</Link>
                {  this.isEditAllowed(this.state.ticketData.state) &&
                    <div><Link to={"/editTicket/" + this.props.match.params.ticketId} className="btn btn-primary">Edit ticket</Link></div>
                }

            </div>
        )
    }
}
export default ViewTicketDetails;

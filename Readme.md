##  Readme.
Used jetty, h2 database, spring, thymeleaf, REST, reactjs, JUnit(Mockito), MockMvc


how to run:

clean jetty:run-war

frontend url: http://localhost:8080/

Default users and passwords:

Role 'Employee':

user1_mogilev@yopmail.com/P@ssword1
user2_mogilev@yopmail.com/P@ssword1

Role 'Manager':

manager1_mogilev@yopmail.com/P@ssword1
manager2_mogilev@yopmail.com/P@ssword1

Role 'Engineer':

engineer1_mogilev@yopmail.com/P@ssword1
engineer2_mogilev@yopmail.com/P@ssword1
